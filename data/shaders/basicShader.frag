
#version 330 core

out vec4 FragColor;
in vec2 uvOut;

uniform vec3 albedo;

const float range = 0.05;
const float Hrange = 1 - range;

void main() {

    FragColor = vec4(albedo * 

      float(uvOut.x > range && uvOut.x < Hrange &&
      uvOut.y > range && uvOut.y < Hrange),

      1.0);

}