solution "Engine_Solution"
  
  --Configurations
    configurations{
    "Debug",
    "Release",
    }

    platforms{
      "x32",
      "x64",
      "Native",
    }

    language "C++"
    debugdir "../build"
    targetdir "../bin"
    location "../build"


    defines{"ENGINE_WIN32"}

    -- Main Links
    links{
      "winmm",
      "kernel32",
      "user32",
      "gdi32",
      "winspool",
      "shell32",
      "ole32",
      "oleaut32",
      "uuid",
      "comdlg32",
      "advapi32",
      "opengl32",
    }


    defines{
      "WIN32",
      --"_DEBUG",
      "_CONSOLE",
      "AL_BUILD_LIBRARY",
      "AL_ALEXT_PROTOTYPES", 
      "AL_LIBTYPE_STATIC",
      "_WIN32_WINNT=0x0502", 
      "_LARGEFILE_SOURCE",
      "_LARGE_FILES", 
      "restrict=",
      "_CRT_SECURE_NO_WARNINGS",
      "_CRT_NONSTDC_NO_DEPRECATE", 
      "strcasecmp=_stricmp",
      "strncasecmp=_strnicmp",
      
      --GLFW
      "GLEW_STATIC",
      "_GLFW_WIN32",
      "HAVE_GLEW",

      "HAVE_STDINT_H",
      "HAVE_TIME_H",
      "HAVE_WINDOWS_H",
      "HAVE_FEATURES_H",
      "HAVE_STAT",
      "HAVE_SLEEP",
    }



    includedirs{
      "../include",
      "../src",
      "../deps",
      "../external",
  
      "../deps/glew/include",
      "../deps/glew/src",
  
      "../deps/Engine/Core/include",
      "../deps/Engine/Core/src",
      "../deps/Engine/Maths/include",
      "../deps/Engine/Maths/src",
      "../deps/Engine/System/include",
      "../deps/Engine/System/src",
  
      "../deps/imgui/",

      "../deps/tinyobj/include",
    }

dofile "toolchain.lua"

    --When in Debug
    configuration "Debug"
    defines { "DEBUG" }
    flags { "Symbols" }

------------------------------------------------------
--MAIN------------------------------------------------
------------------------------------------------------

project "Main"
kind "ConsoleApp"

  --Links to the other projects
    links {
      "Engine",
      "GLFWLIB",
      "IMGUILIB",
    }

    files{
      "../user/src/*.cc",
      "../user/include/*.h",
    }

    includedirs{
      "../user/src",
      "../user/include",
    }

    configuration "Debug"
    defines { "DEBUG" }
    flags {"ExtraWarnings" }
------------------------------------------------------
--Engine----------------------------------------------
------------------------------------------------------


project "Engine"
kind "StaticLib"

    files{
      "../deps/Engine/**.cc",
      "../deps/Engine/**.c",
      "../deps/Engine/**.h",
      "../deps/Engine/**.hpp",
    }

    links{
      "glew32",
      "glu32",
      "GLU32",
      "opengl32",
    }

includedirs{
  
    "../deps/imgui",
    "../deps/imgui/imgui_tabs",
    "../deps/glfw/include",
    "../deps/glew/include",
}
    configuration "Debug"
    defines { "DEBUG" }
    flags {"ExtraWarnings" }
------------------------------------------------------
--IMGUI-----------------------------------------------
------------------------------------------------------

project "IMGUILIB"
  kind "StaticLib"

  defines{
    "HAVE_GLEW",
  }

  includedirs{
  
    "../deps/imgui",
    "../deps/imgui/imguiTextEditor",
    "../deps/imgui/imgui_tabs",
    "../deps/glfw/include",
    "../deps/glew/include/GL",
    "../deps/glm",
  }

  files{
    "../deps/imgui/*.cpp",
    "../deps/imgui/imgui_tabs/*.cpp",
    "../deps/imgui/imguiTextEditor/*.cpp",
  }

    configuration "Debug"
    defines { "DEBUG" }
    flags {"MinimumWarnings" }

------------------------------------------------------
--GLEW GLFW-------------------------------------------
------------------------------------------------------

project "GLFWLIB"
  kind "StaticLib"

files{
    "../deps/glfw/src/*win32*.c",
    "../deps/glfw/src/wgl_context.c",
    "../deps/glfw/src/egl_context.c",
    "../deps/glfw/deps/*win32*.c",
    "../deps/glfw/src/context.c",
    "../deps/glfw/src/init.c",
    "../deps/glfw/src/input.c",
    "../deps/glfw/src/monitor.c",
    "../deps/glfw/src/vulkan.c",
    "../deps/glfw/src/window.c",
    "../deps/glfw/include/*.h",
    "../deps/glew/src/glew.c",
  }
--
  links{
    "glew32",
    "glu32",
    "GLU32",
    "opengl32",
  }

    configuration "Debug"
    defines { "DEBUG" }
    flags {"MinimumWarnings" }

------------------------------------------------------
--TINYOBJ---------------------------------------------
------------------------------------------------------

project "TinyOBJ"
  kind "StaticLib"

  files{
    "../deps/tinyobj/src/*.cc",
    "../deps/tinyobj/include/*.h",
  }

    configuration "Debug"
    defines { 
      "DEBUG",
    }
    flags {"MinimumWarnings" }