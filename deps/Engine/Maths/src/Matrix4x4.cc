
#include "Matrix4x4.h"
/*
Matrix4x4::Matrix4x4() {

	//	1 0 0 0
	//	0 1 0 0
	//	0 0 1 0
	//	0 0 0 1

	V[0] = FVector4(1.0f, 0.0f, 0.0f, 0.0f);
	V[1] = FVector4(0.0f, 1.0f, 0.0f, 0.0f);
	V[2] = FVector4(0.0f, 0.0f, 1.0f, 0.0f);
	V[3] = FVector4(0.0f, 0.0f, 0.0f, 1.0f);
}

Matrix4x4::Matrix4x4(float32 n) {

	//	n n n n
	//	n n n n
	//	n n n n
	//	n n n n

	V[0] = FVector4(n, n, n, n);
	V[1] = FVector4(n, n, n, n);
	V[2] = FVector4(n, n, n, n);
	V[3] = FVector4(n, n, n, n);
}

Matrix4x4::Matrix4x4(
	float32 x0, float32 y0, float32 z0, float32 w0,
	float32 x1, float32 y1, float32 z1, float32 w1,
	float32 x2, float32 y2, float32 z2, float32 w2,
	float32 x3, float32 y3, float32 z3, float32 w3) {

	//	x0, y0, z0, w0
	//	x1, y1, z1, w1
	//	x2, y2, z2, w2
	//	x3, y3, z3, w3

	V[0] = FVector4(x0, y0, z0, w0);
	V[1] = FVector4(x1, y1, z1, w1);
	V[2] = FVector4(x2, y2, z2, w2);
	V[3] = FVector4(x3, y3, z3, w3);
}

Matrix4x4::Matrix4x4(Matrix4x4* m) {
	V[0] = m->V[0];
	V[1] = m->V[1];
	V[2] = m->V[2];
	V[3] = m->V[3];
}

Matrix4x4::Matrix4x4(FVector4 v0, FVector4 v1, FVector4 v2, FVector4 v3) {
	V[0] = v0;
	V[1] = v1;
	V[2] = v2;
	V[3] = v3;
}

/////////////// FUNCTIONS \\\\\\\\\\\\\\\

Matrix4x4 Matrix4x4::Transpose() {
	return Matrix4x4(
		V[0].X, V[1].X, V[2].X, V[3].X,
		V[0].Y, V[1].Y, V[2].Y, V[3].Y,
		V[0].Z, V[1].Z, V[2].Z, V[3].Z,
		V[0].W, V[1].W, V[2].W, V[3].W
	);
}
*/