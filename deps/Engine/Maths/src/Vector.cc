#include "Vector.h"


//CONSTRUCTORS DECLARATION
//Vector::Vector(): X(0), Y(0), Z(0) {}
//Vector::Vector(float32 value) : X(value), Y(value), Z(value) {};
//Vector::Vector(float32 x, float32 y, float32 z) : X(x), Y(y), Z(z) {};
//Vector::Vector(int32 value) : X((float32)value), Y((float32)value), Z((float32)value) {};
//Vector::Vector(int32 x, int32 y, int32 z) : X((float32)x), Y((float32)y), Z((float32)z) {};
//
////GLOBAL VECTORS DECLARATION
//const Vector Vector::Zero(0.0f);
//const Vector Vector::One(1.0f);
//const Vector Vector::Right(1.0f, 0.0f, 0.0f);
//const Vector Vector::Up(0.0f, 1.0f, 0.0f);
//const Vector Vector::Forward(0.0f, 0.0f, 1.0f);