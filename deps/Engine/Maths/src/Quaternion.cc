
#include "Quaternion.h"
#include "MyMath.h"

Quaternion::Quaternion(): X(0.0f), Y(0.0f), Z(0.0f), W(0.0f) {
}

Quaternion::Quaternion(const FVector& rad) {
	const FVector c = MyMath::Cos(rad * 0.5f);
	const FVector s = MyMath::Sin(rad * 0.5f);

	W = c.X * c.Y * c.Z + s.X * s.Y * s.Z;
	X = s.X * c.Y * c.Z - c.X * s.Y * s.Z;
	Y = c.X * s.Y * c.Z + s.X * c.Y * s.Z;
	Z = c.X * c.Y * s.Z - s.X * s.Y * c.Z;
}

FMatrix4x4 Quaternion::ToMatrix() const {
	float32 xx = X * X;
	float32 yy = Y * Y;
	float32 zz = Z * Z;
	float32 xz = X * Z;
	float32 xy = X * Y;
	float32 yz = Y * Z;
	float32 wx = W * X;
	float32 wy = W * Y;
	float32 wz = W * Z;

	FMatrix4x4 result = FMatrix4x4();

	result.V[0][0] = 1.0f - 2.0f * (yy + zz);
	result.V[0][1] = 2.0f * (xy + wz);
	result.V[0][2] = 2.0f * (xz - wy);

	result.V[1][0] = 2.0f * (xy - wz);
	result.V[1][1] = 1.0f - 2.0f * (xx + zz);
	result.V[1][2] = 2.0f * (yz + wx);

	result.V[2][0] = 2.0f * (xz + wy);
	result.V[2][1] = 2.0f * (yz - wx);
	result.V[2][2] = 1.0f - 2.0f * (xx + yy);

	return result;
}
