
#ifndef __MY_MATH_H__
#define __MY_MATH_H__ 1

#include "Typedef.h"

#include "Vector.h"
#include "Vector2.h"
#include "Vector4.h"
#include "Matrix4x4.h"

#include <math.h>

#define MATH_PI (3.14159265359f) // PI
#define MATH_HALF_PI (1.57079632679f) // PI / 2

#define MATH_DEGTORAD(value) (value * 0.01745329251f) // * PI / 180
#define MATH_RADTODEG(value) (value * 57.2957795131f) // * 180 / PI

#define MATH_E (2.71828182846f) // e

#define CMP(x, y) AlmostEqualRelativeAndAbs(x, y, 0.005f)

INLINE bool AlmostEqualRelativeAndAbs(float A, float B, float maxDiff, float maxRelDiff = 1.192092896e-07F /*FLT_EPSILON*/) {
	// Check if the numbers are really close -- needed when comparing numbers near zero.
	float diff = fabs(A - B);
	if (diff <= maxDiff) {
		return true;
	}

	A = fabs(A);
	B = fabs(B);
	float largest = (B > A) ? B : A;

	if (diff <= largest * maxRelDiff) {
		return true;
	}
	return false;
}

struct MyMath {

	////////////////////////////////
	//           SCALAR           //
	////////////////////////////////
	template<class Type>
	INLINE static Type Clamp(const Type& value, const Type& min, const Type& max) {
		return (value < min ? min : (value > max ? max : value));
	}

	INLINE static float32 Sqrt(const float32& scalar) {
		return sqrtf(scalar);
	}

	INLINE static float64 Sqrt(const float64& scalar) {
		return sqrt(scalar);
	}

	template<class Type>
	INLINE static Type Abs(const Type& value) {
		return value > static_cast<Type>(0) ? value : -value;
	}

	////////////////////////////////
	//           VECTOR           //
	////////////////////////////////

	// CLAMP
	template<class Type>
	INLINE static Vector<Type> Clamp(const Vector<Type>& vector, const Type& min, const Type& max) {
		return Vector<Type>(
			vector.X < min ? min : (vector.X > max ? max : vector.X),
			vector.Y < min ? min : (vector.Y > max ? max : vector.Y),
			vector.Z < min ? min : (vector.Z > max ? max : vector.Z));
	}

	// COS
	template<class Type>
	INLINE static Vector<Type> Cos(const Vector<Type>& v) {
		return Vector<Type>(cosf(v.X), cosf(v.Y), cosf(v.Z));
	}

	// SIN
	template<class Type>
	INLINE static Vector<Type> Sin(const Vector<Type>& v) {
		return Vector<Type>(sinf(v.X), sinf(v.Y), sinf(v.Z));
	}

	// SIZE
	template<class Type>
	INLINE static float32 Size(const Vector<Type>& v) {
		return sqrtf((v.X * v.X) + (v.Y * v.Y) + (v.Z * v.Z));
	}

	// SQUARED SIZE
	template<class Type>
	INLINE static float32 SquaredSize(const Vector<Type>& v) {
		return (v.X * v.X) + (v.Y * v.Y) + (v.Z * v.Z);
	}

	// NORMALIZE
	template<class Type>
	INLINE static Vector<Type> Normalize(const Vector<Type>& v) {
		const float32 size = Size(v);
		if (size == 0.0f) { return v; }

		const float32 invSize = 1.0f / size;

		return v * invSize;
	}

	// LARGEST VALUE
	template<class Type>
	INLINE static Type Largest(const Vector<Type>& v) {
		Type t = MyMath::Max(v.X, v.Y);
		t = MyMath::Max(t, v.Z);
		return t;
	}

	// SMALEST VALUE
	template<class Type>
	INLINE static Type Smallest(const Vector<Type>& v) {
		Type t = MyMath::Min(v.X, v.Y);
		t = MyMath::Min(t, v.Z);
		return t;
	}

	// CROSS PRODUCT
	template<class Type>
	INLINE static Vector<Type> Cross(const Vector<Type>& u, const Vector<Type>& v) {
		return Vector<Type>(
			u.Y * v.Z - v.Y * u.Z,
			u.Z * v.X - v.Z * u.X,
			u.X * v.Y - v.X * u.Y
		);
	}

	// DOT PRODUCT
	template<class Type>
	INLINE static float32 Dot(const Vector<Type>& u, const Vector<Type>& v) {
		return (u.X * v.X) + (u.Y * v.Y) + (u.Z * v.Z);
	}

	template<class Type>
	INLINE static void MinMax(Type& Max, Type& Min) {
		if (Min > Max) {
			Type aux = Min;
			Min = Max;
			Max = aux;
		}
	}

	INLINE static float32 Max(const float32& a, const float32& b) { return fmaxf(a, b); }
	INLINE static float64 Max(const float64& a, const float64& b) { return fmax(a, b); }

	INLINE static float32 Min(const float32& a, const float32& b) { return fminf(a, b); }
	INLINE static float64 Min(const float64& a, const float64& b) { return fmin(a, b); }

	//MIN MAX
	template<class Type>
	INLINE static void MinMax(Vector<Type>& Max, Vector<Type>& Min) {
		MinMax(Max.X, Min.X);
		MinMax(Max.Y, Min.Y);
		MinMax(Max.Z, Min.Z);
	}

	////////////////////////////////
	//           VECTOR2          //
	////////////////////////////////

	// COS
	template<class Type>
	INLINE static Vector2<Type> Cos(const Vector2<Type>& v) {
		return Vector2<Type>(cosf(v.X), cosf(v.Y));
	}

	// SIN
	template<class Type>
	INLINE static Vector2<Type> Sin(const Vector2<Type>& v) {
		return Vector2<Type>(sinf(v.X), sinf(v.Y));
	}


	// VECTOR AND MATRIX OPERATIONS

	INLINE static FMatrix4x4 LookAt(const FVector& eye, const FVector& center, const FVector& up) {
		
		const FVector f(Normalize(center - eye));

#if COORDINATE_SYSTEM == RIGHT_HANDED_SYSTEM
		//Right Handed
		const FVector s(Normalize(Cross(f, up)));
		const FVector u(Cross(s, f));
#else
		//Left Handed
		const FVector s(Normalize(Cross(up, f)));
		const FVector u(Cross(f, s));
#endif

		FMatrix4x4 result;

		result.V[0][0] = s.X;
		result.V[1][0] = s.Y;
		result.V[2][0] = s.Z;
		result.V[0][1] = u.X;
		result.V[1][1] = u.Y;
		result.V[2][1] = u.Z;

#if COORDINATE_SYSTEM == RIGHT_HANDED_SYSTEM
		//Right Handed
		result.V[0][2] = -f.X;
		result.V[1][2] = -f.Y;
		result.V[2][2] = -f.Z;
		result.V[3][0] = -Dot(s, eye);
		result.V[3][1] = -Dot(u, eye);
		result.V[3][2] = Dot(f, eye);
#else
		//Left Handed
		result.V[0][2] = f.X;
		result.V[1][2] = f.Y;
		result.V[2][2] = f.Z;
		result.V[3][0] = -Dot(s, eye);
		result.V[3][1] = -Dot(u, eye);
		result.V[3][2] = -Dot(f, eye);
#endif
		return result;
	}

	INLINE static FMatrix4x4 Perspective(const float32& fov, const float32& aspectRatio, const float32& nearPlane, const float32& farPlane) {
		const float32 hft = tanf(fov * 0.5f);
		FMatrix4x4 result = FMatrix4x4(0.0f);

		result.V[0][0] = 1.0f / (aspectRatio * hft);
		result.V[1][1] = 1.0f / (hft);

#if COORDINATE_SYSTEM == RIGHT_HANDED_SYSTEM
		//Right Handed
		result.V[2][3] = -1.0f;
		//Right handed
		result.V[2][2] = -(farPlane + nearPlane) / (farPlane - nearPlane);
#else //COORDINATE_SYSTEM == LEFT_HANDED_SYSTEM
		//Left Handed
		result.V[2][3] = 1.0f;
		//Left handed
		result.V[2][2] = (farPlane + nearPlane) / (farPlane - nearPlane);
#endif


		result.V[3][2] = -(2.0f * farPlane * nearPlane) / (farPlane - nearPlane);

		return result;
	}

	INLINE static FMatrix4x4 Inverse(FMatrix4x4& m) {

		float32 det
			= m.V[0].X * m.V[1].Y * m.V[2].Z * m.V[3].W + m.V[0].X * m.V[1].Z * m.V[2].W * m.V[3].Y + m.V[0].X * m.V[1].W * m.V[2].Y * m.V[3].Z
			+ m.V[0].Y * m.V[1].X * m.V[2].W * m.V[3].Z + m.V[0].Y * m.V[1].Z * m.V[2].X * m.V[3].W + m.V[0].Y * m.V[1].W * m.V[2].Z * m.V[3].X
			+ m.V[0].Z * m.V[1].X * m.V[2].Y * m.V[3].W + m.V[0].Z * m.V[1].Y * m.V[2].W * m.V[3].X + m.V[0].Z * m.V[1].W * m.V[2].X * m.V[3].Y
			+ m.V[0].W * m.V[1].X * m.V[2].Z * m.V[3].Y + m.V[0].W * m.V[1].Y * m.V[2].X * m.V[3].Z + m.V[0].W * m.V[1].Z * m.V[2].Y * m.V[3].X
			- m.V[0].X * m.V[1].Y * m.V[2].W * m.V[3].Z - m.V[0].X * m.V[1].Z * m.V[2].Y * m.V[3].W - m.V[0].X * m.V[1].W * m.V[2].Z * m.V[3].Y
			- m.V[0].Y * m.V[1].X * m.V[2].Z * m.V[3].W - m.V[0].Y * m.V[1].Z * m.V[2].W * m.V[3].X - m.V[0].Y * m.V[1].W * m.V[2].X * m.V[3].Z
			- m.V[0].Z * m.V[1].X * m.V[2].W * m.V[3].Y - m.V[0].Z * m.V[1].Y * m.V[2].X * m.V[3].W - m.V[0].Z * m.V[1].W * m.V[2].Y * m.V[3].X
			- m.V[0].W * m.V[1].X * m.V[2].Y * m.V[3].Z - m.V[0].W * m.V[1].Y * m.V[2].Z * m.V[3].X - m.V[0].W * m.V[1].Z * m.V[2].X * m.V[3].Y;
		if (CMP(det, 0.0f)) {
			return FMatrix4x4();
		}
		float32 i_det = 1.0f / det;

		FMatrix4x4 result;
		result.V[0].X = (m.V[1].Y * m.V[2].Z * m.V[3].W + m.V[1].Z * m.V[2].W * m.V[3].Y + m.V[1].W * m.V[2].Y * m.V[3].Z - m.V[1].Y * m.V[2].W * m.V[3].Z - m.V[1].Z * m.V[2].Y * m.V[3].W - m.V[1].W * m.V[2].Z * m.V[3].Y) * i_det;
		result.V[0].Y = (m.V[0].Y * m.V[2].W * m.V[3].Z + m.V[0].Z * m.V[2].Y * m.V[3].W + m.V[0].W * m.V[2].Z * m.V[3].Y - m.V[0].Y * m.V[2].Z * m.V[3].W - m.V[0].Z * m.V[2].W * m.V[3].Y - m.V[0].W * m.V[2].Y * m.V[3].Z) * i_det;
		result.V[0].Z = (m.V[0].Y * m.V[1].Z * m.V[3].W + m.V[0].Z * m.V[1].W * m.V[3].Y + m.V[0].W * m.V[1].Y * m.V[3].Z - m.V[0].Y * m.V[1].W * m.V[3].Z - m.V[0].Z * m.V[1].Y * m.V[3].W - m.V[0].W * m.V[1].Z * m.V[3].Y) * i_det;
		result.V[0].W = (m.V[0].Y * m.V[1].W * m.V[2].Z + m.V[0].Z * m.V[1].Y * m.V[2].W + m.V[0].W * m.V[1].Z * m.V[2].Y - m.V[0].Y * m.V[1].Z * m.V[2].W - m.V[0].Z * m.V[1].W * m.V[2].Y - m.V[0].W * m.V[1].Y * m.V[2].Z) * i_det;
		result.V[1].X = (m.V[1].X * m.V[2].W * m.V[3].Z + m.V[1].Z * m.V[2].X * m.V[3].W + m.V[1].W * m.V[2].Z * m.V[3].X - m.V[1].X * m.V[2].Z * m.V[3].W - m.V[1].Z * m.V[2].W * m.V[3].X - m.V[1].W * m.V[2].X * m.V[3].Z) * i_det;
		result.V[1].Y = (m.V[0].X * m.V[2].Z * m.V[3].W + m.V[0].Z * m.V[2].W * m.V[3].X + m.V[0].W * m.V[2].X * m.V[3].Z - m.V[0].X * m.V[2].W * m.V[3].Z - m.V[0].Z * m.V[2].X * m.V[3].W - m.V[0].W * m.V[2].Z * m.V[3].X) * i_det;
		result.V[1].Z = (m.V[0].X * m.V[1].W * m.V[3].Z + m.V[0].Z * m.V[1].X * m.V[3].W + m.V[0].W * m.V[1].Z * m.V[3].X - m.V[0].X * m.V[1].Z * m.V[3].W - m.V[0].Z * m.V[1].W * m.V[3].X - m.V[0].W * m.V[1].X * m.V[3].Z) * i_det;
		result.V[1].W = (m.V[0].X * m.V[1].Z * m.V[2].W + m.V[0].Z * m.V[1].W * m.V[2].X + m.V[0].W * m.V[1].X * m.V[2].Z - m.V[0].X * m.V[1].W * m.V[2].Z - m.V[0].Z * m.V[1].X * m.V[2].W - m.V[0].W * m.V[1].Z * m.V[2].X) * i_det;
		result.V[2].X = (m.V[1].X * m.V[2].Y * m.V[3].W + m.V[1].Y * m.V[2].W * m.V[3].X + m.V[1].W * m.V[2].X * m.V[3].Y - m.V[1].X * m.V[2].W * m.V[3].Y - m.V[1].Y * m.V[2].X * m.V[3].W - m.V[1].W * m.V[2].Y * m.V[3].X) * i_det;
		result.V[2].Y = (m.V[0].X * m.V[2].W * m.V[3].Y + m.V[0].Y * m.V[2].X * m.V[3].W + m.V[0].W * m.V[2].Y * m.V[3].X - m.V[0].X * m.V[2].Y * m.V[3].W - m.V[0].Y * m.V[2].W * m.V[3].X - m.V[0].W * m.V[2].X * m.V[3].Y) * i_det;
		result.V[2].Z = (m.V[0].X * m.V[1].Y * m.V[3].W + m.V[0].Y * m.V[1].W * m.V[3].X + m.V[0].W * m.V[1].X * m.V[3].Y - m.V[0].X * m.V[1].W * m.V[3].Y - m.V[0].Y * m.V[1].X * m.V[3].W - m.V[0].W * m.V[1].Y * m.V[3].X) * i_det;
		result.V[2].W = (m.V[0].X * m.V[1].W * m.V[2].Y + m.V[0].Y * m.V[1].X * m.V[2].W + m.V[0].W * m.V[1].Y * m.V[2].X - m.V[0].X * m.V[1].Y * m.V[2].W - m.V[0].Y * m.V[1].W * m.V[2].X - m.V[0].W * m.V[1].X * m.V[2].Y) * i_det;
		result.V[3].X = (m.V[1].X * m.V[2].Z * m.V[3].Y + m.V[1].Y * m.V[2].X * m.V[3].Z + m.V[1].Z * m.V[2].Y * m.V[3].X - m.V[1].X * m.V[2].Y * m.V[3].Z - m.V[1].Y * m.V[2].Z * m.V[3].X - m.V[1].Z * m.V[2].X * m.V[3].Y) * i_det;
		result.V[3].Y = (m.V[0].X * m.V[2].Y * m.V[3].Z + m.V[0].Y * m.V[2].Z * m.V[3].X + m.V[0].Z * m.V[2].X * m.V[3].Y - m.V[0].X * m.V[2].Z * m.V[3].Y - m.V[0].Y * m.V[2].X * m.V[3].Z - m.V[0].Z * m.V[2].Y * m.V[3].X) * i_det;
		result.V[3].Z = (m.V[0].X * m.V[1].Z * m.V[3].Y + m.V[0].Y * m.V[1].X * m.V[3].Z + m.V[0].Z * m.V[1].Y * m.V[3].X - m.V[0].X * m.V[1].Y * m.V[3].Z - m.V[0].Y * m.V[1].Z * m.V[3].X - m.V[0].Z * m.V[1].X * m.V[3].Y) * i_det;
		result.V[3].W = (m.V[0].X * m.V[1].Y * m.V[2].Z + m.V[0].Y * m.V[1].Z * m.V[2].X + m.V[0].Z * m.V[1].X * m.V[2].Y - m.V[0].X * m.V[1].Z * m.V[2].Y - m.V[0].Y * m.V[1].X * m.V[2].Z - m.V[0].Z * m.V[1].Y * m.V[2].X) * i_det;

		return result;
		/*float32 Coef00 = m.V[2][2] * m.V[3][3] - m.V[3][2] * m.V[2][3];
		float32 Coef02 = m.V[1][2] * m.V[3][3] - m.V[3][2] * m.V[1][3];
		float32 Coef03 = m.V[1][2] * m.V[2][3] - m.V[2][2] * m.V[1][3];

		float32 Coef04 = m.V[2][1] * m.V[3][3] - m.V[3][1] * m.V[2][3];
		float32 Coef06 = m.V[1][1] * m.V[3][3] - m.V[3][1] * m.V[1][3];
		float32 Coef07 = m.V[1][1] * m.V[2][3] - m.V[2][1] * m.V[1][3];

		float32 Coef08 = m.V[2][1] * m.V[3][2] - m.V[3][1] * m.V[2][2];
		float32 Coef10 = m.V[1][1] * m.V[3][2] - m.V[3][1] * m.V[1][2];
		float32 Coef11 = m.V[1][1] * m.V[2][2] - m.V[2][1] * m.V[1][2];

		float32 Coef12 = m.V[2][0] * m.V[3][3] - m.V[3][0] * m.V[2][3];
		float32 Coef14 = m.V[1][0] * m.V[3][3] - m.V[3][0] * m.V[1][3];
		float32 Coef15 = m.V[1][0] * m.V[2][3] - m.V[2][0] * m.V[1][3];

		float32 Coef16 = m.V[2][0] * m.V[3][2] - m.V[3][0] * m.V[2][2];
		float32 Coef18 = m.V[1][0] * m.V[3][2] - m.V[3][0] * m.V[1][2];
		float32 Coef19 = m.V[1][0] * m.V[2][2] - m.V[2][0] * m.V[1][2];

		float32 Coef20 = m.V[2][0] * m.V[3][1] - m.V[3][0] * m.V[2][1];
		float32 Coef22 = m.V[1][0] * m.V[3][1] - m.V[3][0] * m.V[1][1];
		float32 Coef23 = m.V[1][0] * m.V[2][1] - m.V[2][0] * m.V[1][1];

		FVector4 Fac0(Coef00, Coef00, Coef02, Coef03);
		FVector4 Fac1(Coef04, Coef04, Coef06, Coef07);
		FVector4 Fac2(Coef08, Coef08, Coef10, Coef11);
		FVector4 Fac3(Coef12, Coef12, Coef14, Coef15);
		FVector4 Fac4(Coef16, Coef16, Coef18, Coef19);
		FVector4 Fac5(Coef20, Coef20, Coef22, Coef23);

		FVector4 Vec0(m[1][0], m[0][0], m[0][0], m[0][0]);
		FVector4 Vec1(m[1][1], m[0][1], m[0][1], m[0][1]);
		FVector4 Vec2(m[1][2], m[0][2], m[0][2], m[0][2]);
		FVector4 Vec3(m[1][3], m[0][3], m[0][3], m[0][3]);

		FVector4 Inv0(Vec1 * Fac0 - Vec2 * Fac1 + Vec3 * Fac2);
		FVector4 Inv1(Vec0 * Fac0 - Vec2 * Fac3 + Vec3 * Fac4);
		FVector4 Inv2(Vec0 * Fac1 - Vec1 * Fac3 + Vec3 * Fac5);
		FVector4 Inv3(Vec0 * Fac2 - Vec1 * Fac4 + Vec2 * Fac5);

		FVector4 SignA(+1, -1, +1, -1);
		FVector4 SignB(-1, +1, -1, +1);
		FMatrix4x4 Inverse(Inv0 * SignA, Inv1 * SignB, Inv2 * SignA, Inv3 * SignB);

		FVector4 Row0(Inverse[0][0], Inverse[1][0], Inverse[2][0], Inverse[3][0]);

		FVector4 Dot0(m[0] * Row0);
		float32 Dot1 = (Dot0.X + Dot0.Y) + (Dot0.Z + Dot0.W);

		float32 OneOverDeterminant = 1.0f / Dot1;

		return Inverse * OneOverDeterminant;*/
	}

};

#endif