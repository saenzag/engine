#ifndef __VECTOR_H__
#define __VECTOR_H__ 1

#include "Typedef.h"

////////////////////////////////////////////////////////////////
////////////////            VECTOR 3            ////////////////
////////////////////////////////////////////////////////////////

template<class Type>
struct Vector {
public:
	union {
		struct { Type X, Y, Z; };
		struct { Type R, G, B; };
		struct { Type V[3]; };
	};

	//CONSTRUCTOR
	Vector() : X((Type)0), Y((Type)0), Z((Type)0) {};
	Vector(Type scalar) : X(scalar), Y(scalar), Z(scalar) {};
	Vector(Type x, Type y, Type z) : X(x), Y(y), Z(z) {};
	Vector(Type* pointer) : X(pointer[0]), Y(pointer[1]), Z(pointer[2]) {};

	//Copy Constructor
	Vector(const Vector& v) : X(v.X), Y(v.Y), Z(v.Z) {};

	// GLOBAL VECTORS float32 precision
	static const Vector<float32> Zero;    //(0,0,0)
	static const Vector<float32> One;   //(1,1,1)
	static const Vector<float32> Right;   //(1,0,0)
	static const Vector<float32> Up;    //(0,1,0)
	static const Vector<float32> Forward; //(0,0,1)

	//Operator Declarations
	//Equal
	Vector& operator=(const Vector& v);
	//Add
	Vector operator+(const Vector& v) const;
	Vector operator+(const Type& scalar) const;
	Vector& operator+=(const Vector& v);
	Vector& operator+=(const Type& scalar);

	//Substract
	Vector operator-(const Vector& v) const;
	Vector operator-(const Type& scalar) const;
	Vector& operator-=(const Vector& v);
	Vector& operator-=(const Type& scalar);
	Vector operator-() const;

	//Multiplication
	Vector operator*(const Type& scalar) const;
	Vector& operator*=(const Type& scalar);

	//Division
	Vector operator/(const Type& scalar) const;
	Vector& operator/=(const Type& scalar);

	//Comparations
	int8 operator==(const Vector& v) const;
	int8 operator!=(const Vector& v) const;

	//Indexer
	Type& operator[](const int32& n);
};

////////////////////////////////////////////////////////////////
////////////////           !VECTOR 3            ////////////////
////////////////////////////////////////////////////////////////

//TYPEDEFS
typedef Vector<int8>  BVector;
typedef Vector<uint8> UBVector;
typedef Vector<int16> SVector;
typedef Vector<uint16>  USVector;
typedef Vector<int32> IVector;
typedef Vector<uint32>  UIVector;

typedef Vector<float32> FVector;
typedef Vector<float64> DVector;

//GLOBAL VECTORS DECLARATION
const Vector<float32> Vector<float32>::Zero(0.0f);
const Vector<float32> Vector<float32>::One(1.0f);
const Vector<float32> Vector<float32>::Right(1.0f, 0.0f, 0.0f);
const Vector<float32> Vector<float32>::Up(0.0f, 1.0f, 0.0f);
const Vector<float32> Vector<float32>::Forward(0.0f, 0.0f, 1.0f);

//OPERATORS
//EQUAL
template<class Type>
INLINE Vector<Type>& Vector<Type>::operator=(const Vector<Type>& v) {
	this->X = v.X;
	this->Y = v.Y;
	this->Z = v.Z;
	return *this;
}
// ADD
template<class Type>
INLINE Vector<Type> Vector<Type>::operator+(const Vector<Type>& v) const {
	return Vector<Type>(X + v.X, Y + v.Y, Z + v.Z);
}

template<class Type>
INLINE Vector<Type> Vector<Type>::operator+(const Type& scalar) const {
	return Vector<Type>(X + scalar, Y + scalar, Z + scalar);
}

template<class Type>
INLINE Vector<Type>& Vector<Type>::operator+=(const Vector<Type>& v) {
	X += v.X; Y += v.Y; Z += v.Z;
	return *this;
}

template<class Type>
INLINE Vector<Type>& Vector<Type>::operator+=(const Type& scalar) {
	X += scalar; Y += scalar; Z += scalar;
	return *this;
}

// SUBSTRACT
template<class Type>
INLINE Vector<Type> Vector<Type>::operator-(const Vector<Type>& v) const {
	return Vector<Type>(X - v.X, Y - v.Y, Z - v.Z);
}

template<class Type>
INLINE Vector<Type> Vector<Type>::operator-(const Type& scalar) const {
	return Vector<Type>(X - scalar, Y - scalar, Z - scalar);
}

template<class Type>
INLINE Vector<Type>& Vector<Type>::operator-=(const Vector<Type>& v) {
	X -= v.X;
	Y -= v.Y;
	Z -= v.Z;
	return *this;
}

template<class Type>
INLINE Vector<Type>& Vector<Type>::operator-=(const Type& scalar) {
	X -= scalar;
	Y -= scalar;
	Z -= scalar;
	return *this;
}

template<class Type>
INLINE Vector<Type> Vector<Type>::operator-() const {
	return Vector(-X, -Y, -Z);
}

// MULTIPLY
template<class Type>
INLINE Vector<Type> Vector<Type>::operator*(const Type& scalar) const {
	return Vector<Type>(X * scalar, Y * scalar, Z * scalar);
}

template<class Type>
INLINE Vector<Type>& Vector<Type>::operator*=(const Type& scalar) {
	X *= scalar; Y *= scalar; Z *= scalar;
	return *this;
}

//DIVIDE
template<class Type>
INLINE Vector<Type> Vector<Type>::operator/(const Type& scalar) const {
	const Type invScalar = (Type(1)) / scalar;
	return Vector<Type><Type>(X * invScalar, Y * invScalar, Z * invScalar);
}

template<class Type>
INLINE Vector<Type>& Vector<Type>::operator/=(const Type& scalar) {
	const Type invScalar = ((Type)1) / scalar;
	X *= invScalar;
	Y *= invScalar;
	Z *= invScalar;
	return *this;
}

//COMPARATORS
template<class Type>
INLINE int8 Vector<Type>::operator==(const Vector<Type>& v) const {
	return (X == v.X && Y == v.Y && Z == v.Z);
}

template<class Type>
INLINE int8 Vector<Type>::operator!=(const Vector<Type>& v) const {
	return (X != v.X || Y != v.Y || Z != v.Z);
}

//INDEXER
template<class Type>
INLINE Type& Vector<Type>::operator[](const int32& n) {
	return this->V[n];
}

#endif