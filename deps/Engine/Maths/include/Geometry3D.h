
#ifndef __GEOMETRY3D_H__
#define __GEOMETRY3D_H__ 1

// Helpers for collider functions
#include "Typedef.h"
#include "MyMath.h"
#include "Vector.h"
#include "Collider.h"
#include <vector>

using namespace ENGINENAME;

#define CMP(x, y) \
	(fabsf(x - y) <= FLT_EPSILON * fmaxf(1.0f, fmaxf(fabsf(x), fabsf(y))))

struct Interval {
	float32 min, max;
};

struct Line {
	Line(const FVector& s, const FVector& e) : start(s), end(e) {};
	FVector start;
	FVector end;
};

struct Plane {
	FVector normal;
	float32 distance;

	Plane() : normal(1.0f, 0.0f, 0.0f) { };
	Plane(const FVector& n, const float32& d) :
		normal(n), distance(d) { };
};

Interval GetInterval(const Collider* box, const FVector& axis) {
	FVector vertex[8];

	FVector C = box->GetOwnerTransform()->GetPosition();	// OBB Center
	FVector E = box->GetSize();		// OBB Extents
	FMatrix4x4 o = box->GetOwnerTransform()->GetMatrix();
	FVector A[] = {			// OBB Axis
		FVector(o[0].X, o[0].Y, o[0].Z),
		FVector(o[1].X, o[1].Y, o[1].Z),
		FVector(o[2].X, o[2].Y, o[2].Z)
	};

	vertex[0] = C + A[0] * E[0] + A[1] * E[1] + A[2] * E[2];
	vertex[1] = C - A[0] * E[0] + A[1] * E[1] + A[2] * E[2];
	vertex[2] = C + A[0] * E[0] - A[1] * E[1] + A[2] * E[2];
	vertex[3] = C + A[0] * E[0] + A[1] * E[1] - A[2] * E[2];
	vertex[4] = C - A[0] * E[0] - A[1] * E[1] - A[2] * E[2];
	vertex[5] = C + A[0] * E[0] - A[1] * E[1] - A[2] * E[2];
	vertex[6] = C - A[0] * E[0] + A[1] * E[1] - A[2] * E[2];
	vertex[7] = C - A[0] * E[0] - A[1] * E[1] + A[2] * E[2];

	Interval result;
	result.min = result.max = MyMath::Dot(axis, vertex[0]);

	for (int32 i = 1; i < 8; ++i) {
		float32 projection = MyMath::Dot(axis, vertex[i]);
		result.min = (projection < result.min) ? projection : result.min;
		result.max = (projection > result.max) ? projection : result.max;
	}

	return result;
}

float32 PenetrationDepthBoxToBox(const Collider* box1, const Collider* box2, const FVector& axis, char8* outShouldFlip) {
	Interval i1 = GetInterval(box1, MyMath::Normalize(axis));
	Interval i2 = GetInterval(box2, MyMath::Normalize(axis));

	if (!((i2.min <= i1.max) && (i1.min <= i2.max))) {
		return 0.0f; // No penerattion
	}

	float32 len1 = i1.max - i1.min;
	float32 len2 = i2.max - i2.min;
	float32 min = fminf(i1.min, i2.min);
	float32 max = fmaxf(i1.max, i2.max);
	float32 length = max - min;

	if (outShouldFlip != 0) {
		*outShouldFlip = (i2.min < i1.min);
	}

	return (len1 + len2) - length;
}

std::vector<FVector> GetVertices(const Collider* box) {
	std::vector<FVector> v;
	v.resize(8);

	FVector C = box->GetOwnerTransform()->GetPosition();	// OBB Center
	FVector E = box->GetSize();		// OBB Extents
	FMatrix4x4 o = box->GetOwnerTransform()->GetMatrix();
	FVector A[] = {			// OBB Axis
		FVector(o[0].X, o[0].Y, o[0].Z),
		FVector(o[1].X, o[1].Y, o[1].Z),
		FVector(o[2].X, o[2].Y, o[2].Z)
	};

	v[0] = C + A[0] * E[0] + A[1] * E[1] + A[2] * E[2];
	v[1] = C - A[0] * E[0] + A[1] * E[1] + A[2] * E[2];
	v[2] = C + A[0] * E[0] - A[1] * E[1] + A[2] * E[2];
	v[3] = C + A[0] * E[0] + A[1] * E[1] - A[2] * E[2];
	v[4] = C - A[0] * E[0] - A[1] * E[1] - A[2] * E[2];
	v[5] = C + A[0] * E[0] - A[1] * E[1] - A[2] * E[2];
	v[6] = C - A[0] * E[0] + A[1] * E[1] - A[2] * E[2];
	v[7] = C - A[0] * E[0] - A[1] * E[1] + A[2] * E[2];

	return v;
}

std::vector<Line> GetEdges(const Collider* box) {
	std::vector<Line> result;
	result.reserve(12);
	std::vector<FVector> v = GetVertices(box);

	int32 index[][2] = { // Indices of edges
		{ 6, 1 },{ 6, 3 },{ 6, 4 },{ 2, 7 },{ 2, 5 },{ 2, 0 },
		{ 0, 1 },{ 0, 3 },{ 7, 1 },{ 7, 4 },{ 4, 5 },{ 5, 3 }
	};

	for (int32 i = 0; i < 12; ++i) {
		result.push_back(Line(
			v[index[i][0]], v[index[i][1]]
		));
	}

	return result;
}

std::vector<Plane> GetPlanes(const Collider* box) {
	FVector C = box->GetOwnerTransform()->GetPosition();	// OBB Center
	FVector E = box->GetSize();		// OBB Extents
	FMatrix4x4 o = box->GetOwnerTransform()->GetMatrix();
	FVector A[] = {			// OBB Axis
		FVector(o[0].X, o[0].Y, o[0].Z),
		FVector(o[1].X, o[1].Y, o[1].Z),
		FVector(o[2].X, o[2].Y, o[2].Z)
	};

	std::vector<Plane> result;
	result.resize(6);

	result[0] = Plane(A[0], MyMath::Dot(A[0], (C + A[0] * E.X)));
	result[1] = Plane(A[0] * -1.0f, -MyMath::Dot(A[0], (C - A[0] * E.X)));
	result[2] = Plane(A[1], MyMath::Dot(A[1], (C + A[1] * E.Y)));
	result[3] = Plane(A[1] * -1.0f, -MyMath::Dot(A[1], (C - A[1] * E.Y)));
	result[4] = Plane(A[2], MyMath::Dot(A[2], (C + A[2] * E.Z)));
	result[5] = Plane(A[2] * -1.0f, -MyMath::Dot(A[2], (C - A[2] * E.Z)));

	return result;
}

char8 ClipToPlane(const Plane& plane, const Line& line, FVector* outPoint) {
	FVector ab = line.end - line.start;

	float32 nA = MyMath::Dot(plane.normal, line.start);
	float32 nAB = MyMath::Dot(plane.normal, ab);

	if (CMP(nAB, 0)) {
		return false;
	}

	float32 t = (plane.distance - nA) / nAB;
	if (t >= 0.0f && t <= 1.0f) {
		if (outPoint != 0) {
			*outPoint = line.start + ab * t;
		}
		return true;
	}

	return false;
}

char8 PointInOBB(const FVector& point, const Collider* box) {
	Transform* tr = box->GetOwnerTransform();
	FVector dir = point - tr->GetPosition();

	FMatrix4x4 orientation = tr->GetMatrix();
	for (int32 i = 0; i < 3; ++i) {
		FVector axis(orientation[i].X, orientation[i].Y, orientation[i].Z);

		float32 distance = MyMath::Dot(dir, axis);

		if (distance > box->GetSize()[i]) {
			return false;
		}
		if (distance < -box->GetSize()[i]) {
			return false;
		}
	}

	return true;
}

std::vector<FVector> ClipEdgesToOBB(const std::vector<Line>& edges, const Collider* box) {
	std::vector<FVector> result;
	result.reserve(edges.size() * 3);
	FVector intersection;

	std::vector<Plane>& planes = GetPlanes(box);

	for (int i = 0; i < planes.size(); ++i) {
		for (int j = 0; j < edges.size(); ++j) {
			if (ClipToPlane(planes[i], edges[j], &intersection)) {
				if (PointInOBB(intersection, box)) {
					result.push_back(intersection);
				}
			}
		}
	}

	return result;
}

#endif // !__GEOMETRY3D_H__
