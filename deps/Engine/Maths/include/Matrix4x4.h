
#ifndef __MATRIX4X4_H__
#define __MATRIX4X4_H__ 1

#include "Typedef.h"
#include "Vector.h"
#include "Vector4.h"
#include <math.h>

template<class Type>
struct Matrix4x4 {
public:

	Vector4<Type> V[4];

	//CONSTRUCTORS
	Matrix4x4() {
		V[0] = Vector4<Type>((Type)1, (Type)0, (Type)0, (Type)0);
		V[1] = Vector4<Type>((Type)0, (Type)1, (Type)0, (Type)0);
		V[2] = Vector4<Type>((Type)0, (Type)0, (Type)1, (Type)0);
		V[3] = Vector4<Type>((Type)0, (Type)0, (Type)0, (Type)1);
	};

	Matrix4x4(float32 scalar) {
		V[0] = Vector4<Type>((Type)scalar, (Type)scalar, (Type)scalar, (Type)scalar);
		V[1] = Vector4<Type>((Type)scalar, (Type)scalar, (Type)scalar, (Type)scalar);
		V[2] = Vector4<Type>((Type)scalar, (Type)scalar, (Type)scalar, (Type)scalar);
		V[3] = Vector4<Type>((Type)scalar, (Type)scalar, (Type)scalar, (Type)scalar);
	};

	Matrix4x4(
		float32 x0, float32 y0, float32 z0, float32 w0,
		float32 x1, float32 y1, float32 z1, float32 w1,
		float32 x2, float32 y2, float32 z2, float32 w2,
		float32 x3, float32 y3, float32 z3, float32 w3) {

		V[0] = Vector4<Type>((Type)x0, (Type)y0, (Type)z0, (Type)w0);
		V[1] = Vector4<Type>((Type)x1, (Type)y1, (Type)z1, (Type)w1);
		V[2] = Vector4<Type>((Type)x2, (Type)y2, (Type)z2, (Type)w2);
		V[3] = Vector4<Type>((Type)x3, (Type)y3, (Type)z3, (Type)w3);
	}

	Matrix4x4(Vector4<Type> v0, Vector4<Type> v1, Vector4<Type> v2, Vector4<Type> v3) {
		V[0] = v0;
		V[1] = v1;
		V[2] = v2;
		V[3] = v3;
	}

	//Copy Constructor
	Matrix4x4(const Matrix4x4& m) {
		V[0] = m.V[0];
		V[1] = m.V[1];
		V[2] = m.V[2];
		V[3] = m.V[3];
	}

	/////////////// FUNCTIONS \\\\\\\\\\\\\\\

	// Translates Matrix
	INLINE static void Translate(Matrix4x4& m, FVector& v) {
		m.V[3] = m.V[0] * v.X + m.V[1] * v.Y + m.V[2] * v.Z + m.V[3];
	}

	// Scales Matrix
	INLINE static void Scale(Matrix4x4& m, FVector& v) {
		Matrix4x4 result;
		result.V[0] = m.V[0] * v.X;
		result.V[1] = m.V[1] * v.Y;
		result.V[2] = m.V[2] * v.Z;
		result.V[3] = m.V[3];

		m = result;
	}

	/////////////// OPERATORS \\\\\\\\\\\\\\\
	  
	// Equal
	Matrix4x4 Matrix4x4::operator=(const Matrix4x4& m);

	// Add
	Matrix4x4 Matrix4x4::operator+(const Matrix4x4& m);
	Matrix4x4 Matrix4x4::operator+=(const Matrix4x4& m);

	// Substract
	Matrix4x4 Matrix4x4::operator-(const Matrix4x4& m);
	Matrix4x4 Matrix4x4::operator-=(const Matrix4x4& m);

	// Scalar Multiplication
	//Matrix4x4 Matrix4x4::operator*(const Type& n);
	//Matrix4x4& Matrix4x4::operator*=(const Type& n);

	// Scalar Division
	//Matrix4x4 Matrix4x4::operator/(const Type& n);
	//Matrix4x4& Matrix4x4::operator/=(const Type& n);

	// Matrix Multiplication
	Matrix4x4 Matrix4x4::operator*(const Matrix4x4& m);
	Matrix4x4 Matrix4x4::operator*=(const Matrix4x4& m);


	Vector4<Type>& Matrix4x4::operator[](const int32& i);
};

//TYPEDEFS
typedef Matrix4x4<int8>   BMatrix4x4;
typedef Matrix4x4<uint8>  UBMatrix4x4;
typedef Matrix4x4<int16>  SMatrix4x4;
typedef Matrix4x4<uint16> USMatrix4x4;
typedef Matrix4x4<int32>  IMatrix4x4;
typedef Matrix4x4<uint32> UIMatrix4x4;

typedef Matrix4x4<float32> FMatrix4x4;
typedef Matrix4x4<float64> DMatrix4x4;

// Equal
template<class Type>
INLINE Matrix4x4<Type> Matrix4x4<Type>::operator=(const Matrix4x4& m) {
	this->V[0] = m.V[0];
	this->V[1] = m.V[1];
	this->V[2] = m.V[2];
	this->V[3] = m.V[3];

	return *this;
}

// Addition
template<class Type>
INLINE Matrix4x4<Type> Matrix4x4<Type>::operator+(const Matrix4x4<Type>& m) {
	return Matrix4x4<Type>(
		this->V[0] + m.V[0],
		this->V[1] + m.V[1],
		this->V[2] + m.V[2],
		this->V[3] + m.V[3]
		);
}

template<class Type>
INLINE Matrix4x4<Type> Matrix4x4<Type>::operator+=(const Matrix4x4<Type>& m) {
	this->V[0] += m.V[0];
	this->V[1] += m.V[1];
	this->V[2] += m.V[2];
	this->V[3] += m.V[3];

	return *this;
}

// Substract
template<class Type>
INLINE Matrix4x4<Type> Matrix4x4<Type>::operator-(const Matrix4x4<Type>& m) {
	return Matrix4x4<Type>(
		this->V[0] - m.V[0],
		this->V[1] - m.V[1],
		this->V[2] - m.V[2],
		this->V[3] - m.V[3]
		);
}

template<class Type>
INLINE Matrix4x4<Type> Matrix4x4<Type>::operator-=(const Matrix4x4<Type>& m) {
	this->V[0] -= m.V[0];
	this->V[1] -= m.V[1];
	this->V[2] -= m.V[2];
	this->V[3] -= m.V[3];

	return *this;
}

/*
// Scalar multiplication
template<class Type>
INLINE Matrix4x4<Type> Matrix4x4<Type>::operator*(const Type& n) {
const Type value = n;
return Matrix4x4<Type>(
this->V[0] * value,
this->V[1] * value,
this->V[2] * value,
this->V[3] * value
);
}

template<class Type>
INLINE Matrix4x4<Type>& Matrix4x4<Type>::operator*=(const Type& n) {
const Type value = n;
this->V[0] *= value;
this->V[1] *= value;
this->V[2] *= value;
this->V[3] *= value;

return *this;
}

// Scalar division
template<class Type>
INLINE Matrix4x4<Type> Matrix4x4<Type>::operator/(const Type& n) {
const Type scalarInv = ((Type)1) / n;
return Matrix4x4<Type>(
this->V[0] * scalarInv,
this->V[1] * scalarInv,
this->V[2] * scalarInv,
this->V[3] * scalarInv
);
}

template<class Type>
INLINE Matrix4x4<Type>& Matrix4x4<Type>::operator/=(const Type& n) {
const Matrix4x4<Type> scalarInv = ((Type)1) / n;

this->V[0] *= scalarInv;
this->V[1] *= scalarInv;
this->V[2] *= scalarInv;
this->V[3] *= scalarInv;

return *this;
}
//*/

// Matrix multiplication
template<class Type>
INLINE Matrix4x4<Type> Matrix4x4<Type>::operator*(const Matrix4x4<Type>& m) {
	const Vector4<Type> m10 = this->V[0];
	const Vector4<Type> m11 = this->V[1];
	const Vector4<Type> m12 = this->V[2];
	const Vector4<Type> m13 = this->V[3];

	const Vector4<Type> m20 = m.V[0];
	const Vector4<Type> m21 = m.V[1];
	const Vector4<Type> m22 = m.V[2];
	const Vector4<Type> m23 = m.V[3];

	Matrix4x4<Type> result;

	result.V[0] = m10 * m20.X + m11 * m20.Y + m12 * m20.Z + m13 * m20.W;
	result.V[1] = m10 * m21.X + m11 * m21.Y + m12 * m21.Z + m13 * m21.W;
	result.V[2] = m10 * m22.X + m11 * m22.Y + m12 * m22.Z + m13 * m22.W;
	result.V[3] = m10 * m23.X + m11 * m23.Y + m12 * m23.Z + m13 * m23.W;

	return result;
}

template<class Type>
INLINE Matrix4x4<Type> Matrix4x4<Type>::operator*=(const Matrix4x4<Type>& m) {
	const Vector4<Type> m20 = m.V[0];
	const Vector4<Type> m21 = m.V[1];
	const Vector4<Type> m22 = m.V[2];
	const Vector4<Type> m23 = m.V[3];

	Matrix4x4<Type> result;

	result[0] = m.V[0] * m20.X + m.V[1] * m20.Y + m.V[2] * m20.Z + m.V[3] * m20.W;
	result[1] = m.V[0] * m21.X + m.V[1] * m21.Y + m.V[2] * m21.Z + m.V[3] * m21.W;
	result[2] = m.V[0] * m22.X + m.V[1] * m22.Y + m.V[2] * m22.Z + m.V[3] * m22.W;
	result[3] = m.V[0] * m23.X + m.V[1] * m23.Y + m.V[2] * m23.Z + m.V[3] * m23.W;

	*this = result;

	return *this;
}

template<class Type>
INLINE Vector<Type> operator*(const Matrix4x4<Type>& m, const Vector<Type>& v) {
	Vector4<Type> v4(v.X, v.Y, v.Z, (Type)1);
	v4 = m * v4;
	return Vector<Type>(v4.X, v4.Y, v4.Z);
};

template<class Type>
INLINE Vector4<Type> operator*(const Matrix4x4<Type>& m, const Vector4<Type>& v) {

	const Vector4<Type> mov0(v.X);
	const Vector4<Type> mov1(v.Y);
	const Vector4<Type> mul0 = m.V[0] * mov0;
	const Vector4<Type> mul1 = m.V[1] * mov1;
	const Vector4<Type> add0 = mul0 + mul1;
	const Vector4<Type> mov2(v.Z);
	const Vector4<Type> mov3(v.W);
	const Vector4<Type> mul2 = m.V[2] * mov2;
	const Vector4<Type> mul3 = m.V[3] * mov3;
	const Vector4<Type> add1 = mul2 + mul3;
	const Vector4<Type> add2 = add0 + add1;

	return add2;
};

template<class Type>
INLINE Vector4<Type>& Matrix4x4<Type>::operator[](const int32& i) {
	return this->V[i];
}

#endif