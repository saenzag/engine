#ifndef __VECTOR2_H__
#define __VECTOR2_H__ 1

#include "Typedef.h"

////////////////////////////////////////////////////////////////
////////////////            VECTOR 2            ////////////////
////////////////////////////////////////////////////////////////

template<class Type>
struct Vector2 {
public:
	union {
		struct { Type X, Y; };
		struct { Type R, G; };
		struct { Type V[2]; };
	};

	//CONSTRUCTORS
	Vector2() : X((Type)0), Y((Type)0) {};
	Vector2(Type scalar) : X(scalar), Y(scalar) {};
	Vector2(Type x, Type y) : X(x), Y(y){};

	//Copy Constructor
	Vector2(const Vector2& v) : X(v.X), Y(v.Y) {};

	// GLOBAL VECTORS float32 precision
	static const Vector2<float32> Zero;    //(0,0)
	static const Vector2<float32> One;   //(1,1)
	static const Vector2<float32> Right;   //(1,0)
	static const Vector2<float32> Up;    //(0,1)

	//Operator Declarations
	//Add
	Vector2 operator+(const Vector2& v) const;
	Vector2 operator+(const Type& scalar) const;
	Vector2& operator+=(const Vector2& v);
	Vector2& operator+=(const Type& scalar);

	//Substract
	Vector2 operator-(const Vector2& v) const;
	Vector2 operator-(const Type& scalar) const;
	Vector2& operator-=(const Vector2& v);
	Vector2& operator-=(const Type& scalar);
	Vector2 operator-() const;

	//Multiplication
	Vector2 operator*(const Type& scalar) const;
	Vector2 operator*(const Vector2& v) const;
	Vector2& operator*=(const Vector2& v);
	Vector2& operator*=(const Type& scalar);

	//Division
	Vector2 operator/(const Type& scalar) const;
	Vector2& operator/=(const Vector2& v);
	Vector2& operator/=(const Type& scalar);

	//Comparations
	int8 operator==(const Vector2& v) const;
	int8 operator!=(const Vector2& v) const;

	//Indexer
	Type& operator[](const int32& n);
};

////////////////////////////////////////////////////////////////
////////////////           !VECTOR 3            ////////////////
////////////////////////////////////////////////////////////////

//TYPEDEFS
typedef Vector2<int8>  BVector2;
typedef Vector2<uint8> UBVector2;
typedef Vector2<int16> SVector2;
typedef Vector2<uint16>  USVector2;
typedef Vector2<int32> IVector2;
typedef Vector2<uint32>  UIVector2;

typedef Vector2<float32> FVector2;
typedef Vector2<float64> DVector2;

//GLOBAL VECTORS DECLARATION
const Vector2<float32> Vector2<float32>::Zero(0.0f);
const Vector2<float32> Vector2<float32>::One(1.0f);
const Vector2<float32> Vector2<float32>::Right(1.0f, 0.0f);
const Vector2<float32> Vector2<float32>::Up(0.0f, 1.0f);

//OPERATORS
// ADD
template<class Type>
INLINE Vector2<Type> Vector2<Type>::operator+(const Vector2<Type>& v) const {
	return Vector2<Type>(X + v.X, Y + v.Y);
}

template<class Type>
INLINE Vector2<Type> Vector2<Type>::operator+(const Type& scalar) const {
	return Vector2<Type>(X + scalar, Y + scalar);
}

template<class Type>
INLINE Vector2<Type>& Vector2<Type>::operator+=(const Vector2<Type>& v) {
	X += v.X; Y += v.Y;
	return *this;
}

template<class Type>
INLINE Vector2<Type>& Vector2<Type>::operator+=(const Type& scalar) {
	X += scalar; Y += scalar;
	return *this;
}

// SUBSTRACT
template<class Type>
INLINE Vector2<Type> Vector2<Type>::operator-(const Vector2<Type>& v) const {
	return Vector2<Type>(X - v.X, Y - v.Y);
}

template<class Type>
INLINE Vector2<Type> Vector2<Type>::operator-(const Type& scalar) const {
	return Vector2<Type>(X - scalar, Y - scalar);
}

template<class Type>
INLINE Vector2<Type>& Vector2<Type>::operator-=(const Vector2<Type>& v) {
	X -= v.X;
	Y -= v.Y;
	return *this;
}

template<class Type>
INLINE Vector2<Type>& Vector2<Type>::operator-=(const Type& scalar) {
	X -= scalar;
	Y -= scalar;
	return *this;
}

template<class Type>
INLINE Vector2<Type> Vector2<Type>::operator-() const {
	return Vector2(-X, -Y);
}

// MULTIPLY
template<class Type>
INLINE Vector2<Type> Vector2<Type>::operator*(const Type& scalar) const {
	return Vector2<Type>(X * scalar, Y * scalar);
}

template<class Type>
INLINE Vector2<Type> Vector2<Type>::operator*(const Vector2<Type>& v) const {
	return Vector2<Type>(X * v.X, Y * v.Y);
}

template<class Type>
INLINE Vector2<Type>& Vector2<Type>::operator*=(const Vector2<Type>& v) {
	X *= v.X; Y *= v.Y;
	return *this;
}

template<class Type>
INLINE Vector2<Type>& Vector2<Type>::operator*=(const Type& scalar) {
	X *= scalar; Y *= scalar;
	return *this;
}

//DIVIDE
template<class Type>
INLINE Vector2<Type> Vector2<Type>::operator/(const Type& scalar) const {
	const Type invScalar = (Type(1)) / scalar;
	return Vector2<Type><Type>(X * invScalar, Y * invScalar);
}

template<class Type>
INLINE Vector2<Type>& Vector2<Type>::operator/=(const Vector2<Type>& v) {
	X /= v.X; Y /= v.Y;
	return *this;
}

template<class Type>
INLINE Vector2<Type>& Vector2<Type>::operator/=(const Type& scalar) {
	const Type invScalar = ((Type)1) / scalar;
	X *= invScalar;
	Y *= invScalar;
	return *this;
}

//COMPARATORS
template<class Type>
INLINE int8 Vector2<Type>::operator==(const Vector2<Type>& v) const {
	return (X == v.X && Y == v.Y);
}

template<class Type>
INLINE int8 Vector2<Type>::operator!=(const Vector2<Type>& v) const {
	return (X != v.X || Y != v.Y);
}

//INDEXER
template<class Type>
INLINE Type& Vector2<Type>::operator[](const int32& n) {
	return this->V[n];
}

#endif