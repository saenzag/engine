#ifndef __VECTOR4_H__
#define __VECTOR4_H__ 1

#include "Typedef.h"

////////////////////////////////////////////////////////////////
////////////////            VECTOR 4            ////////////////
////////////////////////////////////////////////////////////////

template<class Type>
struct Vector4 {
public:
	union {
		struct { Type X, Y, Z, W; };
		struct { Type R, G, B, A; };
		struct { Type V[4]; };
	};

	//CONSTRUCTORS
	Vector4() : X((Type)0), Y((Type)0), Z((Type)0), W((Type)0) {};
	Vector4(Type scalar) : X(scalar), Y(scalar), Z(scalar), W(scalar) {};
	Vector4(Type x, Type y, Type z, Type w) : X(x), Y(y), Z(z), W(w) {};

	//Copy Constructor
	Vector4(const Vector4& v) : X(v.X), Y(v.Y), Z(v.Z), W(v.W) {};

	// GLOBAL VECTORS float32 precision
	static const Vector4<float32> Zero;   //(0,0,0,0)
	static const Vector4<float32> One;    //(1,1,1,1)

	static const Vector4<float32> Right;  //(1,0,0,0)
	static const Vector4<float32> Up;   //(0,1,0,0)
	static const Vector4<float32> Forward;  //(0,0,1,0)

											//Operator Declarations
											//Equal
	Vector4& operator=(const Vector4& v);
	//Add
	Vector4 operator+(const Vector4& v) const;
	Vector4 operator+(const Type& scalar) const;
	Vector4& operator+=(const Vector4& v);
	Vector4& operator+=(const Type& scalar);

	//Substract
	Vector4 operator-(const Vector4& v) const;
	Vector4 operator-(const Type& scalar) const;
	Vector4& operator-=(const Vector4& v);
	Vector4& operator-=(const Type& scalar);
	Vector4 operator-() const;

	//Multiplication
	Vector4 operator*(const Type& scalar) const;
	Vector4 operator*(const Vector4& v) const;
	Vector4& operator*=(const Vector4& v);
	Vector4& operator*=(const Type& scalar);

	//Division
	Vector4 operator/(const Type& scalar) const;
	Vector4& operator/=(const Vector4& v);
	Vector4& operator/=(const Type& scalar);

	//Comparations
	int8 operator==(const Vector4& v) const;
	int8 operator!=(const Vector4& v) const;

	//Indexer
	Type& operator[](const int32& n);
};


////////////////////////////////////////////////////////////////
////////////////           !VECTOR 4            ////////////////
////////////////////////////////////////////////////////////////


//TYPEDEFS
typedef Vector4<int8> BVector4;
typedef Vector4<uint8>  UBVector4;
typedef Vector4<int16>  SVector4;
typedef Vector4<uint16> USVector4;
typedef Vector4<int32>  IVector4;
typedef Vector4<uint32> UIVector4;

typedef Vector4<float32> FVector4;
typedef Vector4<float64> DVector4;

//GLOBAL VECTORS DECLARATION
const Vector4<float32> Vector4<float32>::Zero(0.0f);
const Vector4<float32> Vector4<float32>::One(1.0f);

const Vector4<float32> Vector4<float32>::Right(1.0f, 0.0f, 0.0f, 0.0f);
const Vector4<float32> Vector4<float32>::Up(0.0f, 1.0f, 0.0f, 0.0f);
const Vector4<float32> Vector4<float32>::Forward(0.0f, 0.0f, 1.0f, 0.0f);

//OPERATORS
//Equal
template<class Type>
INLINE Vector4<Type>& Vector4<Type>::operator=(const Vector4<Type>& v) {
	this->X = v.X;
	this->Y = v.Y;
	this->Z = v.Z;
	this->W = v.W;

	return *this;
}

//Add
template<class Type>
INLINE Vector4<Type> Vector4<Type>::operator+(const Vector4<Type>& v) const {
	return Vector4<Type>(X + v.X, Y + v.Y, Z + v.Z, W + v.W);
}

template<class Type>
INLINE Vector4<Type> Vector4<Type>::operator+(const Type& scalar) const {
	return Vector4<Type>(X + scalar, Y + scalar, Z + scalar, W + scalar);
}

template<class Type>
INLINE Vector4<Type>& Vector4<Type>::operator+=(const Vector4<Type>& v) {
	X += v.X;
	Y += v.Y;
	Z += v.Z;
	W += v.W;
	return *this;
}

template<class Type>
INLINE Vector4<Type>& Vector4<Type>::operator+=(const Type& scalar) {
	X += scalar;
	Y += scalar;
	Z += scalar;
	W += scalar;
	return *this;
}

//Substract
template<class Type>
INLINE Vector4<Type> Vector4<Type>::operator-(const Vector4<Type>& v) const {
	return Vector4<Type>(X - v.X, Y - v.Y, Z - v.Z, W - v.W);
}

template<class Type>
INLINE Vector4<Type> Vector4<Type>::operator-(const Type& scalar) const {
	return Vector4<Type>(X - scalar, Y - scalar, Z - scalar, W - scalar);
}

template<class Type>
INLINE Vector4<Type>& Vector4<Type>::operator-=(const Vector4<Type>& v) {
	X -= v.X;
	Y -= v.Y;
	Z -= v.Z;
	W -= v.W;
	return *this;
}

template<class Type>
INLINE Vector4<Type>& Vector4<Type>::operator-=(const Type& scalar) {
	X -= scalar;
	Y -= scalar;
	Z -= scalar;
	W -= scalar;
	return *this;
}

template<class Type>
INLINE Vector4<Type> Vector4<Type>::operator-() const {
	return Vector4<Type>(-X, -Y, -Z, -W);
}

//Multiply
template<class Type>
INLINE Vector4<Type> Vector4<Type>::operator*(const Type& scalar) const {
	return Vector4<Type>(X * scalar, Y * scalar, Z * scalar, W * scalar);
}

template<class Type>
inline Vector4<Type> Vector4<Type>::operator*(const Vector4<Type>& v) const
{
	return Vector4<Type>(X * v.X, Y * v.Y, Z * v.Z, W * v.W);
}

template<class Type>
INLINE Vector4<Type>& Vector4<Type>::operator*=(const Vector4<Type>& v) {
	X *= v.X;
	Y *= v.Y;
	Z *= v.Z;
	W *= v.W;
	return *this;
}

template<class Type>
INLINE Vector4<Type>& Vector4<Type>::operator*=(const Type& scalar) {
	X *= scalar;
	Y *= scalar;
	Z *= scalar;
	W *= scalar;
	return *this;
}

//Divide
template<class Type>
INLINE Vector4<Type> Vector4<Type>::operator/(const Type& scalar) const {
	const Type newScalar = ((Type)1) / scalar;
	return Vector4<Type>(X * newValue, Y * newValue, Z * newValue, W * newValue);
}

template<class Type>
INLINE Vector4<Type>& Vector4<Type>::operator/=(const Vector4<Type>& v) {
	X /= v.X;
	Y /= v.Y;
	Z /= v.Z;
	W /= v.W;
	return *this;
}

template<class Type>
INLINE Vector4<Type>& Vector4<Type>::operator/=(const Type& scalar) {
	Type invertedValue = Type(1) / scalar;
	X *= invertedValue;
	Y *= invertedValue;
	Z *= invertedValue;
	W *= invertedValue;
	return *this;
}

//Comparator
template<class Type>
INLINE int8 Vector4<Type>::operator==(const Vector4<Type>& v) const {
	return (X == v.X && Y == v.Y && Z == v.Z && W == v.W);
}

template<class Type>
INLINE int8 Vector4<Type>::operator!=(const Vector4<Type>& v) const {
	return (X != v.X || Y != v.Y || Z != v.Z || W != v.W);
}

//Indexer
template<class Type>
INLINE Type& Vector4<Type>::operator[](const int32& n) {
	return this->V[n];
}



#endif