
#ifndef __QUATERNION_H__
#define __QUATERNION_H__

#include "Typedef.h"
#include "Vector.h"
#include "Matrix4x4.h"
#include <math.h>

struct Quaternion {
public:
	float32 X, Y, Z, W;

	Quaternion();
	Quaternion(const FVector& rad);

	FMatrix4x4 ToMatrix() const;
};

#endif