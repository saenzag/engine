
#include "RigidBody.h"

#include "MyMath.h"
#include "TransformSystem.h"
#include "Manager.h"

using namespace ENGINENAME;

RigidBody::RigidBody(uint32 Id) {

	SetComponentType(Type);

	mass = 1.0f;
	massInverse = 1.0f;
	damping = 0.999f;
	bouncyness = 0.5f;

	isStatic = 0;
	wrongMass = 0;
	affectedbyGravity = 1;
	overrideGravity = 0;
	
	velocity = FVector::Zero;
	angularVelocity = FVector::Zero;
	aceleration = FVector::Zero;
	force = FVector::Zero;
	torque = FVector::Zero;
	gravity = FVector(0.0f, -9.81f, 0.0f);

	id = Id;
}

RigidBody::~RigidBody() {
}

RigidBody* RigidBody::Construct() {
	return Manager::Instance().rigidBodySystem.Create();
}

void RigidBody::Update(const float32& deltaTime) {
	//aceleration
	aceleration = force * massInverse;

	//velocity
	velocity = (velocity * damping) + (aceleration * deltaTime);

	if (fabsf(velocity.X) < 0.001f) {
		velocity.X = 0.0f;
	}
	if (fabsf(velocity.Y) < 0.001f) {
		velocity.Y = 0.0f;
	}
	if (fabsf(velocity.Z) < 0.001f) {
		velocity.Z = 0.0f;
	}

	/*if (velocity == FVector::Zero) {
		if (aceleration == FVector::Zero) {
			return;
		}
	}*/

	//Angular Velocity
	FMatrix4x4 invTensor = Manager::Instance().rigidBodySystem.InvTensor(this);
	FVector angAccel =  invTensor * torque;

	angularVelocity = (angularVelocity * damping) + angAccel * deltaTime;

	if (fabsf(angularVelocity.X) < 0.001f) {
		angularVelocity.X = 0.0f;
	}
	if (fabsf(angularVelocity.Y) < 0.001f) {
		angularVelocity.Y = 0.0f;
	}
	if (fabsf(angularVelocity.Z) < 0.001f) {
		angularVelocity.Z = 0.0f;
	}


	//Position
	Transform* tr = GetOwner()->GetComponent<Transform>();
	const FVector newPosition = tr->GetPosition() + (velocity * deltaTime) + (aceleration * deltaTime * deltaTime);
	tr->SetPosition(newPosition);

	tr->SetRotation(tr->GetRotation() + (angularVelocity * deltaTime) + (angAccel * deltaTime * deltaTime));
}

void RigidBody::SetStatic(const char8& Static) {
	if (Static) { ClearForce(); ClearVelocity(); ClearAceleration(); ClearTorque(); ClearAngularVelocity(); }
	isStatic = Static;
	AddVersion();
}

void RigidBody::EnableGravity(const char8& enabled) {
	affectedbyGravity = enabled;
	AddVersion();
}

void RigidBody::SetVelocity(const FVector &newVelocity) {
	velocity = newVelocity;
	AddVersion();
}

void RigidBody::SetAngularVelocity(const FVector & newAngVelocity) {
	angularVelocity = newAngVelocity;
	AddVersion();
}

void RigidBody::SetMass(const float32& newMass) {
	if (newMass <= 0.0f) {
		wrongMass = 1;
		return; 
	}
	wrongMass = 0;

	mass = newMass;
	massInverse = 1.0f / newMass; 
	AddVersion();
}

void RigidBody::SetDamping(const float32 & newDamping) {
	damping = newDamping;
	AddVersion();
}

void RigidBody::SetBouncyness(const float32 & newBouncyness) {
	bouncyness = newBouncyness;
	AddVersion();
}

void RigidBody::AddForce(const FVector& Force) {
	force += Force;
}

void RigidBody::AddTorque(const FVector& Torque) {
	torque += Torque;
}

void RigidBody::AddImpulse(const FVector& Impulse) {
	velocity += Impulse * GetMassInverse();
}

void RigidBody::ClearVelocity() {
	velocity = FVector::Zero;
}

void RigidBody::ClearAceleration() {
	aceleration = FVector::Zero;
}

void RigidBody::ClearForce() {
	force = FVector::Zero;
}

void RigidBody::ClearTorque() {
	torque = FVector::Zero;
}
void RigidBody::ClearAngularVelocity() {
	angularVelocity = FVector::Zero;
}