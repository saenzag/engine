#include "TransformSystem.h"

using namespace ENGINENAME;

TransformSystem::TransformSystem()
{
}

TransformSystem::~TransformSystem() {
}

Transform* TransformSystem::Create() {

	Transform* newTransform = new Transform(transforms.size());

	if (newTransform) {
		transforms.push_back(newTransform);

		return newTransform;
	}

	return nullptr;
}

void TransformSystem::Update() {

	for (auto& transform : transforms) {
		if (transform->IsDirty()) {
			transform->UpdateMatrix();
		}
	}
}