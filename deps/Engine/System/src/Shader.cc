
#include "Shader.h"
#include "Manager.h"
#include "MyOpenGL.h"
#include "Node.h"

using namespace ENGINENAME;

Shader::Shader(const char8* ShaderName, uint32 Id) {
	name = ShaderName;
	id = Id;
}

Shader::~Shader() {
}

char8 FileToString(const std::string &from, std::string &to) {
	FILE* file;
	file = fopen(from.c_str(), "r");

	if (file) {
		fseek(file, 0, SEEK_END);
		uint32 size = ftell(file);
		rewind(file);

		char8* aux = new char8[size];
		fread(aux, sizeof(char8), size, file);
		to = aux;

		delete[] aux;
		fclose(file);

		to = to.substr(0, to.find_last_of("}") + 1);

		return ErrorType::kNoError;
	}

	return ErrorType::kWrongPath; //error
}

void Shader::Init() {
	if (HasAnyError() || IsDataReady()) { return; }
	
	Manager& manager = Manager::Instance();

	std::string vertexPath = manager.shaderSystem.shaderBasePath + "/" + name + ".vert";
	std::string fragmentPath = manager.shaderSystem.shaderBasePath + "/" + name + ".frag";

	//////// VERTEX SHADER \\\\\\\\ 
	AddError(FileToString(vertexPath, vertex));

	//////// FRAGMENT SHADER \\\\\\\\ 
	AddError(FileToString(fragmentPath, fragment));

	if (HasAnyError()) { return; }

	//Data now loaded
	MarkAsLoaded();
	AddVersion();

	std::string vertexError;
	std::string fragmentError;

	uint32 shaderInitError = kNoError;
	shaderInitError |= MyOpenGL::InitShader(vs, vertex.c_str(), ShaderType::kVertexShader,vertexError);
	shaderInitError |= MyOpenGL::InitShader(fs, fragment.c_str(), ShaderType::kFragmentShader,fragmentError);
	shaderInitError |= MyOpenGL::InitProgram(pg, vs, fs);

	AddError(shaderInitError);

	if (HasAnyError() == false) { 
		MarkAsPrepared();
		AddVersion();
		MarkAsUpdated();
	}
}

void Shader::Use() {
	MyOpenGL::UseProgram(pg);
}

void Shader::SendInformation(Node* n) {
	Manager& manager = Manager::Instance();

	if (name == "basicShader") {
		uint32 modelMatrix = GetUniform("modelMatrix");
		uint32 viewMatrix = GetUniform("viewMatrix");
		uint32 projectionMatrix = GetUniform("projectionMatrix");

		uint32 albedoVector = GetUniform("albedo");

		FMatrix4x4 model = n->GetComponent<Transform>()->GetMatrix();
		FMatrix4x4 view = manager.camera.GetViewMatrix();
		FMatrix4x4 projection = manager.camera.GetProjectionMatrix();
		
		FVector albedo = n->GetMaterial()->GetAlbedo();

		MyOpenGL::SetUniform(kUniformType_Mat4, modelMatrix, &model);
		MyOpenGL::SetUniform(kUniformType_Mat4, viewMatrix, &view);
		MyOpenGL::SetUniform(kUniformType_Mat4, projectionMatrix, &projection);
		MyOpenGL::SetUniform(kUniformType_Vec3, albedoVector, &albedo);
	} else {
		//...
	}
}

uint32 Shader::GetUniform(std::string uniformName) {
	for (uint32 i = 0; i < uniforms.size(); ++i) {
		if (uniforms[i].name_ == uniformName) {
			return uniforms[i].position_;
		}
	}

	UniformData newData;
	newData.position_ = MyOpenGL::GetUniform(pg, uniformName.c_str());
	
	if (newData.position_ != (uint32)-1) {
		newData.name_ = uniformName;
		uniforms.push_back(newData);

		return newData.position_;
	}
	return (uint32)-1;
}