
#include "NodeSystem.h"
#include "Node.h"

using namespace ENGINENAME;

NodeSystem::NodeSystem() {

}

NodeSystem::~NodeSystem() {

}

Node* NodeSystem::CreateNode() {
	uint32 newID = nodes.size();
	Node* newNode = new Node(newID);
	
	if (newNode) {
		char8 b[16] = {};
		sprintf(b,"Node_%d", nodes.size());
		newNode->SetName(b);

		nodes.push_back(newNode);
		return newNode;
	}

	return nullptr;
}

Node* NodeSystem::GetNode(uint32 id) {

	if (id < nodes.size()) {
		//if (nodes[id]) {
			return nodes[id];
		//}
	}

	return nullptr;
}

void NodeSystem::Draw() {
	for (uint32 i = 0; i < nodes.size(); i++) {
		nodes[i]->Draw();
	}
}
