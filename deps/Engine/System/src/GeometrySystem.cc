
#include "GeometrySystem.h"
#include "Geometry.h"

#include "tiny_obj_loader.h"

#include <string>
#include <map>
#include "Manager.h"
#include "Vector.h"

using namespace ENGINENAME;

GeometrySystem::GeometrySystem() {
}

GeometrySystem::~GeometrySystem() {
}

void GeometrySystem::Update() {
	for (uint32 i = 0; i < geometries.size(); i++) {
		if (geometries[i]) {
			if (geometries[i]->IsDataReady()) { continue; }
			if (geometries[i]->HasAnyError()) { continue; }

			if (geometries[i]->IsDataLoaded() == false) {
				LoadGeometry(geometries[i], geometries[i]->GetPath().c_str());
			}

			if (geometries[i]->IsDataPrepared() == false) {
				geometries[i]->Prepare();
			}
		}
	}
}

Geometry* GeometrySystem::CreateGeometry(const char8* path) {

	for (uint32 i = 0; i < geometries.size(); i++) {
		if (geometries[i]) {
			//geometry already exists
			if (geometries[i]->GetPath() == path) {
				return geometries[i];
			}
		}
	}
	//geometry is new, let's create it
	Geometry* newGeometry = new Geometry(geometries.size(), path);

	if (newGeometry) {
		geometries.push_back(newGeometry);

		return newGeometry;
	}

	return nullptr;
}

void GeometrySystem::LoadGeometry(Geometry* inGeometry, const char8* filename) {

	FILE* file = fopen(filename, "r");
	if (file) {
		fclose(file);
	} else {
		inGeometry->AddError(ErrorType::kWrongPath);
		return;
	}

	std::string fileName = filename;
	std::string fname = fileName.substr(fileName.find_last_of("/") + 1);
	std::string fPath = fileName.substr(0, fileName.find_last_of("/"));


	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string errorString;

	std::vector<Vertex> uniqueVertices = {};

	std::vector<Vertex> vertices;
	std::vector<uint32> indices;

	FVector minPosition;
	FVector maxPosition;
	uint8 firstVertex = 1;

	if (tinyobj::LoadObj(&attrib, &shapes, &materials, &errorString, filename, fPath.c_str()) == false) {
		//throw std::runtime_error(errorString);
		inGeometry->AddError(errorString);
		return;
	}

	for (const auto& shape : shapes) {
		for (const auto& index : shape.mesh.indices) {
			Vertex vertex;
			vertex.position = FVector(
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			);

			if (firstVertex) {
				firstVertex = 0;
				minPosition = vertex.position;
				maxPosition = vertex.position;
			}
			//Get max and min positions
			maxPosition.X = vertex.position.X > maxPosition.X ? vertex.position.X : maxPosition.X;
			maxPosition.Y = vertex.position.Y > maxPosition.Y ? vertex.position.Y : maxPosition.Y;
			maxPosition.Z = vertex.position.Z > maxPosition.Z ? vertex.position.Z : maxPosition.Z;

			minPosition.X = vertex.position.X < minPosition.X ? vertex.position.X : minPosition.X;
			minPosition.Y = vertex.position.Y < minPosition.Y ? vertex.position.Y : minPosition.Y;
			minPosition.Z = vertex.position.Z < minPosition.Z ? vertex.position.Z : minPosition.Z;

			if (index.texcoord_index != -1) {
				vertex.uv = FVector2(
					attrib.texcoords[2 * index.texcoord_index + 0],
					attrib.texcoords[2 * index.texcoord_index + 1]
				);
			}
			else {
				vertex.uv = FVector2(0.0f, 0.0f);
			}

			if (index.normal_index != -1) {
				vertex.normal = FVector(
					attrib.normals[3 * index.normal_index + 0],
					attrib.normals[3 * index.normal_index + 1],
					attrib.normals[3 * index.normal_index + 2]
				);
			}
			else {
				vertex.normal = FVector::One;
			}

			int32 vertexFound = 0;
			for (uint32 i = 0; i < uniqueVertices.size(); ++i) {
				if (uniqueVertices[i] == vertex) {
					vertexFound = 1;
					indices.push_back(i);
					break;
				}
			}

			if (!vertexFound) {
				indices.push_back(uniqueVertices.size());
				uniqueVertices.push_back(vertex);
			}

		}

		// BEGIN NEW GEOMETRY
		inGeometry->SetVertex(uniqueVertices);
		inGeometry->SetIndices(indices);
		inGeometry->SetMaxPos(maxPosition);
		inGeometry->SetMinPos(minPosition);
		inGeometry->MarkAsLoaded();
		inGeometry->AddVersion();

		// END NEW GEOMETRY

		indices.clear();
		vertices.clear();
		uniqueVertices.clear();

		return; // avoid multiple shapes
	}

}