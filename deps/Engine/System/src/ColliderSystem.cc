
#include "ColliderSystem.h"
#include "Collider.h"

#include "Transform.h"
#include "RigidBody.h"
#include "MyMath.h"
#include "Geometry3D.h"
#include "Manager.h"

using namespace ENGINENAME;

ColliderSystem::ColliderSystem() {
	possibleColisions = new std::vector<Collider*>();
}

ColliderSystem::~ColliderSystem() {
	possibleColisions->clear();
	delete possibleColisions;
}

Collider* ColliderSystem::Create() {
	Collider* newCollider = new Collider();
	colliders.push_back(newCollider);

	return newCollider;
}

void ColliderSystem::Init() {
	bvHierachy.Init();
}

void ColliderSystem::Update(float32 deltaTime) {
	Manager& manager = Manager::Instance();

	//GetNew possible colliison vector
	bvHierachy.Swap(possibleColisions);

	colisionResults.clear();
	for (uint32 i = 0; i < possibleColisions->size(); i += 2) {
		Collider* col1 = (*possibleColisions)[i + 0];
		Collider* col2 = (*possibleColisions)[i + 1];

		SolveCollision(col1, col2, deltaTime);
	}
	
	//Solve collisions
	for (CollisionResult& cr : colisionResults) {
		for (uint32 k = 0; k < 8 /*Iterations*/; ++k) {
			//per result
			for (uint32 i = 0; i < cr.contacts.size(); ++i) {
				//per contact
				manager.rigidBodySystem.ApplyImpulse(cr.A->GetOwner()->GetComponent<RigidBody>(),
					cr.B->GetOwner()->GetComponent<RigidBody>(),
					&cr, i);
			}
		}
	}

	const float32 PenetrationSlack = 0.01f;
	const float32 LinearProjectionPercent = 0.8f;

	for (CollisionResult& cr : colisionResults) {

		RigidBody* m1 = cr.A->GetOwner()->GetComponent<RigidBody>();
		RigidBody* m2 = cr.B->GetOwner()->GetComponent<RigidBody>();
		float32 totalMass = m1->GetMassInverse() + m2->GetMassInverse();

		if (totalMass == 0.0f) {
			continue;
		}

		float32 depth = fmaxf(cr.depth - PenetrationSlack, 0.0f);
		float32 scalar = (totalMass == 0.0f) ? 0.0f : depth / totalMass;
		FVector correction = cr.normal * scalar * LinearProjectionPercent;

		
		FVector pos1 = cr.A->GetOwnerTransform()->GetPosition() - correction * m1->GetMassInverse();
		FVector pos2 = cr.B->GetOwnerTransform()->GetPosition() + correction * m2->GetMassInverse();

		cr.A->GetOwnerTransform()->SetPosition(pos1);
		cr.B->GetOwnerTransform()->SetPosition(pos2);
	}

	/*for (uint32 i = 0; i < colliders.size(); ++i) {
		Collider* col1 = colliders[i];
		if (col1 == nullptr) { continue; }

		for (uint32 j = i + 1; j < colliders.size(); ++j) {
			Collider* col2 = colliders[j];
			if (col2 == nullptr) { continue; }

			SolveCollision(col1, col2, deltaTime);
		}
	}*/
}

void ColliderSystem::End() {
	bvHierachy.End();
}

void ColliderSystem::SolveCollision(Collider* col1, Collider* col2, float32 deltaTime) {
	const uint32 typeAddition = col1->GetShape() + col2->GetShape();

	CollisionResult cr;
	switch (typeAddition) {
	case ColliderShape::Shape_Sphere + ColliderShape::Shape_Sphere:
		cr = DetectSphereToSphere(col1, col2);
		break;
	case ColliderShape::Shape_Sphere + ColliderShape::Shape_Cube:
		if (col1->GetShape() == ColliderShape::Shape_Cube) {
			//swap
			Collider* aux = col1;
			col1 = col2;
			col2 = aux;
		}
		cr = DetectSphereToCube(col1, col2);
		break;
	case ColliderShape::Shape_Cube + ColliderShape::Shape_Cube:
		cr = DetectCubeToCube(col1, col2);
		break;
	}

	if (cr.colliding) {
		cr.A = col1;
		cr.B = col2;
		colisionResults.push_back(cr);
	}

}
////////////////////////////////////////////////////////////////
// DETECT COLLISIONS

//////////////////////////////////
//		SPHERE TO SPHERE		//
//////////////////////////////////
CollisionResult ColliderSystem::DetectSphereToSphere(Collider* col1, Collider* col2) {

	CollisionResult cr;
	Transform* tr1 = col1->GetOwnerTransform();
	Transform* tr2 = col2->GetOwnerTransform();

	float32 radius = col1->GetSize().X + col2->GetSize().X;
	FVector distance = tr2->GetPosition() - tr1->GetPosition();

	if (MyMath::SquaredSize(distance) - (radius * radius) > 0 ||
		MyMath::SquaredSize(distance) == 0.0f) {
		return cr;
	}

	distance = MyMath::Normalize(distance);
	cr.colliding = true;
	cr.normal = distance;
	cr.depth = MyMath::Abs(MyMath::Size(distance) - radius) * 0.5f;

	//distance to intersection points
	float32 dtp = col1->GetSize().X - cr.depth;
	FVector contactPoint = tr1->GetPosition() + (distance * dtp);

	cr.contacts.push_back(contactPoint);

	return cr;
}

//////////////////////////////////
//        SPHERE TO CUBE        //
//////////////////////////////////
CollisionResult ColliderSystem::DetectSphereToCube(Collider* col1, Collider* col2) {
	CollisionResult cr;
	Transform* sphereTr = col1->GetOwnerTransform();
	Transform* boxTr = col2->GetOwnerTransform();

	// Closest Point //
	FVector closestPoint = boxTr->GetPosition();
	FVector direction = sphereTr->GetPosition() - boxTr->GetPosition();

	FMatrix4x4 orientation = col2->GetOwnerTransform()->GetMatrix();
	for (uint8 i = 0; i < 3; ++i) {
		FVector axis = FVector(orientation[i].X, orientation[i].Y, orientation[i].Z);
		float32 distance = MyMath::Dot(direction, axis);

		if (distance > col2->GetSize()[i]) {
			distance = col2->GetSize()[i];
		}
		if (distance < -col2->GetSize()[i]) {
			distance = -col2->GetSize()[i];
		}
		closestPoint += axis * distance;
	}
	// !Closest Point //

	float32 distanceSquared = MyMath::SquaredSize(closestPoint - sphereTr->GetPosition());
	if (distanceSquared > col1->GetSize().X * col1->GetSize().X) {
		return cr;
	}

	FVector normal;
	if (MyMath::Abs(distanceSquared - 0.0f) <= FLT_EPSILON * MyMath::Max(1.0f, MyMath::Abs(distanceSquared))) {
		float32 sq = MyMath::SquaredSize(closestPoint - boxTr->GetPosition());
		if (MyMath::Abs(sq - 0.0f) <= 
			FLT_EPSILON * MyMath::Max(1.0f, MyMath::Abs(sq))) {
			return cr;
		}
		normal = MyMath::Normalize(closestPoint - boxTr->GetPosition());
	} else {
		normal = MyMath::Normalize(sphereTr->GetPosition() - closestPoint);
	}

	FVector outsidePoint = sphereTr->GetPosition() - (normal * col1->GetSize().X);

	cr.colliding = true;
	cr.contacts.push_back(closestPoint + (outsidePoint - closestPoint) * 0.5f);
	cr.normal = -normal;
	cr.depth = MyMath::Size(closestPoint - outsidePoint) * 0.5f;

	return cr;
}

//////////////////////////////////
//         CUBE TO CUBE         //
//////////////////////////////////
CollisionResult ColliderSystem::DetectCubeToCube(Collider* col1, Collider* col2) {
	CollisionResult cr;

	Transform* tr1 = col1->GetOwnerTransform();
	Transform* tr2 = col2->GetOwnerTransform();

	//test first sphere-sphere
	float32 radiiSum = MyMath::Largest(tr1->GetScale()) + MyMath::Largest(tr2->GetScale());
	float32 sqDistance = MyMath::SquaredSize(tr1->GetPosition() - tr2->GetPosition());
	if (sqDistance < radiiSum * radiiSum == false) {
		return cr;
	}

	FMatrix4x4 m1 = tr1->GetMatrix();
	FMatrix4x4 m2 = tr2->GetMatrix();

	FVector test[15] = {
		FVector(m1[0].X, m1[0].Y, m1[0].Z),
		FVector(m1[1].X, m1[1].Y, m1[1].Z),
		FVector(m1[2].X, m1[2].Y, m1[2].Z),

		FVector(m2[0].X, m2[0].Y, m2[0].Z),
		FVector(m2[1].X, m2[1].Y, m2[1].Z),
		FVector(m2[2].X, m2[2].Y, m2[2].Z)
	};

	for (int32 i = 0; i < 3; ++i) {
		test[6 + (i * 3) + 0] = MyMath::Cross(test[i], test[0]);
		test[6 + (i * 3) + 1] = MyMath::Cross(test[i], test[1]);
		test[6 + (i * 3) + 2] = MyMath::Cross(test[i], test[2]);
	}

	FVector* hitNormal = nullptr;
	char8 shouldFlip;

	for (int32 i = 0; i < 15; ++i) {
		if (test[i].X < 0.000001f){ test[i].X = 0.0f;}
		if (test[i].Y < 0.000001f){ test[i].Y = 0.0f;}
		if (test[i].Z < 0.000001f){ test[i].Z = 0.0f;}
		if (MyMath::SquaredSize(test[i]) < 0.001f) {
			continue;
		}

		float32 depth = PenetrationDepthBoxToBox(col1, col2, test[i], &shouldFlip);
		if (depth <= 0.0f) {
			return cr;
		}
		else if (depth < cr.depth) {
			if (shouldFlip) {
				test[i] = test[i] * -1.0f;
			}
			cr.depth = depth;
			hitNormal = &test[i];
		}
	}

	if (hitNormal == 0) {
		return cr;
	}
	FVector axis = MyMath::Normalize(*hitNormal);

	std::vector<FVector> c1 = ClipEdgesToOBB(GetEdges(col2), col1);
	std::vector<FVector> c2 = ClipEdgesToOBB(GetEdges(col1), col2);
	cr.contacts.reserve(c1.size() + c2.size());
	cr.contacts.insert(cr.contacts.end(), c1.begin(), c1.end());
	cr.contacts.insert(cr.contacts.end(), c2.begin(), c2.end());

	Interval i = GetInterval(col1, axis);
	float32 distance = (i.max - i.min)* 0.5f - cr.depth * 0.5f;
	FVector pointOnPlane = tr1->GetPosition() + axis * distance;

	for (int32 i = cr.contacts.size() - 1; i >= 0; --i) {
		FVector contact = cr.contacts[i];
		cr.contacts[i] = contact + (axis * MyMath::Dot(axis, pointOnPlane - contact));

		// This bit is in the "There is more" section of the book
		for (int32 j = cr.contacts.size() - 1; j > i; --j) {
			if (MyMath::SquaredSize(cr.contacts[j] - cr.contacts[i]) < 0.0001f) {
				cr.contacts.erase(cr.contacts.begin() + j);
				break;
			}
		}
	}

	cr.colliding = true;
	cr.normal = axis;

	return cr;
}
