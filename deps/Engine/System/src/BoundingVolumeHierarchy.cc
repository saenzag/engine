#include "BoundingVolumeHierarchy.h"

#include "Collider.h"
#include "Transform.h"
#include "MyMath.h"

#include <algorithm>
#include <stack>
#include "LineRenderer.h"

#include <thread>
#include <ppl.h>

using namespace ENGINENAME;

void UpdateBVHMultithread(){
	//Call to update in another thread
	Manager::Instance().colliderSystem.bvHierachy.Update();
}

BVHierarchy::BVHierarchy() {
	col = new std::vector<Collider*>();
}

BVHierarchy::~BVHierarchy() {
	delete col;
}

void BVHierarchy::Init() {
	thread = std::thread(UpdateBVHMultithread);
}


void BVHierarchy::Update() {
	Manager& manager = Manager::Instance();
	while (stopThread == false && manager.HasToExit() == false) {
		
		manager.clock.Start("Construct BVH", false);

		//Generate treenode
		ConstructHierarchy(manager.colliderSystem.colliders);

		//Once generated get collision vector and save it
		GetCollisionVector(*col);

		manager.clock.Stop("Construct BVH", false);

		manager.clock.Start("Wait BVH", false);
		waitingForSwap.store(true);
		//Once we got them wait till next order
		while (waitingForSwap.load() && stopThread == false && manager.HasToExit() == false) {
			continue;
		}
		manager.clock.Stop("Wait BVH", false);

		//Main thread got it's collider vector
		//Clear new collider vector
		col->clear();

		//TODO: WIP
		//Regenerate treenode...
		ClearTree(treeNodeAux);
		treeNodeAux = treeNodeBase;
		treeNodeBase = nullptr;

		manager.clock.ThreadUpdate();
	}

}

void BVHierarchy::End() {
	stopThread = true;
	thread.join();
}

void BVHierarchy::Swap(std::vector<Collider*>* toSwap) {


	//Test if the thread is not waiting, continue
	if (waitingForSwap.load() == false) { return; }

	//got new vector, swap ours with that one
	//std::vector<Collider*>* aux = toSwap;
	//toSwap = col;
	//col = aux;
	toSwap->swap(*col);

	//End with the thread waiting time
	waitingForSwap.store(false);
}
// C functions
int SortXAxis(const void* c1, const void* c2) {
	const Collider* col1 = *(Collider**)c1;
	const Collider* col2 = *(Collider**)c2;
	return col1->GetOwnerTransform()->GetPosition().X < col2->GetOwnerTransform()->GetPosition().X ? -1 : 1;
}

int SortYAxis(const void* c1, const void* c2) {
	const Collider* col1 = *(Collider**)c1;
	const Collider* col2 = *(Collider**)c2;
	return col1->GetOwnerTransform()->GetPosition().Y < col2->GetOwnerTransform()->GetPosition().Y ? -1 : 1;
}

int SortZAxis(const void* c1, const void* c2) {
	const Collider* col1 = *(Collider**)c1;
	const Collider* col2 = *(Collider**)c2;
	return col1->GetOwnerTransform()->GetPosition().Z < col2->GetOwnerTransform()->GetPosition().Z ? -1 : 1;
}

//STD format
bool SortXAxisB(const Collider* c1, const Collider* c2) {
	return c1->GetOwnerTransform()->GetPosition().X < c2->GetOwnerTransform()->GetPosition().X;
}

bool SortYAxisB(const Collider* c1, const Collider* c2) {
	return c1->GetOwnerTransform()->GetPosition().Y < c2->GetOwnerTransform()->GetPosition().Y;
}

bool SortZAxisB(const Collider* c1, const Collider* c2) {
	return c1->GetOwnerTransform()->GetPosition().Z < c2->GetOwnerTransform()->GetPosition().Z;
}

void BVHierarchy::ConstructHierarchy(std::vector<Collider*>& colliders) {
	if (colliders.size() <= 0) { return; }

	std::vector<Collider*> copy(colliders);
	treeNodeBase = new TreeNode();
	Recurse(treeNodeBase, copy);

	/*FVector M, m;
	GetColliderVectorMinMax(colliders, m, M);

	AABB *box = new AABB(((M - m) * 0.5f) + m, (M - m) * 0.5f);
	std::vector<Collider*> copyVector(colliders);

	switch (GetLargestAxis(*box)) {
	case 0:
		concurrency::parallel_sort(copyVector.begin(), copyVector.end(),SortXAxisB);
		//(&copyVector[0], copyVector.size(), sizeof(copyVector[0]), SortXAxis);
		break;
	case 1:
		concurrency::parallel_sort(copyVector.begin(), copyVector.end(), SortYAxisB);
		//qsort(&copyVector[0], copyVector.size(), sizeof(copyVector[0]), SortYAxis);
		break;
	case 2:
		concurrency::parallel_sort(copyVector.begin(), copyVector.end(), SortZAxisB);
		//qsort(&copyVector[0], copyVector.size(), sizeof(copyVector[0]), SortZAxis);
		break;
	}

	treeNodeBase = new TreeNode();
	treeNodeBase->box = box;

	std::vector<Collider*> left(copyVector.begin(), copyVector.begin() + (copyVector.size() >> 1));
	std::vector<Collider*> right(copyVector.begin() + (copyVector.size() >> 1), copyVector.end());

	if (left.size() > 0) {
		TreeNode* leftTreeNode = new TreeNode();
		treeNodeBase->left = leftTreeNode;
		Recurse(leftTreeNode, left);

	}
	if (right.size() > 0) {
		TreeNode* rightTreeNode = new TreeNode();
		treeNodeBase->right = rightTreeNode;
		Recurse(rightTreeNode, right);
	}*/

}

void BVHierarchy::Print(TreeNode* t, int _count) {
	if (t == nullptr) { return; }

	if (t->isLeaf()) {
		if (t->col1) {
			printf("\n");
			for (int i = 0; i < _count; ++i) { printf("---"); }
			printf(" Leaf1 - #%d", t->col1);
		}
	}
	else {
		printf("\n");
		for (int i = 0; i < _count; ++i) { printf("---"); }
		printf(" LEFT - #%d", t->left);
		Print(t->left, _count + 1);

		printf("\n");
		for (int i = 0; i < _count; ++i) { printf("---"); }
		printf(" RIGHT - #%d", t->right);
		Print(t->right, _count + 1);
	}

}

void BVHierarchy::PrintLine(TreeNode* t, int remark, int drawOnlyRemarkedOne, int count) {
	if (t == nullptr) { return; }

	if (t->isLeaf()) {
		if (t->col1) {
		}
	}
	else {

		if (t == treeNodeBase) {
			if (drawOnlyRemarkedOne) {
				if (remark == count) {
					PrintBox(t->box->center - t->box->halfSize,
						t->box->center + t->box->halfSize, FVector::Right);
				}
			}
			else {
				PrintBox(t->box->center - t->box->halfSize,
					t->box->center + t->box->halfSize, remark == count ? FVector::Right : FVector::Zero);
			}
			count++;
		}

		if (t->left) {
			if (t->left->box) {

				if (drawOnlyRemarkedOne) {
					if (remark == count) {
						PrintBox(t->left->box->center - t->left->box->halfSize,
							t->left->box->center + t->left->box->halfSize, FVector::Right);
					}
				}
				else {
					PrintBox(t->left->box->center - t->left->box->halfSize,
						t->left->box->center + t->left->box->halfSize, remark == count ? FVector::Right : FVector::Zero);
				}
			}
			PrintLine(t->left, remark, drawOnlyRemarkedOne, count + 1);
		}
		if (t->right) {
			if (t->right->box) {
				if (drawOnlyRemarkedOne) {
					if (remark == count) {
						PrintBox(t->right->box->center - t->right->box->halfSize,
							t->right->box->center + t->right->box->halfSize, FVector::Forward);
					}
				}
				else {
					PrintBox(t->right->box->center - t->right->box->halfSize,
						t->right->box->center + t->right->box->halfSize, remark == count ? FVector::Forward : FVector::Zero);
				}
			}
			PrintLine(t->right, remark, drawOnlyRemarkedOne, count + 1);
		}
	}
}

uchar8 BVHierarchy::IsColliding(const AABB* c1, const AABB* c2) {

	return !(MyMath::Abs(c1->center.X - c2->center.X) > c1->halfSize.X + c2->halfSize.X ||
		MyMath::Abs(c1->center.Y - c2->center.Y) > c1->halfSize.Y + c2->halfSize.Y ||
		MyMath::Abs(c1->center.Z - c2->center.Z) > c1->halfSize.Z + c2->halfSize.Z);

}

void BVHierarchy::GetCollisionVector(std::vector<Collider*>& outVec) {
	if (treeNodeBase == nullptr) { return; }
	//nothing to analize
	if (treeNodeBase->left == nullptr || treeNodeBase->right == nullptr) { return; }

	struct BBStr {
		BBStr() {};
		BBStr(const BBStr& copy) : a(copy.a), b(copy.b) {};
		BBStr(TreeNode* A, TreeNode* B) : a(A), b(B) {};

		TreeNode* a = nullptr;
		TreeNode* b = nullptr;
	};
	std::stack<BBStr> stack;

	BBStr root(treeNodeBase->left, treeNodeBase->right);
	stack.push(root);
	int32 repetitions = 0;

	while (stack.empty() == false) {

		BBStr current(stack.top());
		stack.pop();

		uchar8 aleaf = current.a->isLeaf();
		uchar8 bleaf = current.b->isLeaf();

		if (aleaf || bleaf) {

			if (aleaf && bleaf) {

				//do they collide?
				if (IsColliding(current.a->box, current.b->box)) {

					char8 found = 0;
					for (uint32 i = 0; i < outVec.size(); i += 2) {
						if (outVec[i] == current.a->col1) {
							if (outVec[i + 1] == current.b->col1) {
								found = 1;
								++repetitions;
								break;
							}
						}
					}
					if (found) { continue; }

					outVec.push_back(current.a->col1);
					outVec.push_back(current.b->col1);
				}
			} else {
				if (aleaf) {
					//a is leaf, b not
					stack.push(BBStr(current.a, current.b->left));
					stack.push(BBStr(current.a, current.b->right));
				} else {
					//b is leaf, a not
					stack.push(BBStr(current.a->left, current.b));
					stack.push(BBStr(current.a->right, current.b));
				}
			}

		}
		else {
			if (IsColliding(current.a->box, current.b->box)) {
				//They do collide
				stack.push(BBStr(current.a->left, current.a->right));
				stack.push(BBStr(current.a->left, current.b->left));
				stack.push(BBStr(current.a->left, current.b->right));
				stack.push(BBStr(current.a->right, current.b->left));
				stack.push(BBStr(current.a->right, current.b->right));
				stack.push(BBStr(current.b->left, current.b->right));
			} else {
				//they do not collide
				stack.push(BBStr(current.a->left, current.a->right));
				stack.push(BBStr(current.b->left, current.b->right));
			}
		}

	}

	return;
}

void BVHierarchy::PrintBox(const FVector& min, const FVector& max, const FVector& color, const FMatrix4x4& model) {
	Manager& manager = Manager::Instance();

	manager.lineRenderer->BeginDraw();

	manager.lineRenderer->Draw(FVector(min.X, min.Y, min.Z), FVector(min.X, min.Y, max.Z), color, model);
	manager.lineRenderer->Draw(FVector(min.X, min.Y, min.Z), FVector(min.X, max.Y, min.Z), color, model);
	manager.lineRenderer->Draw(FVector(min.X, min.Y, min.Z), FVector(max.X, min.Y, min.Z), color, model);

	manager.lineRenderer->Draw(FVector(max.X, max.Y, max.Z), FVector(min.X, max.Y, max.Z), color, model);
	manager.lineRenderer->Draw(FVector(max.X, max.Y, max.Z), FVector(max.X, max.Y, min.Z), color, model);
	manager.lineRenderer->Draw(FVector(max.X, max.Y, max.Z), FVector(max.X, min.Y, max.Z), color, model);

	manager.lineRenderer->Draw(FVector(min.X, min.Y, max.Z), FVector(min.X, max.Y, max.Z), color, model);
	manager.lineRenderer->Draw(FVector(min.X, max.Y, max.Z), FVector(min.X, max.Y, min.Z), color, model);
	manager.lineRenderer->Draw(FVector(min.X, max.Y, min.Z), FVector(max.X, max.Y, min.Z), color, model);

	manager.lineRenderer->Draw(FVector(max.X, min.Y, min.Z), FVector(max.X, max.Y, min.Z), color, model);
	manager.lineRenderer->Draw(FVector(max.X, min.Y, min.Z), FVector(max.X, min.Y, max.Z), color, model);
	manager.lineRenderer->Draw(FVector(min.X, min.Y, max.Z), FVector(max.X, min.Y, max.Z), color, model);

	manager.lineRenderer->EndDraw();

}

void BVHierarchy::Recurse(TreeNode* currentNode, std::vector<Collider*>& colliders) {

	FVector min, max;
	GetColliderVectorMinMax(colliders, min, max);
	AABB* newBox = new AABB(((max - min) * 0.5f) + min, (max - min) * 0.5f);

	currentNode->box = newBox;

	if ((colliders).size() == 1) {
		//we found a leaf
		currentNode->col1 = (colliders)[0];
		//if ((colliders).size() > 1) { currentNode->col2 = (colliders)[1]; }
		return;
	}

	switch (GetLargestAxis(*newBox)) {
	case 0:
		concurrency::parallel_sort(colliders.begin(), colliders.end(), SortXAxisB);
		//qsort(&((colliders)[0]), (colliders).size(), sizeof((colliders)[0]), SortXAxis);
		break;
	case 1:
		concurrency::parallel_sort(colliders.begin(), colliders.end(), SortYAxisB);
		//qsort(&((colliders)[0]), (colliders).size(), sizeof((colliders)[0]), SortYAxis);
		break;
	case 2:
		concurrency::parallel_sort(colliders.begin(), colliders.end(), SortZAxisB);
		//qsort(&((colliders)[0]), (colliders).size(), sizeof((colliders)[0]), SortZAxis);
		break;
	}

	TreeNode* right = new TreeNode();
	TreeNode* left = new TreeNode();

	//right->parent = currentNode;
	//left->parent = currentNode;

	currentNode->right = right;
	currentNode->left = left;

	std::vector<Collider*> vecLeft((colliders).begin(), (colliders).begin() + ((colliders).size() >> 1));
	std::vector<Collider*> vecRight((colliders).begin() + ((colliders).size() >> 1), (colliders).end());

	Recurse(left, vecLeft);
	Recurse(right, vecRight);
}

uint8 BVHierarchy::GetLargestAxis(const AABB& box) {
	const FVector diff(box.halfSize);
	if (diff.X > diff.Y && diff.X > diff.Z) {
		return 0;
	}
	else if (diff.Y > diff.Z) {
		return 1;
	}
	else {
		return 2;
	}
}

void BVHierarchy::GetColliderVectorMinMax(const std::vector<Collider*>& colliders, FVector& Min, FVector& Max) {

	if (colliders.size() <= 0) { return; }
	char8 first = 1;

	for (const auto& collider : colliders) {
		FVector max = collider->GetSize();
		FVector min = -max;

		// Generate box to AABB correctly
		std::vector<FVector> box;
		box.push_back(FVector(min.X, min.X, min.X));
		box.push_back(FVector(max.X, min.X, min.X));
		box.push_back(FVector(max.X, min.X, max.X));
		box.push_back(FVector(min.X, min.X, max.X));

		box.push_back(FVector(min.X, max.X, min.X));
		box.push_back(FVector(max.X, max.X, min.X));
		box.push_back(FVector(max.X, max.X, max.X));
		box.push_back(FVector(min.X, max.X, max.X));

		for (FVector& v : box) {
			FVector point = collider->GetOwnerTransform()->GetMatrix() * v;

			//MyMath::MinMax(max, min);

			if (first) {
				first = 0;
				Min = point;
				Max = point;
			}
			else {
				MyMath::MinMax(Max, point);
				MyMath::MinMax(point, Min);
			}
		}
	}
}

void BVHierarchy::ClearTree(TreeNode* base) {
	if (base) {
		ClearTree(base->left);
		ClearTree(base->right);

		if (base->box) {
			delete base->box;
			base->box = nullptr;
		}
		delete base;
		base = nullptr;
	}
}
