
#include "Node.h"

#include "Manager.h"
#include "TransformSystem.h"
#include "MyOpenGL.h"

#include "Transform.h"
#include "Geometry.h"
#include "Material.h"

using namespace ENGINENAME;

Node::Node(uint32 Id) {
	id = Id;
	AddComponent<Transform>();
}

Node::~Node() {
}

void Node::SetName(const char8* newName) {
	name = newName;
}

void Node::AddGeometry(const char8* path) {
	if (geometry != nullptr) { return; }

	Manager& manager = Manager::Instance();

	geometry = manager.geometrySystem.CreateGeometry(path);
}

void Node::AddMaterial(const char8 *shaderName) {
	if (material != nullptr) { return; }
	Manager& manager = Manager::Instance();

	material = manager.materialSystem.CreateMaterial(shaderName);
}

void Node::Draw() {

	if (geometry == nullptr || material == nullptr) { return; }

	if (geometry->IsDataReady()) {

		material->shader->Use();
		material->shader->SendInformation(this);

		MyOpenGL::BindVertexArray(geometry->GetVAO());
		MyOpenGL::DrawTriangles(geometry->GetIndices().size());

	}
}
