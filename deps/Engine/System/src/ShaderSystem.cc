
#include "ShaderSystem.h"
#include "MyOpenGL.h"

using namespace ENGINENAME;

ShaderSystem::ShaderSystem() {
	lastUsedShader = (uint32)(-1);
}

ShaderSystem::~ShaderSystem() {
}

Shader* ShaderSystem::CreateShader(const char8* name) {
	for (uint32 i = 0; i < shaders.size(); i++) {
		if (shaders[i]->GetName() == name) {
			//shader already exists
			return shaders[i];
		}
	}

	Shader* newShader = new Shader(name, shaders.size());
	if (newShader) {
		shaders.push_back(newShader);
		return newShader;
	}

	return nullptr;
}

uint8 ShaderSystem::UseShader(Shader* shader){
	if (shader == nullptr) { return 0; }

	return UseShader(shader->GetId());
}

uint8 ShaderSystem::UseShader(uint32 shaderID) {
	//is this shader currently active?
	if (shaderID == lastUsedShader) { return 1; }
	lastUsedShader = shaderID;
	// else use shader

	// does this shader exist?
	if (shaderID > shaders.size() - 1) { return 0; }
	if (shaders[shaderID] == nullptr) { return 0; }
	//  is initialized?
	if (shaders[shaderID]->IsDataReady() == false) { return 0; }

	MyOpenGL::UseProgram(shaderID);
	return 0;
}