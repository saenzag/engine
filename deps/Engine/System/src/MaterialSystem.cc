
#include "MaterialSystem.h"

using namespace ENGINENAME;

MaterialSystem::MaterialSystem()
{
}

MaterialSystem::~MaterialSystem() {
}

Material* MaterialSystem::CreateMaterial(const char8* shaderName) {
	uint32 newId = materials.size();

	Material* newMaterial = new Material(newId, shaderName);
	if (newMaterial) {
		materials.push_back(newMaterial);
		return newMaterial;
	}

	return nullptr;
}

Material* MaterialSystem::GetMaterial(uint32 id) {
	if (id > materials.size() - 1) {
		return nullptr;
	}

	return materials[id];
}

void MaterialSystem::Update() {
	for (uint32 i = 0; i < materials.size(); ++i) {
		materials[i]->Update();
	}
}