
#include "Transform.h"
#include "Quaternion.h"
#include "MyMath.h"
#include "Manager.h"

using namespace ENGINENAME;


Transform::Transform(uint32 Id) {
	id = Id;

	SetComponentType(Type);

	position = FVector::Zero;
	rotation = FVector::Zero;
	scale = FVector::One;
}

Transform::~Transform() {
}

Transform* Transform::Construct() {
	return Manager::Instance().transformSystem.Create();
}

FMatrix4x4 Transform::UpdateMatrix() {

	//identity matrix
	FMatrix4x4 newMatrix;

	//Translate
	newMatrix.Translate(newMatrix, position);

	//Rotate
	Quaternion rotationQuad = Quaternion(rotation);
	newMatrix = newMatrix * rotationQuad.ToMatrix();

	//Scale
	newMatrix.Scale(newMatrix, scale);

	matrix = newMatrix;

	AddVersion();
	MarkAsUpdated();

	return matrix;
}

FVector Transform::SetRotation(FVector v) {

	// range [-180, 180]
	if (v.X > MATH_PI) {
		while (v.X > MATH_PI) {
			v.X -= 2 * MATH_PI;
		}
	} else {
		if (v.X < -MATH_PI) {
			while (v.X < -MATH_PI) {
				v.X += 2 * MATH_PI;
			}
		}
	}

	if (v.Y > MATH_PI) {
		while (v.Y > MATH_PI) {
			v.Y -= 2 * MATH_PI;
		}
	} else {
		if (v.Y < -MATH_PI) {
			while (v.Y < -MATH_PI) {
				v.Y += 2 * MATH_PI;
			}
		}
	}

	if (v.Z > MATH_PI) {
		while (v.Z > MATH_PI) {
			v.Z -= 2 * MATH_PI;
		}
	} else {
		if (v.Z < -MATH_PI) {
			while (v.Z < -MATH_PI) {
				v.Z += 2 * MATH_PI;
			}
		}
	}

	rotation = v;
	AddVersion();

	return rotation;
}
