
#include "RigidBodySystem.h"

#include "RigidBody.h"
#include "ColliderSystem.h"
#include "MyMath.h"
#include "Transform.h"
#include "Collider.h"

using namespace ENGINENAME;

FMatrix4x4 RigidBodySystem::InvTensor(RigidBody* rb) {
	Collider* col = rb->GetOwner()->GetComponent<Collider>();
	if (rb->GetMass() == 0 || col == nullptr) {
		return FMatrix4x4(0.0f);
	}
	float32 ix = 0.0f;
	float32 iy = 0.0f;
	float32 iz = 0.0f;
	float32 iw = 0.0f;
	float32 fraction, r2;

	switch (col->Type) {
	case Shape_Sphere:
		r2 = col->GetSize().X * col->GetSize().X;
		fraction = (2.0f / 5.0f);

		ix = r2 * rb->GetMass() * fraction;
		iy = r2 * rb->GetMass() * fraction;
		iz = r2 * rb->GetMass() * fraction;
		iw = 1.0f;

		break;
	case Shape_Cube:
		FVector size = col->GetSize() * 2.0f;
		fraction = (1.0f / 12.0f);

		float x2 = size.X * size.X;
		float y2 = size.Y * size.Y;
		float z2 = size.Z * size.Z;

		ix = (y2 + z2) * rb->GetMass() * fraction;
		iy = (x2 + z2) * rb->GetMass() * fraction;
		iz = (x2 + y2) * rb->GetMass() * fraction;
		iw = 1.0f;
		break;
	}

	FMatrix4x4 mat = MyMath::Inverse(FMatrix4x4(
		ix,	  0.0f, 0.0f, 0.0f,
		0.0f, iy,   0.0f, 0.0f,
		0.0f, 0.0f, iz,   0.0f,
		0.0f, 0.0f, 0.0f, iw));

	return mat;
}

// GRAVITY FORCE CALCULATOR
void GravityForce::UpdateForce(RigidBody* target, float32 deltaTime) {

	if (target->HasWrongMass()) { return; }
	
	if (target->IsAffectedByGravity()) {
		if (target->IsOverridingGravity()) {
			target->AddForce(globalGravity * target->GetMass());
		} else {
			target->AddForce(target->GetOverridedGravity() * target->GetMass());
		}
	}
}

// DRAG FORCE CALCULATION
void DragForce::UpdateForce(RigidBody* target, float32 deltaTime) {
	const FVector velocity = target->GetVelocity();
	const float32 VSize = MyMath::Size(velocity);

	const float32 dragCoefficient = (k1 * VSize) + (k2 * VSize * VSize);

	const FVector normalizedVelocity = MyMath::Normalize(velocity);
	target->AddForce(-(normalizedVelocity * dragCoefficient));
}

/*
*	PHYSICS SYTEM
*/
RigidBodySystem::RigidBodySystem() {
}

RigidBodySystem::~RigidBodySystem() {
}

RigidBody* RigidBodySystem::Create() {
	RigidBody* pc = new RigidBody(rigidBodies.size());

	if (pc != nullptr) {
		rigidBodies.push_back(pc);
	}

	return pc;
}

void RigidBodySystem::Update(float32 deltaTime) const {

	GravityForce gravityForceGenerator;
	gravityForceGenerator.globalGravity = FVector(0.0f, -9.81f, 0.0f);

	DragForce dragForceGenerator;
	dragForceGenerator.k1 = 0.1f;
	dragForceGenerator.k2 = 0.0f;

	for (auto& phcomp : rigidBodies) {
		if (phcomp->IsStatic()) { continue; }
		
		gravityForceGenerator.UpdateForce(phcomp, deltaTime);
		dragForceGenerator.UpdateForce(phcomp, deltaTime);

		phcomp->Update(deltaTime);

		phcomp->ClearForce();
	}
}

void RigidBodySystem::ApplyImpulse(RigidBody* rb1, RigidBody* rb2, CollisionResult* cr, const uint32& contactNum) {
	Transform* tr1 = rb1->GetOwner()->GetComponent<Transform>();
	Transform* tr2 = rb2->GetOwner()->GetComponent<Transform>();
	
	if (tr1 == nullptr || tr2 == nullptr) {
		return;
	}
	
	// Linear impulse
	float32 invMass1 = rb1->GetMassInverse();
	float32 invMass2 = rb2->GetMassInverse();
	float32 invMassSum = invMass1 + invMass2;

	if (invMassSum == 0.0f) {
		return; // Both objects have infinite mass!
	}

	FVector r1 = cr->contacts[contactNum] - tr1->GetPosition();
	FVector r2 = cr->contacts[contactNum] - tr2->GetPosition();
	FMatrix4x4 i1 = InvTensor(rb1);
	FMatrix4x4 i2 = InvTensor(rb2);

	FVector relativeVel = (rb2->GetVelocity() + MyMath::Cross(rb2->GetAngularVelocity(), r2)) - (rb1->GetVelocity() + MyMath::Cross(rb1->GetAngularVelocity(), r1));
	// Relative collision normal
	FVector relativeNorm = MyMath::Normalize(cr->normal);

	// Moving away from each other? Do nothing!
	float32 dot = MyMath::Dot(relativeVel, relativeNorm);
	if (MyMath::Dot(relativeVel, relativeNorm) > 0.0f) {
		return;
	}

	float32 e = fminf(rb1->GetBouncyness(), rb2->GetBouncyness());

	float32 numerator = (-(1.0f + e) * MyMath::Dot(relativeVel, relativeNorm));
	float32 d1 = invMassSum;
	FVector d2 = MyMath::Cross(i1 * (MyMath::Cross(r1, relativeNorm)), r1);
	FVector d3 = MyMath::Cross(i2 * (MyMath::Cross(r2, relativeNorm)), r2);
	float32 denominator = d1 + MyMath::Dot(relativeNorm, d2 + d3);

	float32 j = (denominator == 0.0f) ? 0.0f : numerator / denominator;
	if (cr->contacts.size() > 0.0f && j != 0.0f) {
		j /= (float32)cr->contacts.size();
	}

	FVector impulse = relativeNorm * j;

	if (rb1->IsStatic() == false) { 
		rb1->SetVelocity(rb1->GetVelocity() - impulse *  invMass1);
	}
	if (rb2->IsStatic() == false) {
		rb2->SetVelocity(rb2->GetVelocity() + impulse * invMass2);
	}

	if (rb1->IsStatic() == false) {
		rb1->SetAngularVelocity(rb1->GetAngularVelocity() - (i1 * MyMath::Cross(r1, impulse)));
	}
	if (rb2->IsStatic() == false) {
		rb2->SetAngularVelocity(rb2->GetAngularVelocity() + (i2 * MyMath::Cross(r2, impulse)));
	}
	// Friction
	FVector t = relativeVel - (relativeNorm * MyMath::Dot(relativeVel, relativeNorm));
	if (CMP(MyMath::SquaredSize(t), 0.0f)) {
		return;
	}
	t = MyMath::Normalize(t);

	numerator = -MyMath::Dot(relativeVel, t);
	d1 = invMassSum;

	d2 = MyMath::Cross(i1 * MyMath::Cross(r1, t), r1);
	d3 = MyMath::Cross(i2 * MyMath::Cross(r2, t), r2);
	denominator = d1 + MyMath::Dot(t, d2 + d3);

	float jt = (denominator == 0.0f) ? 0.0f : numerator / denominator;
	if (cr->contacts.size() > 0.0f && jt != 0.0f) {
		jt /= (float32)cr->contacts.size();
	}

	if (CMP(jt, 0.0f)) {
		return;
	}

	FVector tangentImpuse;
#ifdef DYNAMIC_FRICTION
	float sf = sqrtf(A.staticFriction * B.staticFriction);
	float df = sqrtf(A.dynamicFriction * B.dynamicFriction);
	if (fabsf(jt) < j * sf) {
		tangentImpuse = t * jt;
	}
	else {
		tangentImpuse = t * -j * df;
	}
#else
	float friction = sqrtf(rb1->GetDamping() * rb2->GetDamping());
	if (jt > j * friction) {
		jt = j * friction;
	}
	else if (jt < -j * friction) {
		jt = -j * friction;
	}
	tangentImpuse = t * jt;
#endif

	if (rb1->IsStatic() == false) {
		rb1->SetVelocity(rb1->GetVelocity() - tangentImpuse * invMass1);
	}
	if (rb2->IsStatic() == false) {
		rb2->SetVelocity(rb2->GetVelocity() + tangentImpuse * invMass2);
	}

	if (rb1->IsStatic() == false) {
		rb1->SetAngularVelocity(rb1->GetAngularVelocity() - (i1 * MyMath::Cross(r1, tangentImpuse)));
	}
	if (rb2->IsStatic() == false) {
		rb2->SetAngularVelocity(rb2->GetAngularVelocity() + (i2 * MyMath::Cross(r2, tangentImpuse)));
	}
}

