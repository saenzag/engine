#include "Geometry.h"
#include "MyOpenGL.h"

#include <string>

using namespace ENGINENAME;

Geometry::Geometry(uint32 idNumber, std::string relativePath) {
	id = idNumber;
	path = relativePath;
}

Geometry::~Geometry() {
}

void Geometry::SetVertex(const std::vector<Vertex> &v) {
	vertex = v;
}

void Geometry::SetIndices(const std::vector<uint32> &i) {
	indices = i;
}

std::vector<Vertex> Geometry::GetVertex() {
	return vertex;
}

std::vector<uint32> Geometry::GetIndices() {
	return indices;
}

void Geometry::Prepare() {
	if (!IsDataLoaded()) { return; }

	MyOpenGL::GenerateVAO(vao);
	MyOpenGL::BindVertexArray(vao);
	
	MyOpenGL::GenerateVBO(vbo);
	MyOpenGL::BindVertexBuffer(vbo);

	MyOpenGL::SetVertexBufferData(vertex);
	MyOpenGL::SetIndicesData(indices);

	// Position
	MyOpenGL::SetVertexAttribs(0, 3, Vertex::GetOffsetFor(VertexData::kPosition));
	// Normal
	MyOpenGL::SetVertexAttribs(1, 3, Vertex::GetOffsetFor(VertexData::kNormal));
	// Uv
	MyOpenGL::SetVertexAttribs(2, 2, Vertex::GetOffsetFor(VertexData::kUv));

	MyOpenGL::UnbindVAO();

	MarkAsPrepared();
	AddVersion();
	MarkAsUpdated();
}