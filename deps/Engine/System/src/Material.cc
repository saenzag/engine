
#include "Material.h"
#include "Manager.h"

using namespace ENGINENAME;

Material::Material(uint32 Id, const char8* shaderName) {
	Manager& manager = Manager::Instance();
	id = Id;

	shader = manager.shaderSystem.CreateShader(shaderName);
}

Material::~Material() {
}

void Material::Update() {
	if (shader == nullptr) { return; }

	if (shader->IsDataReady() == false) { shader->Init(); }
}