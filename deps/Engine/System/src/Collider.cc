
#include "Collider.h"
#include "Manager.h"
#include "Geometry.h"
#include "Node.h"

using namespace ENGINENAME;

Collider::Collider() {
	shape = Shape_Sphere;
	size = FVector::One;

	SetComponentType(Type);
}

Collider::~Collider() {
}

Collider* Collider::Construct() {
	return Manager::Instance().colliderSystem.Create();
}

void Collider::Init() {
	ownerTransform = this->GetOwner()->GetComponent<Transform>();
	Geometry* g = (static_cast<Node*>(this->GetOwner()))->GetGeometry();
	if (g) {
		//size = FVector(g->GetMax() - g->GetMin());
	}
}

FVector ENGINENAME::Collider::GetSize() const
{
	return ownerTransform->GetScale();
	/*return FVector(
		size.X,
		size.Y,
		size.Z);*/
}
