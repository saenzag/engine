
#ifndef __SHADER_SYSTEM_H__
#define __SHADER_SYSTEM_H__ 1

#include "Typedef.h"
#include "Shader.h"
#include <vector>
#include <string>

namespace ENGINENAME {
	class ShaderSystem {
	public:
		ShaderSystem();
		~ShaderSystem();

		std::string shaderBasePath = "../data/shaders";
		Shader* CreateShader(const char8* name);

		uint8 UseShader(Shader* shader);
		uint8 UseShader(uint32 shaderID);

	private:
		std::vector<Shader*> shaders;

		uint32 lastUsedShader;
	};

};

#endif