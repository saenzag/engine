
#ifndef __MATERIAL_H__
#define __MATERIAL_H__ 1

#include "Typedef.h"
#include "Shader.h"
#include "Vector.h"

#include "Object.h"

namespace ENGINENAME {
	class Material : public Object {
	public:
		Material(uint32 id, const char8* shaderName);
		~Material();

		void SetAlbedo(FVector a) { albedo = a; }
		FVector GetAlbedo() { return albedo; }
		
		void Update();

		Shader* shader = nullptr;

	private:
		uint32 id;
		
		FVector albedo;
	};

};

#endif