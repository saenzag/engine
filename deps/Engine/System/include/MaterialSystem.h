
#ifndef __MATERIAL_SYSTEM_H__
#define __MATERIAL_SYSTEM_H__ 1

#include "Typedef.h"
#include <vector>
#include "Material.h"

namespace ENGINENAME {
	class MaterialSystem {
	public:
		MaterialSystem();
		~MaterialSystem();

		void Update();

		Material* CreateMaterial(const char8* shaderName);
		Material* GetMaterial(uint32 id);

	private:
		std::vector<Material*> materials;
	};

};

#endif