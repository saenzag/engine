
#ifndef __VERSIONER_H__
#define __VERSIONER_H__ 1

#include "Typedef.h"

namespace ENGINENAME {

	/**
	*	Versioner
	*		This class calculates any component version to keep
	*		it updated.
	*/

	class Versioner {
	public:
		Versioner() {}
		~Versioner() {}
		
		//	VERSIONING
		INLINE void AddVersion() { lastVersion = currentVersion++; };
		INLINE void MarkAsUpdated() { lastUpdateVersion = currentVersion; }
		INLINE uint8 IsDirty() { return lastUpdateVersion != currentVersion; }

		INLINE const uint32 GetVersion() { return currentVersion; }
		INLINE const uint32 GetLastVersion() { return lastVersion; }
		INLINE const uint32 GetLastUpdateVersion() { return lastUpdateVersion; }

		//	DATA
		INLINE const void MarkAsLoaded() { dataState |= 1; }
		INLINE const void MarkAsPrepared() { dataState |= 2; }

		INLINE const uint8 IsDataLoaded() { return dataState & 1; }
		INLINE const uint8 IsDataPrepared() { return dataState & 2; }
		INLINE const uint8 IsDataReady() { return dataState & 3; }
	private:
		uint32 currentVersion = 0;
		uint32 lastVersion = 0;
		uint32 lastUpdateVersion = 0;

		// Data is loaded	1 << 1
		// Data is prepared 1 << 2
		// Data is Ready	1 << 1 + 1 << 2
		uint8 dataState = 0;
	};
};

#endif