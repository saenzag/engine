
#ifndef __SHADER_H__
#define __SHADER_H__ 1

#include "Typedef.h"

#include "Object.h"

#include <string>
#include <vector>

namespace ENGINENAME {

	struct UniformData {
		std::string name_;
		uint32 position_;
	};

	class Node;

	class Shader : public Object {
	public:
		Shader(const char8* ShaderName, uint32 Id);
		~Shader();

		void Init();
		void Use();

		std::string GetName() const { return name; }
		uint32 GetId() const { return id; }

		//uint8 IsInitialized() { return initialized; }
		void SendInformation(Node* n);

	private:
		uint32 vs, fs;
		uint32 pg;
		//uint8 initialized;
		uint32 id;

		std::string vertex;
		std::string fragment;

		std::string name;

		uint32 GetUniform(std::string uniformName);
		std::vector<UniformData> uniforms;
	};

};

#endif