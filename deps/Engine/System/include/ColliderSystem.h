
#ifndef __COLLIDER_SYSTEM_H__
#define __COLLIDER_SYSTEM_H__ 1

#include "Typedef.h"
#include <vector>
#include "BoundingVolumeHierarchy.h"
#include "Vector.h"

namespace ENGINENAME {

	class Collider;

	struct CollisionResult {
	public:
		INLINE CollisionResult() { Reset(); }

		INLINE void Reset() {
			colliding = false;
			A = nullptr;
			B = nullptr;
			normal = FVector(0.0f, 0.0f, 1.0f);
			depth = FLT_MAX; // Max float
			if (contacts.size() > 0) {
				contacts.clear();
			}
		}

		Collider* A;
		Collider* B;

		uchar8 colliding;
		FVector normal;
		float32 depth;
		std::vector<FVector> contacts;
	};

	class ColliderSystem {
	public:
		ColliderSystem();
		~ColliderSystem();

		Collider* Create();

		void Init();
		void Update(float32 deltaTime = 0.016f);
		void End();

		BVHierarchy bvHierachy;

	//private:

		std::vector<Collider*> colliders;
		std::vector<Collider*>* possibleColisions;
		std::vector<CollisionResult> colisionResults;

		void SolveCollision(Collider* col1, Collider* col2, float32 deltaTime);

		// Collision Detectors
		CollisionResult DetectSphereToSphere(Collider* col1, Collider* col2);
		CollisionResult DetectSphereToCube(Collider* col1, Collider* col2);
		CollisionResult DetectCubeToCube(Collider * col1, Collider * col2);
	};

}

#endif // !__COLLIDER_SYSTEM_H__