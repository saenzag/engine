
#ifndef __TRANSFORM_H__
#define __TRANSFORM_H__ 1

#include "Typedef.h"

#include "Component.h"

#include "Vector.h"
#include "Matrix4x4.h"

namespace ENGINENAME {
	class Transform : public Component {
	public:
		Transform(uint32 Id);
		~Transform();

		static const ComponentType Type = CT_Transform;
		static Transform* Construct();
		static void Init(){}

		uint32 GetId() { return id; }
		FMatrix4x4 UpdateMatrix();

		INLINE FVector GetPosition() const { return position; }
		INLINE FVector GetRotation() const { return rotation; }
		INLINE FVector GetScale() const { return scale; }

		INLINE FVector SetPosition(const FVector& v) { position = v; AddVersion(); return position; }
		INLINE FVector AddPosition(const FVector& v) { position += v; AddVersion(); return position; }
		FVector SetRotation(FVector v);
		INLINE FVector SetScale(const FVector& v) { scale = v; AddVersion(); return scale; }

		INLINE FMatrix4x4 GetMatrix() const { return matrix; }
		
	private:
		uint32 id;

		FVector position;
		FVector rotation;
		FVector scale;

		FMatrix4x4 matrix;

	};

};

#endif