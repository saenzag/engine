
#ifndef __TRANSFORM_SYSTEM_H__
#define __TRANSFORM_SYSTEM_H__ 1

#include "Typedef.h"
#include "Transform.h"
#include <vector>

namespace ENGINENAME {
	class TransformSystem {
	public:
		TransformSystem();
		~TransformSystem();

		Transform* Create();
		void Update();

	private:
		std::vector<Transform*> transforms;
	};
}

#endif
