
#ifndef __COLLIDER_H__
#define __COLLIDER_H__ 1

#include "Typedef.h"
#include "Vector.h"

#include "Component.h"

namespace ENGINENAME {

	enum ColliderShape {
		Shape_Sphere	= 1 << 0,
		Shape_Cube		= 1 << 1,
		Shape_Plane		= 1 << 2,
		//Shape_Capsule	= 1 << 3,
	};

	class Transform;

	class Collider : public Component {
	public:
		Collider();
		~Collider();

		static const ComponentType Type = CT_Collider;
		static Collider* Construct();
		void Init();

		// Collider
		INLINE ColliderShape GetShape() const { return shape; };
		INLINE void SetShape(const ColliderShape &Shape) { shape = Shape; };
		FVector GetSize() const;
		INLINE void SetSize(const FVector &Size) { size = Size; };
		INLINE Transform* GetOwnerTransform() const { return ownerTransform; };

	private:
		FVector size;
		ColliderShape shape;
		Transform* ownerTransform;
	};

}

#endif // !__COLLIDER_H__
