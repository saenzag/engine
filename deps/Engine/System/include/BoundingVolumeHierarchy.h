#ifndef __BVH_H__
#define __BVH_H__ 1

#include "Typedef.h"

#include "Vector.h"
#include "MyMath.h"
#include <vector>

#include <thread>
#include <atomic>

namespace ENGINENAME {

	class Collider;

	struct AABB {
		AABB(const FVector& Center, const FVector& HalfSize) : center(Center), halfSize(HalfSize) {};

		FVector center;
		FVector halfSize;
	};

	struct TreeNode {
		AABB* box = nullptr;

		TreeNode* right = nullptr;
		TreeNode* left = nullptr;

		Collider* col1 = nullptr;

		bool isLeaf() {
			return (col1 != nullptr);
		}
	};

	class BVHierarchy {
	public:
		BVHierarchy();
		~BVHierarchy();

		//Initialised by Collider system, dont use
		void Init();
		void Update();
		void End();
		void Swap(std::vector<Collider*>* toSwap);

		void ConstructHierarchy(std::vector<Collider*>& colliders);
		void Print(TreeNode* t, int _count);
		void PrintLine(TreeNode* t, int remark, int drawOnlyRemarkedOne = 1, int count = 0);
		void GetCollisionVector(std::vector<Collider*>& outVec);
		TreeNode* treeNodeAux = nullptr;

	private:
		void PrintBox(const FVector& min, const FVector& max, const FVector& color, const FMatrix4x4& model = FMatrix4x4());

		uchar8 IsColliding(const AABB* c1, const AABB* c2);

		void Recurse(TreeNode* currentNode, std::vector<Collider*>& colliders);
		uint8 GetLargestAxis(const AABB& box);
		void GetColliderVectorMinMax(const std::vector<Collider*>& colliders, FVector& Min, FVector& Max);

		void ClearTree(TreeNode* base);

		TreeNode* treeNodeBase = nullptr;
		std::vector<Collider*>* col;

		//MULTITHREAD STUFF WIP
		std::thread thread;
		uchar8 stopThread = false;
		std::atomic<uchar8> waitingForSwap = false;
	};

}
#endif // !__BVH_H__
