
#ifndef __PHYSICS_SYSTEM_H__
#define __PHYSICS_SYSTEM_H__ 1

#include "Typedef.h"
#include "Vector.h"
#include <vector>
#include "Matrix4x4.h"

namespace ENGINENAME {

	class RigidBody;
	struct CollisionResult;

	// Force calculator for any physics body
	class ForceGenerator {
	public:
		virtual void UpdateForce(RigidBody* target, float32 deltaTime) = 0;
	};

	// GRAVITY FORCE
	// Fgravity = gravityAceleration * ParticleMass
	class GravityForce : public ForceGenerator {
	public:
		virtual void UpdateForce(RigidBody* target, float32 deltaTime) override;
		FVector globalGravity;
	};

	// DRAG FORCE
	//				^				^			^
	// Fdrag = -Vparticle * (k1 * |Vp| + k2 * |Vp|^2)
	class DragForce : public ForceGenerator {
	public:
		virtual void UpdateForce(RigidBody* target, float32 deltaTime) override;

		// Drag Linear Coeficient
		float32 k1;
		// Drag Cuadratic Coeficient
		float32 k2;
	};

	class RigidBodySystem {
	public:
		RigidBodySystem();
		~RigidBodySystem();

		RigidBody* Create();
		void Update(float32 deltaTime = 0.016f) const;
		void ApplyImpulse(RigidBody* rb1, RigidBody* rb2, CollisionResult* cr, const uint32& contactNum);
		FMatrix4x4 InvTensor(RigidBody* rb);

	private:
		std::vector<RigidBody*> rigidBodies;
	};

};

#endif