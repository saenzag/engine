
#ifndef __NODE_H__
#define __NODE_H__ 1

#include "Typedef.h"
#include <string>

#include "Object.h"

namespace ENGINENAME {

	class Transform;
	class Geometry;
	class Material;
	class Physic;

	class Node : public Object {
	public:
		Node(uint32 Id);
		~Node();

		void SetName(const char8* newName);

		std::string Name() { return name; }

		void AddGeometry(const char8* path);
		void AddMaterial(const char8* shaderName);

		Geometry* GetGeometry() const { return geometry; }
		Material* GetMaterial() const { return material; }
		const uint32 GetId() const { return id; }

		void Draw();

	private:
		uint32 id;
		std::string name;

		//Components
		Geometry* geometry = nullptr;
		Material* material = nullptr;
	};

};

#endif