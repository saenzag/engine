
#ifndef __GEOMETRY_SYSTEM_H__
#define __GEOMETRY_SYSTEM_H__ 1

#include "Typedef.h"
#include "Geometry.h"
#include <vector>

namespace ENGINENAME {
	class GeometrySystem {
	public:
		GeometrySystem();
		~GeometrySystem();

		void Update();

		Geometry* CreateGeometry(const char8* path);
		void LoadGeometry(Geometry* inGeometry, const char8* filename);

	private:
		std::vector<Geometry*> geometries;

	};

}

#endif