
#ifndef __GEOMETRY_H__
#define __GEOMETRY_H__ 1

#include "Typedef.h"

#include "Vector.h"
#include "Vector2.h"

#include "Object.h"

#include <vector>

namespace ENGINENAME {

	enum VertexData {
		kPosition,
		kNormal,
		kUv,
	};

	struct Vertex {
		FVector position;
		FVector normal;
		FVector2 uv;

		INLINE int8 Vertex::operator==(const Vertex& V) const {
			return (
				position == V.position &&
				normal == V.normal &&
				uv == V.uv);
		}

		static const uint32 GetOffsetFor(VertexData vd) {
			switch (vd) {
			case VertexData::kPosition:
				return 0;
			case VertexData::kNormal:
				return sizeof(FVector);
			case VertexData::kUv:
				return 2 * sizeof(FVector);
			default:
				return 0;
			}
		}
	};

	class Geometry : public Object {
	public:
		Geometry(uint32 idNumber, std::string path);
		~Geometry();

		void SetVertex(const std::vector<Vertex> &v);
		void SetIndices(const std::vector<uint32> &i);
		void SetMaxPos(const FVector& max) { maxPosition = max; }
		void SetMinPos(const FVector& min) { minPosition = min; }
		void Prepare();

		const uint32 GetId() { return id; }
		const std::string GetPath() { return path; }
		std::vector<Vertex> GetVertex();
		std::vector<uint32> GetIndices();
		uint32& GetVAO() { return vao; }
		uint32& GetVBO() { return vbo; }

		INLINE FVector GetMin() { return minPosition; }
		INLINE FVector GetMax() { return maxPosition; }

	private:
		std::vector<Vertex> vertex;
		std::vector<uint32> indices;

		std::string path;
		FVector maxPosition, minPosition;

		uint32 id;
		uint32 vao, vbo;
	};

};

#endif