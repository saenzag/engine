
#ifndef __ERRORCONTROLLER_H__
#define __ERRORCONTROLLER_H__ 1

#include "Typedef.h"

#include <string>

namespace ENGINENAME {
	class ErrorController {
	public:
		ErrorController() { error = ErrorType::kNoError; errorString = ""; }
		~ErrorController() {}

		INLINE const uint32 HasAnyError() { return error != ErrorType::kNoError && errorString != ""; }
		INLINE const uint32 GetError() { return error; }
		INLINE const char8* GetErrorString() { return errorString.c_str(); }
		INLINE void AddError(uint32 newErrors = kNoError) { error |= newErrors; };

		//Strings
		INLINE void AddError(const char8* string) { errorString += string; };
		INLINE void AddError(std::string string) { errorString += string; };

	private:
		uint32 error;
		std::string errorString;
	};
};

#endif