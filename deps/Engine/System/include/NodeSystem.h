
#ifndef __NODE_SYSTEM_H
#define __NODE_SYSTEM_H 1

#include "Typedef.h"
#include <vector>

namespace ENGINENAME {

	class Node;

	class NodeSystem {
	public:
		NodeSystem();
		~NodeSystem();

		//returns created node's Id
		Node* CreateNode();
		Node* GetNode(uint32 id);

		void Draw();
		const uint32 GetSize() const{ return nodes.size(); }

	private:
		std::vector<Node*> nodes;
	};

};

#endif