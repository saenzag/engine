
#ifndef __RIGIDBODY_H__
#define __RIGIDBODY_H__ 1

#include "Typedef.h"

#include "Component.h"

#include "Vector.h"

namespace ENGINENAME {

	class RigidBody : public Component {
	public:
		RigidBody(uint32 Id);
		~RigidBody();

		static const ComponentType Type = CT_RigidBody;
		static RigidBody* Construct();
		void Init(){}


		void Update(const float32& deltaTime);

		// SETTERS
		void SetMass(const float32& newMass);
		void SetDamping(const float32& newDamping);
		void SetBouncyness(const float32& newBouncyness);
		void SetStatic(const char8& Static = true);
		void EnableGravity(const char8& enabled = true);
		void SetVelocity(const FVector& newVelocity);
		void SetAngularVelocity(const FVector& newAngVelocity);

		INLINE void ClearVelocity();
		INLINE void ClearAceleration();
		INLINE void ClearForce();
		INLINE void ClearTorque();
		INLINE void ClearAngularVelocity();

		void AddForce(const FVector& Force);
		void AddTorque(const FVector& Torque);
		void AddImpulse(const FVector& Impulse);

		// GETTERS
		INLINE float32 GetMass() const { return isStatic ? 0.0f : mass; }
		INLINE float32 GetMassInverse() const { return isStatic ? 0.0f : massInverse; }
		INLINE float32 GetDamping() const { return damping; }
		INLINE float32 GetBouncyness() const { return bouncyness; }

		INLINE FVector GetVelocity() const { return velocity; }
		INLINE FVector GetAngularVelocity() const { return angularVelocity; }
		INLINE FVector GetAceleration() const { return aceleration; }
		INLINE FVector GetForce() const { return force; }
		INLINE FVector GetTorque() const { return torque; }
		INLINE FVector GetOverridedGravity() const { return gravity; }

		// DATA GETTERS
		INLINE char8 IsStatic() const { return isStatic; }
		INLINE char8 HasWrongMass() const { return wrongMass; }
		INLINE char8 IsAffectedByGravity() const { return affectedbyGravity; }
		INLINE char8 IsOverridingGravity() const { return overrideGravity; }

	private:

		uint32 id;

		float32 mass;
		float32 massInverse;
		float32 damping;
		float32 bouncyness;

		char8 isStatic;
		char8 wrongMass;
		char8 affectedbyGravity;
		char8 overrideGravity;

		FVector velocity;
		FVector angularVelocity;
		FVector aceleration;
		FVector force;
		FVector torque;
		FVector gravity;
	};

};

#endif