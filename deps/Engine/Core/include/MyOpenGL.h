
#ifndef __MY_OPENGL_H__
#define __MY_OPENGL_H__ 1

#include "Typedef.h"
#include <vector>
#include "Geometry.h"

namespace ENGINENAME {

	class MyOpenGL {
	public:
		MyOpenGL();
		~MyOpenGL();

		// Hello Triangle
		static void PrepareHelloTriangle(uint32 &program, uint32 &vao);
		static void DrawHelloTriangle(uint32 &program, uint32 &vao);

		//GLEW
		static uint32 InitializeGlew();

		// Geometry
		static void GenerateVAO(uint32 &vao);
		static void GenerateVBO(uint32 &vbo);
		static void BindVertexArray(uint32 &vao);
		static void BindVertexBuffer(uint32 &vbo);

		static void SetVertexBufferData(std::vector<Vertex> &v);
		static void SetVertexBufferData(std::vector<float32> &v);
		static void SetIndicesData(std::vector<uint32> &i);

		static void SetVertexAttribArrays(uint32 enabled, uint32 num);
		static void SetVertexAttribs(uint32 index, uint32 size, uint32 offset, uint32 sizeOf = sizeof(Vertex));

		static uint32 InitShader(uint32 &shader, const char8* source, ShaderType st, std::string& errorString);
		static uint32 InitProgram(uint32 &program, uint32 &vs, uint32 &fs, std::string errorString = ""); //creates, attaches and links program
		static void UseProgram(uint32 &program);
		static uint32 GetUniform(uint32 &program, const char8* name);
		static void SetUniform(UniformType ut, uint32 uniform, void* data);

		static void DrawTriangles(uint32 numIndices);

		static void UnbindVAO();

		static void DrawLine();
	};
};

#endif