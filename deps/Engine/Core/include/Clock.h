
#ifndef __CLOCK_H__
#define __CLOCK_H__ 1

#include "Typedef.h"

#include <map>
#include <string>
#include <Windows.h>
#include <vector>
#include <thread>

namespace ENGINENAME {

	enum MeasureState : uint8 {
		kNotMeasured = 0,
		kMeasureStarted = 1,
		kMeasureDone = 2
	};

	enum MeasurePrecision : uint32 {
		k_Seconds		= 1,
		k_Miliseconds	= 1000,
		k_Microseconds	= 1000000,
	};

	struct Measure {
		LARGE_INTEGER beginTime;
		LARGE_INTEGER endTime;
		MeasureState measureState = kNotMeasured;

		void SetBeginTime(LARGE_INTEGER li){
			if (measureState != kNotMeasured) { return; }
			beginTime = li;
			measureState = kMeasureStarted;
		}
		void SetEndTime(LARGE_INTEGER li) {
			if (measureState != kMeasureStarted) { return; }
			endTime = li;
			measureState = kMeasureDone;
		}
	};

	class Clock {
	public:
		Clock();
		~Clock();

		void Init();
		void Update();

		//Update, should be called at the end of each iteration in a new thread,
		//	not neccesary on main thread
		void ThreadUpdate();

		ErrorType ShutDown(const uint8 saveInDisk = 0);

		//Save all the data into disk
		//	-optionalName: sets this name in the begining of the file name
		//	-beginPos/EndPos: frames to write in the file from and to positions. -1 to set 0 and final positions
		//	-measuresToSave: wich measures to save in disk, if empty all of them will be saved
		ErrorType SaveDataIntoDisk(const char8* optionalName = "", int32 beginPos = -1, int32 endPos = -1, std::vector<std::string> measuresToSave = std::vector<std::string>());

		ErrorType Start(const char8* Name, uint8 workOnlyWhenClockIsActive = true);
		ErrorType Stop(const char8* End, uint8 workOnlyWhenClockIsActive = true);

		void RecordFrames(uint32 framesNum) { framesToRecord = framesNum; }
		void RecordTime(float32 time) { timeToRecord = time; }

		INLINE float64 GetMeasureDuration(Measure &m, MeasurePrecision precision = k_Seconds);
		INLINE float64 GetMeasureDuration(LARGE_INTEGER &beginTime, LARGE_INTEGER &endTime, MeasurePrecision precision = k_Seconds);

		INLINE LARGE_INTEGER GetTime();
		INLINE uint32 GetCurrentFrame();

		INLINE float64 GetDeltaSeconds() const { return deltaTime * 0.000001; }
		INLINE float64 GetDeltaMiliSeconds() const { return deltaTime * 0.001; }
		INLINE float64 GetDeltaMicroSeconds() const { return deltaTime; }

		INLINE uint32 GetRemainingRecordingFrames() const { return framesToRecord; }
		INLINE float32 GetRemainingRecordingTime() const { return timeToRecord; }

	private:
		Measure systemMeasure;
		
		// FREQUENZY AND OVERHEAD
		LARGE_INTEGER frequenzy;
		//LONGLONG frequenzyInv;
		LONGLONG overhead;

		LARGE_INTEGER lastFrameTime;
		LARGE_INTEGER currentFrameTime;
		float64 deltaTime;

		//uint32 frameNumber = 0;
		std::map<std::thread::id, uint32> frameNum;
		uint8 enabled = 0;

		uint32 framesToRecord = 0;
		float32 timeToRecord = 0;

		//MAP<name,MAP<threadID,MAP<frameNumber,Measure>>>
		std::map<std::string,
			std::map<std::thread::id,
				std::map<uint32, Measure>>> measures;
	};
};

#endif