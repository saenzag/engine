
#ifndef __UI_H__
#define __UI_H__ 1

#include "Typedef.h"
#include "Vector2.h"

namespace ENGINENAME{

	class Node;

	class UI {
	public:
		UI();
		~UI();

		void Init();
		void Update();
		void End();

	private:
		Node* selectedNode = nullptr;
		void BeginFrame();
		void EndFrame();

		void MainMenu();
		void Inspector();
		void Hierarchy();
		void World();
		void Logger();
		void Profiler();
		void SaveProfiler();

		uint8 showMainMenu = true;
		uint8 showInspector = true;

		uint8 showAdvancedInformation = false;
		uint8 showCameraInformation = true;

		uint8 inspectorWorldTab = false;
		uint8 loggerProfilerTab = false;

		float64 elapsedTime = 0.0;
		float64 updateFrequenzy = 0.5;
		float64 deltaTime = 0.0;

		//WINDOW POSITIONING
		const FVector2 MenuOffset = FVector2(0.0f, 18.0f);

		const FVector2 InspectorPos = FVector2(0.8f, 0.0f);
		const FVector2 InspectorSize = FVector2(0.2f, 1.0f);

		const FVector2 HierarchyPos = FVector2(0.6f, 0.7f);
		const FVector2 HierarchySize = FVector2(0.2f, 0.3f);

		const FVector2 LoggerPos = FVector2(0.0f, 0.7f);
		const FVector2 LoggerSize = FVector2(0.6f, 0.3f);
	};
};

#endif