
#ifndef __OBJECT_MANAGER_H__
#define __OBJECT_MANAGER_H__ 1

#include "Typedef.h"
#include <vector>

namespace ENGINENAME {

	class Object;

	class ObjectManager {
	public:
		ObjectManager();
		~ObjectManager();

		void AddObject(Object* c);

	private:
		std::vector<Object*> componens;
	};

};

#endif //!__OBJECT_MANAGER_H__