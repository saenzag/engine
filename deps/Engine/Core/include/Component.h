
#ifndef __COMPONENT_H__
#define __COMPONENT_H__ 1

#include "Typedef.h"
#include "Object.h"


namespace ENGINENAME {

	class Component : public Object {
	public:
		Component();
		~Component();

		INLINE ComponentType GetComponentType() { return componentType; }
		INLINE void SetComponentType(ComponentType ct) { componentType = ct; }

		void SetOwner(Object* Owner);
		Object* GetOwner() { return owner; }

	private:
		ComponentType componentType;
		Object* owner = nullptr;
	};
};

#endif // !__COMPONENT_H__
