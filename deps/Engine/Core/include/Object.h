
#ifndef __OBJECT_H__
#define __OBJECT_H__ 1

#include "Typedef.h"

#include "Versioner.h"
#include "ErrorController.h"

#include <vector>

namespace ENGINENAME {

	class Component;

	class Object : public Versioner, public ErrorController {
	public:
		Object();
		~Object();

		template<class C>
		C* AddComponent() {

			C* comp = (C*)(GetComponent<C>());
			if (comp == nullptr) {
				comp = (C*)(C::Construct());
				comp->SetOwner(this);

				comp->Init();

				components.push_back(comp);
			}
			return comp;
		}

		template<class C>
		C* GetComponent() {
			for (Component* comp : components) {
				if (C::Type == comp->GetComponentType()) {
					return (C*)(comp);
				}
			}
			return nullptr;
		}

	private:
		Object* parent = nullptr;

		std::vector<Component*> components;
	};
};

#endif