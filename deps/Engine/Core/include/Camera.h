
#ifndef __CAMERA_H__
#define __CAMERA_H__ 1

#include "Typedef.h"
#include "Vector.h"
#include "Vector2.h"
#include "Matrix4x4.h"

#include "Component.h"

namespace ENGINENAME {

	class Window;
	class Object;

	class Camera : public Component {
	public:
		Camera();
		~Camera();

		void Init();
		void Update(float32 deltaTime = 0.016f);

		INLINE FMatrix4x4 GetViewMatrix() const { return view; }
		INLINE FMatrix4x4 GetProjectionMatrix() const { return projection; }
		INLINE FMatrix4x4 GetViewProjectionMatrix() const { return viewProjection; }

		INLINE FVector GetPosition() const { return position; }
		INLINE FVector GetForward() const { return direction; }
		INLINE FVector GetUp() const { return up; }
		INLINE FVector GetRight() const { return right; }

		INLINE float32* GetNearPlane() { return &nearPlane; }
		INLINE float32* GetFarPlane() { return &farPlane; }

		INLINE void AddVersion() { version++; }

	private:
		Window* window;

		float32 velocity;
		float32 mouseVelocity;
		float32 fov;

		FVector2 deltaCursor;

		FVector position;
		FVector direction;
		FVector right;
		FVector up;

		FMatrix4x4 view;
		FMatrix4x4 projection;
		FMatrix4x4 viewProjection;

		float32 hAngle;
		float32 vAngle;

		float32 nearPlane;
		float32 farPlane;

		uint32 version, lastVersion;
	};

}


#endif