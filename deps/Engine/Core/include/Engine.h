#ifndef __ENGINE_H__
#define __ENGINE_H__ 1

#define ENGINENAME NAME
#define ENGINENAMETEXT "ENGINE NAME STILL PENDING, I ACCEPT SUGGESTIONS"

#define FORCEINLINE  __forceinline
#define INLINE __inline

#define LEFT_HANDED_SYSTEM	(1)
#define RIGHT_HANDED_SYSTEM	(2)
#define COORDINATE_SYSTEM (LEFT_HANDED_SYSTEM)

#pragma warning (disable: 4201) // Not standar extension: struct/union

#endif