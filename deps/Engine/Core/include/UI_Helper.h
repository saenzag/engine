#include "Typedef.h"
#include "Manager.h"

#include "Vector.h"
#include "Vector2.h"
#include "Matrix4x4.h"

#include "imgui.h"

#include <string>

namespace ENGINENAME {
	void UI_AddTab(uint8 Index, const char8* Text, uint8 &currentTab) {
		const size_t TabWidth = 85;
		const size_t TabHeight = 22;

		ImGui::PushID(Index);
		ImGui::SameLine(0.0f, 0.5f);

		if (currentTab != Index) {
			ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor(15, 15, 15));			// Color on tab open
		}
		else {
			ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor(100, 100, 100));			// Color on tab closed
		}

		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor(40, 40, 40));			// Color on mouse hover in tab
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor(35, 35, 35));			// Color on click tab

		if (ImGui::Button(Text)) {//, ImVec2(TabWidth, TabHeight))) {	// If tab clicked
			currentTab = Index;
		}

		ImGui::PopStyleColor(3);
		ImGui::PopID();
	}

	void UI_SpacingSeparatorSpacing(const int32 spacing1 = 1, const int32 separatorNum = 1, const int32 spacing2 = 1) {
		for (int32 i = 0; i < spacing1; ++i) { ImGui::Spacing(); }
		for (int32 i = 0; i < separatorNum; ++i) { ImGui::Separator(); }
		for (int32 i = 0; i < spacing2; ++i) { ImGui::Spacing(); }
	}

	void UI_PrintMatrix4(const char8* name, const FMatrix4x4 &m, const char8* format = "%06.2f") {
		char8 buffer[64] = {};
		sprintf(buffer, "[%s, %s, %s, %s]", format, format, format, format);
		std::string str = name;
		if (str.size() > 0) {
			ImGui::Text(name);
		}
		for (int32 i = 0; i < 4; ++i) {
			ImGui::Text(buffer, m.V[i].X, m.V[i].Y, m.V[i].Z, m.V[i].W);
		}
	}

	void UI_PrintVector(const char8* name, const FVector &v, const char8* format = "%06.2f") {
		char8 buffer[64] = {};
		sprintf(buffer, "[%s, %s, %s]", format, format, format);
		const std::string str = name;
		if (str.size() > 0) {
			ImGui::Text(name);
		}
		ImGui::Text(buffer, v.X, v.Y, v.Z);
	}

	void UI_NextWindowPosSize(const FVector2 posOff, const FVector2 sizeOff, const FVector2 &windowModificationOffset = FVector2()){
		Manager& manager = Manager::Instance();
		const FVector2 windowSize = manager.mainWindow.GetWindowSize() - windowModificationOffset;

		FVector2 pos = (posOff * windowSize) + windowModificationOffset;
		FVector2 size = sizeOff * windowSize;
		
		ImGui::SetNextWindowPos(ImVec2(pos.X,pos.Y));
		ImGui::SetNextWindowSize(ImVec2(size.X,size.Y));
	}
}