#ifndef __MANAGER_H__
#define __MANAGER_H__ 1

#include "Typedef.h"
#include "Window.h"
#include "Camera.h"
#include "UI.h"
#include "Clock.h"

// SYSTEMS
#include "NodeSystem.h"
#include "GeometrySystem.h"
#include "TransformSystem.h"
#include "MaterialSystem.h"
#include "ShaderSystem.h"
#include "RigidBodySystem.h"
#include "ColliderSystem.h"


namespace ENGINENAME {

	struct LineRenderer;

	class Manager {
	public:
		Manager();
		~Manager();

		//Singleton
		static Manager& Instance() {
			static Manager *instance = new Manager();
			return *instance;
		}
		//Singleton

		//Methods
		void Init();
		void UpdateSystems();
		void Draw();
		void End();

		void BeginFrame();
		void EndFrame();

		//Core
		Window mainWindow;
		Camera camera;
		UI ui;
		Clock clock;

		NodeSystem nodeSystem;
		TransformSystem transformSystem;
		GeometrySystem geometrySystem;
		MaterialSystem materialSystem;
		ShaderSystem shaderSystem;
		RigidBodySystem rigidBodySystem;
		ColliderSystem colliderSystem;

		LineRenderer* lineRenderer;

		//Functions
		void Exit() { exit = 1; }
		uint8 HasToExit() { return exit; }

	private:
		uint8 exit = 0;
	};

};

#endif