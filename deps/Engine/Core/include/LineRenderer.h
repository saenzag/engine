
#ifndef __LINE_RENDERER_H__
#define __LINE_RENDERER_H__ 1


#include "Typedef.h"
#include "Vector.h"
#include "Matrix4x4.h"

#include "Manager.h"
#include "MyOpenGL.h"

#include <vector>

namespace ENGINENAME {
	//Disclaimer
	// This full structs belongs to Enrique Alemany Martinez @unyankee and the engine OutterSpace
	struct LineRenderer {

		char8 prepared = 0;
		uint32 VertexArrayBuffer;
		uint32 VertexBuffer;
		uint32 vshader;
		uint32 fshader;
		uint32 program;

		void Prepare() {
			if (prepared) { return; }
			prepared = 1;

			float pivotPoints[6] = {
				0.0f,0.0f,0.0f, // A Point
				1.0f,1.0f,1.0f, // B Point
			};

			//Vertex Init
			MyOpenGL::GenerateVAO(VertexArrayBuffer);
			MyOpenGL::BindVertexArray(VertexArrayBuffer);
			MyOpenGL::GenerateVBO(VertexBuffer);
			MyOpenGL::BindVertexBuffer(VertexBuffer);

			std::vector<float32> vv;
			vv.push_back(0.0f);
			vv.push_back(0.0f);
			vv.push_back(0.0f);
			vv.push_back(1.0f);
			vv.push_back(1.0f);
			vv.push_back(1.0f);

			MyOpenGL::SetVertexBufferData(vv);

			MyOpenGL::SetVertexAttribs(0, 3, 0, 0);
			MyOpenGL::UnbindVAO();

			//Shader Init

			const char* lineVertex = R"(
				#version 450 core
				layout (location = 0) in vec3 aPos;
				
				//uniforms
				// Here A point and B Point are stored on the translation / scaling
				uniform mat4 ABPoints;
				
				uniform mat4 myModel;
				uniform mat4 myView;
				uniform mat4 myProjection;
				
				void main() {
					vec4 worldPosition =  ABPoints * vec4(aPos,1);   //desired world position 			
					gl_Position = myProjection * myView * vec4(worldPosition.xyz,1);
				})";

			const char* lineFragment = R"(
				#version 450 core

				out vec4 FragColor;
				uniform vec3 color;
				
				void main() {
				    FragColor = vec4(color, 1.0f);
				})";

			std::string err;
			MyOpenGL::InitShader(vshader, lineVertex, ShaderType::kVertexShader, err);
			MyOpenGL::InitShader(fshader, lineFragment, ShaderType::kFragmentShader, err);
			MyOpenGL::InitProgram(program, vshader, fshader, err);
		}

		void BeginDraw() {
			MyOpenGL::UseProgram(program);
			MyOpenGL::BindVertexArray(VertexArrayBuffer);

		}

		void EndDraw() {
			uint32 zero = 0;
			//MyOpenGL::BindVertexBuffer(zero);
			MyOpenGL::UnbindVAO();
		}

		void Draw(FVector a, FVector b, FVector color, FMatrix4x4 model = FMatrix4x4()) {
			FMatrix4x4 abPoints;
			FMatrix4x4 view;
			FMatrix4x4 projection;

			FMatrix4x4::Translate(abPoints, a);

			FMatrix4x4 aux;
			FMatrix4x4::Scale(aux, b - a);

			abPoints = abPoints * aux;

			FVector4 pos;
			pos = abPoints * FVector4(0.0f, 0.0f, 0.0f, 1);

			pos = abPoints * FVector4(1.0f, 1.0f, 1.0f, 1);

			view = Manager::Instance().camera.GetViewMatrix();
			projection = Manager::Instance().camera.GetProjectionMatrix();

			MyOpenGL::SetUniform(UniformType::kUniformType_Mat4, MyOpenGL::GetUniform(program, "myModel"), &model);
			MyOpenGL::SetUniform(UniformType::kUniformType_Mat4, MyOpenGL::GetUniform(program, "myView"), &view);
			MyOpenGL::SetUniform(UniformType::kUniformType_Mat4, MyOpenGL::GetUniform(program, "myProjection"), &projection);
			MyOpenGL::SetUniform(UniformType::kUniformType_Mat4, MyOpenGL::GetUniform(program, "ABPoints"), &abPoints);
			MyOpenGL::SetUniform(UniformType::kUniformType_Vec3, MyOpenGL::GetUniform(program, "color"), &color);

			MyOpenGL::DrawLine();
		}
	};
}


#endif //!__LINE_RENDERER_H__