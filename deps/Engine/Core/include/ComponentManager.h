
#ifndef __COMPONENT_MANAGER_H__
#define __COMPONENT_MANAGER_H__ 1

#include "Typedef.h"
#include <vector>

namespace ENGINENAME {

	class Component;

	class ComponentManager {
	public:
		ComponentManager();
		~ComponentManager();

		void AddComponent(Component* c);

	private:
		std::vector<Component*> componens;
	};

};

#endif //!__COMPONENT_MANAGER_H__