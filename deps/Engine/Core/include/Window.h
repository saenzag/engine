#ifndef __WINDOW_H__
#define __WINDOW_H__ 1

#include "Typedef.h"

#include "Vector.h"
#include "Vector2.h"

#include <map>

namespace ENGINENAME {

	enum InputKeyState {
		k_NotPressed =	0,
		k_Down =		1 << 0,
		k_Up =			1 << 1,
		k_Pressed =		1 << 2,
	};

	
	enum MouseInput {
		kKey_MouseLeft = 0,
		kKey_MouseRight = 1,
		kKey_MouseMid = 2,
	};
	

	enum InputKey {
		//Signs
		kKey_Spacebar =		32,	//  
		kKey_Apostrophe =	39,	// '
		kKey_Comma =		44,	// ,
		kKey_Minus =		45,	// -
		kKey_Period =		46,	// .
		kKey_Slash =		47,	// /

		//Keyboard Numbers
		kKey_0 = 48,
		kKey_1 = 49,
		kKey_2 = 50,
		kKey_3 = 51,
		kKey_4 = 52,
		kKey_5 = 53,
		kKey_6 = 54,
		kKey_7 = 55,
		kKey_8 = 57,
		kKey_9 = 58,

		//Keyboar Letters
		kKey_A = 65,
		kKey_B = 66,
		kKey_C = 67,
		kKey_D = 68,
		kKey_E = 69,
		kKey_F = 70,
		kKey_G = 71,
		kKey_H = 72,
		kKey_I = 73,
		kKey_J = 74,
		kKey_K = 75,
		kKey_L = 76,
		kKey_M = 77,
		kKey_N = 78,
		kKey_O = 79,
		kKey_P = 80,
		kKey_Q = 81,
		kKey_R = 82,
		kKey_S = 83,
		kKey_T = 84,
		kKey_U = 85,
		kKey_V = 86,
		kKey_W = 87,
		kKey_X = 88,
		kKey_Y = 89,
		kKey_Z = 90,

		//Numpad 
		kKey_NumPad0 = 320,
		kKey_NumPad1 = 321,
		kKey_NumPad2 = 322,
		kKey_NumPad3 = 323,
		kKey_NumPad4 = 324,
		kKey_NumPad5 = 325,
		kKey_NumPad6 = 326,
		kKey_NumPad7 = 327,
		kKey_NumPad8 = 328,
		kKey_NumPad9 = 329,

		//FUNCTION KEYS
		kKey_Esc =			256,
		kKey_Enter =		257,
		kKey_Tabulation =	258,
		kKey_Backspace =	259,
		kKey_Insert =		260,
		kKey_Delete =		261,
		
		//Arrow Keys
		kKey_Right =		262,
		kKey_Left =			263,
		kKey_Down =			264,
		kKey_Up =			265,

		//
		kKey_PageUp =		266,
		kKey_PageDown =		267,
		kKey_PrintScreen =	283,

		//F keys
		kKey_F1 =	290,
		kKey_F2 =	291,
		kKey_F3 =	292,
		kKey_F4 =	293,
		kKey_F5 =	294,
		kKey_F6 =	295,
		kKey_F7 =	296,
		kKey_F8 =	297,
		kKey_F9 =	298,
		kKey_F10 =	299,
		kKey_F11 =	300,
		kKey_F12 =	301,

	};

	enum WindowMode {
		kWindowMode_FullScreen	= 0,
		kWindowMode_Maximized	= 1,
		kWindowMode_Windowed	= 2,
	};

	class Window {
	public:
		Window();
		~Window();

		void Init(FVector2 windowSize = FVector2(1920.0f, 1080.0f));
		void ShutDown();
		void Render();
		void ClearWindow();

		void ChangeWindowMode(WindowMode newWindowMode);
		void UpdateInputs();

		//Setters
		void SetWindowSize(FVector2 newSize);
		void SetAspectRatio(float32 newAspectRatio) { aspectRatio = newAspectRatio; }
		void SetClearColor(FVector newClearColor = FVector::One);
		void SetMousePosition(FVector2 position);

		//Getters
		FVector2 GetWindowSize() { return windowSize_; }
		float32 GetAspectRatio() { return aspectRatio; }
		FVector2 GetMousePosition();
		int32 IsClosed();
		InputKeyState GetInput(InputKey key);
		InputKeyState GetMouseInput(MouseInput input);
		void* GetWindow();

	private:
		WindowMode windowMode_ = kWindowMode_FullScreen;
		FVector2 windowSize_ = FVector2(1920.0f, 1080.0f);
		float32 aspectRatio = 1920.0f / 1080.0f;
		FVector clearColor_ = FVector(0.0f);
		bool isOpen_ = true;

		std::map<InputKey, InputKeyState> inputs;
		std::map<MouseInput, InputKeyState> mouseInputs;

		struct WindowData;
		WindowData* windowData_ = nullptr;

		//methods
		void UpdateInput(InputKeyState &keystate, int32 keyPressed);
	};
};

#endif