
#ifndef __TYPEDEF_H__
#define __TYPEDEF_H__ 1

#include "Engine.h"

#ifdef ENGINE_WIN32
#include <stdint.h>

typedef char			char8;	// char
typedef unsigned char	uchar8;	// unsigned char
typedef float			float32;// float
typedef double			float64;// double

typedef ::int8_t		int8;	// char bool
typedef ::int16_t		int16;  // short
typedef ::int32_t		int32;  // int
typedef ::int64_t		int64;  // long

typedef ::uint8_t		uint8;	// unsigned char bool
typedef ::uint16_t		uint16;	// unsigned short
typedef ::uint32_t		uint32;	// unsigned int
typedef ::uint64_t		uint64;	// unsigned long

////////////////////////////////////////////////////////////////
//                           ERRORS                           //
////////////////////////////////////////////////////////////////
enum ErrorType : uint32 {
	// No Errors
	kNoError =				0,

	// Wrong File Path
	kWrongPath =			1 << 0,

	// Error compiling Shader
	kShaderCompilation =	1 << 1,

	// Error linking Shader
	kShaderLinking =		1 << 2,

	// Something has not been initialized
	kNotInitialized =		1 << 3,

	// This Function Call has been done already
	kRepeatingCall =		1 << 4,

	// Some Calling is Necessary before this one
	kMissingCall =			1 << 5,
};

////////////////////////////////////////////////////////////////
//                        SHADER TYPES                        //
////////////////////////////////////////////////////////////////
enum ShaderType {
	kVertexShader,
	kFragmentShader,
};

////////////////////////////////////////////////////////////////
//                    SHADER UNIFORM TYPES                    //
////////////////////////////////////////////////////////////////
enum UniformType {
	kUniformType_Integer,
	kUniformType_Float,
	kUniformType_Vec3,
	kUniformType_Vec4,
	kUniformType_Mat3,
	kUniformType_Mat4,
};

////////////////////////////////////////////////////////////////
//                       COMPONENT TYPES                      //
////////////////////////////////////////////////////////////////
enum ComponentType {
	CT_Camera,
	CT_Transform,
	CT_Collider,
	CT_RigidBody,
};

#endif // enginewin

#endif