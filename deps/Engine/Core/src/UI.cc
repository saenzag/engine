
#include "UI.h"
#include "UI_Helper.h"
#include "Manager.h"
#include "MyMath.h"

#include "Node.h"
#include "Transform.h"
#include "RigidBody.h"

#include "imgui.h"
#include "imgui_glfw.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "LineRenderer.h"
#include "Collider.h"

using namespace ENGINENAME;

#define UI_INSPECTOR 1
#define UI_HIERARCHY 1
#define UI_LOGGER 0

UI::UI() {
}

UI::~UI() {
}

void UI::Init() {
	Manager& manager = Manager::Instance();
	//IMGUI
	GLFWwindow* windowRef = (GLFWwindow*)manager.mainWindow.GetWindow();

	ImGui::CreateContext();
	ImGui_ImplGlfw_InitForOpenGL(windowRef, true);
	ImGui_ImplOpenGL3_Init();

	ImGui::StyleColorsDark();
}

void UI::End() {
	ImGui_ImplGlfw_Shutdown();
}

void UI::Update() {
	BeginFrame();

	if (ImGui::Begin("Test")) {

		static int value = 0;
		ImGui::DragInt("to mark", &value, 0.05f, 0, 10);
		MyMath::Clamp(value, 0, 10);

		static bool drawOnly = true;
		ImGui::Checkbox("draw only remarked one", &drawOnly);
		
		Manager& manager = Manager::Instance();

		//manager.colliderSystem.bvHierachy.PrintLine(manager.colliderSystem.bvHierachy.treeNodeAux, value, drawOnly);

		ImGui::End();
	}

	if (showMainMenu) { MainMenu(); }

#if UI_INSPECTOR

	UI_NextWindowPosSize(InspectorPos, InspectorSize, MenuOffset);
	if (ImGui::Begin(inspectorWorldTab == 0 ? "Inspector" : "World",
		NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize)) {
		UI_AddTab(0, "Inspector", inspectorWorldTab);
		UI_AddTab(1, "World", inspectorWorldTab);

		switch (inspectorWorldTab) {
		case 0: // inspector
			Inspector();
			break;
		case 1: // world
			World();
			break;
		}

		ImGui::End();
	}
#endif // UI_INSPECTOR

#if UI_HIERARCHY
	Hierarchy();
#endif

#if UI_LOGGER
	UI_NextWindowPosSize(LoggerPos, LoggerSize, MenuOffset);
	ImGui::Begin(loggerProfilerTab == 0 ? "Logger" : "Profiler", NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

	UI_AddTab(0, "Logger", loggerProfilerTab);
	UI_AddTab(1, "Profiler", loggerProfilerTab);
	switch (loggerProfilerTab) {
	case 0:
		Logger();
		break;
	case 1:
		Profiler();
		break;
	}
	ImGui::End();

#endif

	EndFrame();
}

void UI::BeginFrame() {
	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

void UI::EndFrame() {
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void UI::MainMenu() {
	Manager& manager = Manager::Instance();

	if (ImGui::BeginMainMenuBar()) {

		if (ImGui::BeginMenu("File")) {

			ImGui::Separator();
			if (ImGui::MenuItem("Exit", "Alt + F4")) {
				manager.Exit();
			}

			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("View")) {
			if (ImGui::MenuItem("Advanced Data", "", showAdvancedInformation ? true : NULL)) {
				showAdvancedInformation = !showAdvancedInformation;
			}
			if (ImGui::MenuItem("Camera Data", "", showCameraInformation ? true : NULL)) {
				showCameraInformation = !showCameraInformation;
			}

			ImGui::EndMenu();
		}

		ImGui::EndMainMenuBar();
	}
}

void UI::Inspector() {
	if (selectedNode == nullptr) { return; }

	////////////////////////////////
	//          TRANSFORM         //
	////////////////////////////////

	Transform* transform = selectedNode->GetComponent<Transform>();
	if (ImGui::CollapsingHeader("Transform", NULL, ImGuiTreeNodeFlags_Leaf) && transform != nullptr) {
		static float32 TransformDragSpeed = 0.1f;

		FVector newPosition = transform->GetPosition();
		ImGui::Text("Position: "); ImGui::SameLine();
		if (ImGui::DragFloat3("##position", newPosition.V, TransformDragSpeed, 0.0f, 0.0f, "%0.2f")) {
			transform->SetPosition(newPosition);
		}

		FVector newRotation = MATH_RADTODEG(transform->GetRotation());
		ImGui::Text("Rotation: "); ImGui::SameLine();
		if (ImGui::DragFloat3("##rotation", newRotation.V, TransformDragSpeed, 0.0f, 0.0f, "%0.2f")) {
			transform->SetRotation(MATH_DEGTORAD(newRotation));
		}

		FVector newScale = transform->GetScale();
		ImGui::Text("Scale:    "); ImGui::SameLine();
		if (ImGui::DragFloat3("##scale", newScale.V, TransformDragSpeed, 0.0f, 0.0f, "%0.2f")) {
			transform->SetScale(newScale);
		}

		ImGui::Text("Drag Vel: "); ImGui::SameLine();
		ImGui::DragFloat("##DragSpeed", &TransformDragSpeed, 0.01f, 0.01f, 10.0f, "%0.2f");

		if (showAdvancedInformation) {
			UI_SpacingSeparatorSpacing();
			UI_PrintMatrix4("Model Matrix: ", transform->GetMatrix());

			ImGui::Spacing();

			ImGui::Text("Version: %d", transform->GetVersion());
			if (transform->IsDirty()) {
				ImGui::Text("Updated: FALSE");
			}
			else {
				ImGui::Text("Updated: TRUE");
			}
		}
	}

	////////////////////////////////
	//          GEOMETRY          //
	////////////////////////////////
	Geometry* g = selectedNode->GetGeometry();
	if (g != nullptr) {
		ImGui::Spacing();
		ImGui::Spacing();
		if (ImGui::CollapsingHeader("Geometry##Geomemtryoninspector")) {
			ImGui::Spacing();

			ImGui::Text("File: ");
			ImGui::Text(("   " + g->GetPath()).c_str());
			UI_SpacingSeparatorSpacing();

			if (g->IsDataLoaded()) {
				ImGui::Text("Is Loaded:   TRUE");
			}
			else {
				ImGui::Text("Is Loaded:   FALSE");
			}

			if (g->IsDataPrepared()) {
				ImGui::Text("Is Prepared: TRUE");
			}
			else {
				ImGui::Text("Is Prepared: FALSE");
			}
			if (g->HasAnyError()) {
				ImGui::Text("Geometry Has some error. Errorcode %d", g->GetError());
			}

		}
	}

	////////////////////////////////
	//          MATERIAL          //
	////////////////////////////////
	Material* m = selectedNode->GetMaterial();
	if (m) {
		if (ImGui::CollapsingHeader("Material##MaterialCollapsingHeader")) {
			ImGui::Text("Shader:");
			ImGui::Text(("   " + m->shader->GetName()).c_str());

			ImGui::Spacing();

			FVector albedo;
			albedo = m->GetAlbedo();
			if (ImGui::ColorEdit3("Albedo##MaterialAlbedo", albedo.V, ImGuiColorEditFlags_PickerHueBar)) {

				albedo = MyMath::Clamp(albedo, 0.0f, 1.0f);

				m->SetAlbedo(FVector(albedo));
			}
		}
	}

	////////////////////////////////
	//           PHYSICS          //
	////////////////////////////////
	RigidBody* rb = selectedNode->GetComponent<RigidBody>();
	if (rb) {
		if (ImGui::CollapsingHeader("RigidBody##PhysicsCollapsingHeader", ImGuiTreeNodeFlags_DefaultOpen)) {

			bool isStatic = rb->IsStatic();
			ImGui::Text("Static  "); ImGui::SameLine();
			if (ImGui::Checkbox("##isphysicsStatic", &isStatic)) {
				rb->SetStatic(isStatic);
			}

			ImGui::SameLine();
			ImGui::Text("Enabled Gravity"); ImGui::SameLine();
			bool affectedByGravity = rb->IsAffectedByGravity();
			if (ImGui::Checkbox("##PhysicsEnabledGravity", &affectedByGravity)) {
				rb->EnableGravity(affectedByGravity);
			}

			ImGui::Text("Mass    "); ImGui::SameLine();
			float32 mass = rb->GetMass();
			if (ImGui::DragFloat("##PhysicsDragMass", &mass, 0.01f, 0.0f, 0.0f, "%.3f Kg")) {
				rb->SetMass(mass);
			}

			ImGui::Text("Damping "); ImGui::SameLine();
			float32 damping = rb->GetDamping();
			if (ImGui::DragFloat("##PhysicsDampingDrag", &damping, 0.01f, 0.0f, 0.0f, "%.3f")) {
				rb->SetDamping(damping);
			}

			UI_SpacingSeparatorSpacing();

			UI_PrintVector("Velocity:", rb->GetVelocity());
			UI_PrintVector("Aceleration:", rb->GetAceleration());
			UI_PrintVector("Force:", rb->GetForce());

			UI_SpacingSeparatorSpacing();

			ImGui::Text("Apply Force"); ImGui::SameLine();
			static bool applyforceToPhisics = false;
			if (ImGui::Checkbox("##ApplyForceToBody", &applyforceToPhisics)) {
			}

			static FVector forceUI = FVector::Zero;

			ImGui::DragFloat3("##dragforcefloat", forceUI.V);

			if (applyforceToPhisics) {
				rb->AddForce(forceUI);
			}

			ImGui::Spacing();
			static FVector impulseUI = FVector::Zero;
			if (ImGui::Button("Apply Impulse##addimpulsetophysics")) {
				rb->AddImpulse(impulseUI);
			}
			ImGui::DragFloat3("##dragimpulsefloat", impulseUI.V);
		}
	}
}

void UI::World() {
	Manager& manager = Manager::Instance();

	elapsedTime += manager.clock.GetDeltaSeconds();
	if (elapsedTime > updateFrequenzy) {
		elapsedTime = 0.0;
		deltaTime = manager.clock.GetDeltaMiliSeconds();
	}

	// System Information
	UI_SpacingSeparatorSpacing(2, 0, 0);

	ImGui::Text("Delta Time %0.2f (ms)", deltaTime);
	UI_SpacingSeparatorSpacing(2, 0, 0);

	if (showCameraInformation) {
		if (ImGui::CollapsingHeader("Camera", NULL, ImGuiTreeNodeFlags_Leaf)) {
			FVector forward = manager.camera.GetForward();
			FVector up = manager.camera.GetUp();
			FVector right = manager.camera.GetRight();

			ImGui::Text("Forward [%05.2f,%05.2f,%05.2f]", forward.X, forward.Y, forward.Z);
			ImGui::Text("Up      [%05.2f,%05.2f,%05.2f]", up.X, up.Y, up.Z);
			ImGui::Text("Right   [%05.2f,%05.2f,%05.2f]", right.X, right.Y, right.Z);

			UI_SpacingSeparatorSpacing();

			ImGui::Text("NearPlane: "); ImGui::SameLine();
			if (ImGui::DragFloat("##nearPlane", manager.camera.GetNearPlane(), 0.05f, 0.01f, 10.0f, "%0.2f")) {
				manager.camera.AddVersion();
			}
			ImGui::Text("FarPlane:  "); ImGui::SameLine();
			if (ImGui::DragFloat("##farPlane", manager.camera.GetFarPlane(), 0.05f, *manager.camera.GetNearPlane() + 0.01f, 1000.0f, "%0.2f")) {
				manager.camera.AddVersion();
			}

			if (showAdvancedInformation) {
				UI_SpacingSeparatorSpacing();
				UI_PrintMatrix4("View Matrix: ", manager.camera.GetViewMatrix());
				UI_SpacingSeparatorSpacing(2, 0, 0);
				UI_PrintMatrix4("Projection Matrix: ", manager.camera.GetProjectionMatrix());
			}
		}
	}
}

void UI::Hierarchy() {
	Manager& manager = Manager::Instance();

	UI_NextWindowPosSize(HierarchyPos, HierarchySize, MenuOffset);
	ImGui::Begin("Hierarchy", NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);

	for (uint32 i = 0; i < manager.nodeSystem.GetSize(); ++i) {
		Node* n = manager.nodeSystem.GetNode(i);

		if (ImGui::RadioButton(n->Name().c_str(), selectedNode == nullptr ? false : i == selectedNode->GetId())) {
			selectedNode = manager.nodeSystem.GetNode(i);
		}

	}
	ImGui::End();

}

void UI::Logger() {

}

void UI::Profiler() {

	static uint8 showProfilerSave = true;
	if (ImGui::Button("Save")) {
		if (!showProfilerSave) {
			ImGui::SetNextWindowPos(ImVec2(100, 100));
			ImGui::SetNextWindowSize(ImVec2(300, 400));

			showProfilerSave = true;
		}
	}
	if (showProfilerSave) {
		ImGui::Begin("Profiler Save Options##", (bool*)&showProfilerSave, ImGuiWindowFlags_NoCollapse);
		if (!ImGui::IsWindowFocused()) {
			showProfilerSave = false;
		}
		SaveProfiler();
		ImGui::End();
	}


}

void UI::SaveProfiler() {
	Manager& manager = Manager::Instance();

	const float32 windowWidth = ImGui::GetWindowWidth();
	const float32 windowHeight = ImGui::GetWindowHeight();

	static std::vector<std::string> filters;
	static uint32 filterNum = 0;
	static uint8 SaveAllMeasures = true;

	static int32 recordFrames = 0;
	ImGui::PushItemWidth(windowWidth * 0.5f);
	ImGui::DragInt("##recordFrames", &recordFrames, 1.0f, 0, INT_MAX);
	ImGui::SameLine();
	if (ImGui::Button("RecordFrames")) {
		manager.clock.RecordFrames(recordFrames < 0 ? 0 : recordFrames + 1);
	}
	if (uint32 rrf = manager.clock.GetRemainingRecordingFrames()) {
		ImGui::SameLine();
		ImGui::Text("Recording Frames Left: %d", rrf);
	}

	static float32 recordTime = 0;
	ImGui::PushItemWidth(windowWidth * 0.5f);
	ImGui::DragFloat("##recordTime", &recordTime, 0.1f, 0.0f, (float32)INT_MAX, "%0.2f");
	ImGui::SameLine();
	if (ImGui::Button(" RecordTime ")) {
		manager.clock.RecordTime(recordTime < 0.0f ? 0.0f : recordTime + (float32)manager.clock.GetDeltaSeconds());
	}
	if (float32 rrt = manager.clock.GetRemainingRecordingTime()) {
		ImGui::SameLine();
		ImGui::Text("Recording Time Left:   %0.2f", rrt);
	}

	UI_SpacingSeparatorSpacing();

	static char8 buffer[16] = {};
	ImGui::Text("File Name:");
	ImGui::PushItemWidth(windowWidth * 0.5f);
	ImGui::InputText("##inputText", buffer, 16); ImGui::SameLine();
	//Save all inputs
	if (ImGui::Button("SaveAll")) {
		manager.clock.SaveDataIntoDisk(buffer, -1, -1, SaveAllMeasures ? std::vector<std::string>() : filters);
	}

	//SaveFromTo frames
	static int32 from = 0;
	static int32 to = 1;

	const float widthOff = windowWidth - 12.0f;

	ImGui::PushItemWidth(widthOff * 0.25f);
	if (ImGui::DragInt("##savefrom", &from, 1.0f, 0, INT_MAX - 1, "From %d")) {
		if (from >= to) { to = from + 1; }
	}
	if (from < 0) { from = 0; }

	ImGui::SameLine();
	ImGui::PushItemWidth(widthOff * 0.25f);
	if (ImGui::DragInt("##saveto", &to, 1.0f, 1, INT_MAX, "To %d")) {
		if (to <= from) { from = to - 1; }
	}
	if (to < 1) { to = 0; }

	ImGui::SameLine();
	if (ImGui::Button("Save FromTo")) {
		manager.clock.SaveDataIntoDisk(buffer, from, to, SaveAllMeasures ? std::vector<std::string>() : filters);
	}

	UI_SpacingSeparatorSpacing();

	ImGui::Text("Save all Measures"); ImGui::SameLine();
	ImGui::Checkbox("##saveallmeasures", (bool*)&SaveAllMeasures);

	if (!SaveAllMeasures) {
		ImGui::SameLine();
		if (ImGui::Button("+##")) {
			filterNum++;
			filters.push_back("");
		}
		ImGui::SameLine();
		if (ImGui::Button("-##")) {
			if (filterNum > 0) {
				filterNum--;
				filters.pop_back();
			}
		}

		for (uint32 i = 0; i < filterNum; ++i) {
			char8 auxbuffer[64] = {};
			sprintf(auxbuffer, "%s\0", filters[i].c_str());
			std::string id = "#";
			id += std::to_string(i);

			if (ImGui::InputText(id.c_str(), auxbuffer, 64)) {
				filters[i] = auxbuffer;
			}
		}
	}


}