
#include "Clock.h"

#include <time.h>
#include <sstream>

using namespace ENGINENAME;

Clock::Clock() {
}

Clock::~Clock() {
}

//begin measure
ErrorType Clock::Start(const char8* name, uint8 workOnlyWhenClockIsActive) {

	if (workOnlyWhenClockIsActive) {// &&
		if (framesToRecord == 0) {// &&
			if (timeToRecord == 0.0f) {
				// If only can record when the clock is active and
				//	the clock isn't active dont do anything
				return ErrorType::kNoError;
			}
		}
	}

	std::string nameString = name;
	std::thread::id threadID = std::this_thread::get_id();
	if (measures.count(nameString)) {
		// Already Exists
		//		find this name					find this thread		 find thread number
		if (((measures.find(nameString)->second).find(threadID)->second).count(GetCurrentFrame())) {
			// this frame has been Measured on this thread
			//...
			return ErrorType::kRepeatingCall;
		}
		else {
			// this frame has not been measured on this thread
			Measure newMeasure;
			newMeasure.SetBeginTime(GetTime());


			measures[nameString][threadID][GetCurrentFrame()] = newMeasure;

			return ErrorType::kNoError;
		}
	}
	else {
		// Doesnt Exist this measure
		Measure newMeasure;
		newMeasure.SetBeginTime(GetTime());
		measures[nameString][threadID][GetCurrentFrame()] = newMeasure;

		return ErrorType::kNoError;
	}
}

//end measure
ErrorType Clock::Stop(const char8* name, uint8 workOnlyWhenClockIsActive) {

	if (workOnlyWhenClockIsActive) {
		if (framesToRecord == 0) {
			if (timeToRecord == 0.0f) {
				// If only can record when the clock is active and
				//	the clock isn't active dont do anything
				return ErrorType::kNoError;
			}
		}
	}

	std::string nameString = name;
	std::thread::id threadID = std::this_thread::get_id();
	if (measures.count(nameString)) {
		//Already Exists Name
		uint32 currentFrame = GetCurrentFrame();
		if (measures.find(nameString)->second.find(threadID)->second.count(currentFrame)) {
			// This frame exists on this frame
			measures[nameString][threadID][currentFrame].SetEndTime(GetTime());

			return ErrorType::kNoError;
		} else {
			// This frame doesnt exist
			return ErrorType::kMissingCall;
		}
	}

	return ErrorType::kMissingCall;
}

void Clock::Init() {
	srand((uint32)time((time_t*)NULL));
	// Get System Begin Time
	QueryPerformanceCounter(&systemMeasure.beginTime);

	// Get Frequenzy
	QueryPerformanceFrequency(&frequenzy);
	//frequenzyInv = 1.0 / (float64)frequenzy.QuadPart;

	// Calculate Overhead
	LARGE_INTEGER b, e;
	QueryPerformanceCounter(&b);
	QueryPerformanceCounter(&e);
	overhead = e.QuadPart - b.QuadPart;

	//get first frame times
	Update();
}

void Clock::Update() {
	lastFrameTime = currentFrameTime;
	QueryPerformanceCounter(&currentFrameTime);
	deltaTime = GetMeasureDuration(lastFrameTime, currentFrameTime, MeasurePrecision::k_Microseconds);

	if (framesToRecord > 0) { framesToRecord--; }
	if (timeToRecord > 0.0f) {
		float64 ds = GetDeltaSeconds();
		timeToRecord -= ds < timeToRecord ? (float32)ds : timeToRecord;
	}

	ThreadUpdate();
	//frameNumber++;
}

void Clock::ThreadUpdate() {
	++frameNum[std::this_thread::get_id()];
}

ErrorType Clock::ShutDown(const uint8 saveInDisk) {
	QueryPerformanceCounter(&systemMeasure.endTime);

	if (saveInDisk) {
		return SaveDataIntoDisk();
	}
	return kNoError;
}

LARGE_INTEGER Clock::GetTime() {
	LARGE_INTEGER t;
	QueryPerformanceCounter(&t);
	return t;
}

uint32 Clock::GetCurrentFrame() {
	return frameNum[std::this_thread::get_id()];
}

float64 Clock::GetMeasureDuration(Measure &m, MeasurePrecision precision) {
	return ((m.endTime.QuadPart - m.beginTime.QuadPart - overhead) * (float64)precision / frequenzy.QuadPart);
}

float64 Clock::GetMeasureDuration(LARGE_INTEGER &beginTime, LARGE_INTEGER &endTime, MeasurePrecision precision) {
	return ((endTime.QuadPart - beginTime.QuadPart - overhead) * (float64)precision / frequenzy.QuadPart);
}

ErrorType Clock::SaveDataIntoDisk(const char8* optionalName, int32 beginPos, int32 endPos, std::vector<std::string> measuresToSave) {
	QueryPerformanceCounter(&systemMeasure.endTime);
	FILE* f;
	std::string buffer;
	//set file path in buffer
	SYSTEMTIME st;
	GetSystemTime(&st);

	buffer = "../data/clock/";
	std::string aux = optionalName;
	if (aux.size() > 1) {
		buffer += optionalName;
		buffer += "_";
	}
	buffer += (st.wDay < 10 ? "0" : "") + std::to_string(st.wDay);
	buffer += (st.wMonth < 10 ? "0" : "") + std::to_string(st.wMonth);
	buffer += std::to_string(st.wYear).substr(2) + "_";
	buffer += (st.wHour < 10 ? "0" : "") + std::to_string(st.wHour);
	buffer += (st.wMinute < 10 ? "0" : "") + std::to_string(st.wMinute);
	buffer += (st.wSecond < 10 ? "0" : "") + std::to_string(st.wSecond);
	buffer += ".txt";

	// OPEN FILE
	f = fopen(buffer.c_str(), "w");
	if (!f) {
		return ErrorType::kWrongPath;
	}

	// WRITE SYSTEM SPECIFICATIONS
	buffer = "SystemDuration -> ";
	buffer += std::to_string(GetMeasureDuration(systemMeasure));
	buffer += " s\n";

	fwrite(buffer.c_str(), sizeof(char8), buffer.size(), f);
	uint32 nameCount = 0;
	int32 frameCount = 0;
	uint32 threadCount = 0;
	const uint32 filtersSize = measuresToSave.size();

	for (auto& name : measures) {
		//New Measure name

		//Name filters
		if (filtersSize > 0) {
			uint8 found = 0;
			for (uint32 i = 0; i < filtersSize; ++i) {
				if (name.first == measuresToSave[i]) {
					found = 1;
					break;
				}
			}
			if (!found) { continue; }
		}

		buffer = "[" + std::to_string(nameCount) + "] Name ->" + name.first;
		buffer += "\n";
		fwrite(buffer.c_str(), sizeof(char8), buffer.size(), f);

		threadCount = 0;
		for (auto& thread : name.second) {

			std::ostringstream ss;
			ss << thread.first;
			buffer = "   [" + std::to_string(threadCount) + "] Thread -> " + ss.str();
			fwrite(buffer.c_str(), sizeof(char8), buffer.size(), f);
			
			float32 avgTime = 0.0f;
			buffer = "";
			for (auto& frame : thread.second) {
				//Begin Pos
				if (beginPos != -1) { if (frameCount < beginPos) { frameCount++; continue; } }
				//End Pos
				if (endPos != -1) { if (frameCount > endPos) { frameCount++; continue; } }

				buffer += "      [" + std::to_string(frameCount) + "] Frame ";
				buffer += std::to_string(frame.first);
				buffer += " - Duration ";
				float64 measureDuration = GetMeasureDuration(frame.second, MeasurePrecision::k_Miliseconds);
				buffer += std::to_string(measureDuration);
				buffer += " (ms)\n";

				avgTime += measureDuration;
				frameCount++;
			}
			std::string avgtimebuffer = " - Avg: " + std::to_string(avgTime / (float32)frameCount) + " ms\n";
			fwrite(avgtimebuffer.c_str(), sizeof(char8), avgtimebuffer.size(), f);

			fwrite(buffer.c_str(), sizeof(char8), buffer.size(), f);

			threadCount++;
			frameCount = 0;
		}
		nameCount++;
	}
	//Close FILE
	fclose(f);
	return kNoError;
}

