
#include "Camera.h"
#include "Manager.h"
#include "MyMath.h"

using namespace ENGINENAME;

Camera::Camera() {
	fov = 45.0f;
	velocity = 8.0f;
	mouseVelocity = 0.003f;

	hAngle = MATH_HALF_PI;
	vAngle = 0.0f;
	position = FVector(0.0f, 0.0f, -50.0f);
	version = 0;
	lastVersion = 0;
}

Camera::~Camera() {
}

void Camera::Init() {
	Manager& manager = Manager::Instance();

	window = &manager.mainWindow;

	nearPlane = 0.1f;
	farPlane = 1000.0f;

	version++;
}

void Camera::Update(float32 deltaTime) {

	if (window->GetMouseInput(MouseInput::kKey_MouseRight) == InputKeyState::k_Down) {
		deltaCursor = window->GetMousePosition();
	}

	if (window->GetMouseInput(MouseInput::kKey_MouseRight)) {
		FVector2 mousePos = window->GetMousePosition();

		if (deltaCursor != window->GetMousePosition()) {
			hAngle += mouseVelocity * (deltaCursor.X - mousePos.X);
			vAngle += mouseVelocity * (deltaCursor.Y - mousePos.Y);

			if (vAngle > MATH_HALF_PI) {
				vAngle = MATH_HALF_PI;
			} else if (vAngle < -MATH_HALF_PI) {
				vAngle = -MATH_HALF_PI;
			}

			version++;
			window->SetMousePosition(deltaCursor);
		}

		if (window->GetInput(InputKey::kKey_W)) {
			position += direction * velocity * deltaTime;
			version++;
		}

		if (window->GetInput(InputKey::kKey_S)) {
			position -= direction * velocity * deltaTime;
			version++;
		}

		if (window->GetInput(InputKey::kKey_A)) {
			position += right * velocity * deltaTime;
			version++;
		}

		if (window->GetInput(InputKey::kKey_D)) {
			position -= right * velocity * deltaTime;
			version++;
		}

		if (window->GetInput(InputKey::kKey_Q)) {
			position -= FVector::Up * velocity * deltaTime;
			version++;
		}

		if (window->GetInput(InputKey::kKey_E)) {
			position += FVector::Up * velocity * deltaTime;
			version++;
		}
	}

	if (version != lastVersion) {
		lastVersion = version;

		direction = FVector(cos(vAngle) * cos(hAngle),
			sin(vAngle),
			cos(vAngle) * sin(hAngle));

		right = MyMath::Cross(direction, FVector::Up);
		//FVector(
			//sin(hAngle - MATH_HALF_PI),
			//0.0f,
			//cos(hAngle - MATH_HALF_PI));

		up = MyMath::Cross(right, direction);

		view = MyMath::LookAt(position,	position + direction, up);

		projection = MyMath::Perspective(MATH_DEGTORAD(fov),
			window->GetAspectRatio(),
			nearPlane, farPlane);

		viewProjection = projection * view;
	}
}