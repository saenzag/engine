
#include <stdio.h>

#include "Typedef.h"
#include "Manager.h"
#include "MyMath.h"

#include "Node.h"
#include "RigidBody.h"
#include "Collider.h"

#include <math.h>
#include <algorithm>

using namespace ENGINENAME;

#define WORKONLYWHENCLOCKACTIVE 1

#define MEASURE(name, func)									\
		manager.clock.Start(name,WORKONLYWHENCLOCKACTIVE);	\
		func;												\
		manager.clock.Stop(name,WORKONLYWHENCLOCKACTIVE);

int32 main() {

	Manager& manager = Manager::Instance();

	manager.mainWindow.Init(FVector2(1360.0f, 800.0f));
	manager.mainWindow.SetClearColor(FVector(0.8f));

	manager.Init();

	static const int nodeAmmount = 3;
	Node* n[nodeAmmount];

	for (int32 i = 0; i < nodeAmmount; ++i) {
		n[i] = manager.nodeSystem.CreateNode();

		if (i % 2) {
			n[i]->AddGeometry("../data/sphere.obj"); 
		} else {
			n[i]->AddGeometry("../data/cube.obj"); 
		}

		n[i]->AddMaterial("basicShader");
		n[i]->GetMaterial()->SetAlbedo(FVector(
			0.01f * (float32)(rand() % 100),
			0.01f * (float32)(rand() % 100),
			0.01f * (float32)(rand() % 100)));

		Transform* tr = n[i]->GetComponent<Transform>();
	
		//*
		tr->SetPosition(FVector(
			-20.0f + (float32)(rand() % 40),
			-20.0f + (float32)(rand() % 40),
			-20.0f + (float32)(rand() % 40)));
			//*/

		/*
		tr->SetPosition(FVector(
			((float32)(-100 + rand() % 200)) / 100.0f,
			((float32)(-100 + rand() % 200)) / 100.0f,
			5.0f * i));//*/

		//tr->SetPosition(FVector::Zero);
	
		//*
		tr->SetRotation(FVector(
			-MATH_HALF_PI + (float32)(rand() % 6),
			-MATH_HALF_PI + (float32)(rand() % 6),
			-MATH_HALF_PI + (float32)(rand() % 6)));//*/

		std::string name = "Node_";
		name += (i > 9) ? std::to_string(i) : "0" + std::to_string(i);
		name += (i % 2) ? "_Sphere" : "_Cube";

		n[i]->SetName(name.c_str());

		n[i]->AddComponent<RigidBody>()->EnableGravity(true);

		if (i % 2) {
			n[i]->AddComponent<Collider>()->SetShape(ColliderShape::Shape_Sphere);
		} else {
			n[i]->AddComponent<Collider>()->SetShape(ColliderShape::Shape_Cube);
		}

	}

	n[0]->GetComponent<Transform>()->SetRotation(FVector(0.0f, 0.0f, 0.0f));
	n[0]->GetComponent<Transform>()->SetPosition(FVector(0.0f, -25.0f, 0.0f));
	n[0]->GetComponent<Transform>()->SetScale(FVector(1.0f, 1.0f, 1.0f));
	n[0]->GetComponent<RigidBody>()->SetStatic(true);
	n[0]->GetComponent<RigidBody>()->EnableGravity(false);

	n[2]->GetComponent<Transform>()->SetPosition(FVector(0.0f, 0.0f, 0.0f));
	n[2]->GetComponent<Transform>()->SetRotation(FVector(0.0f, 0.0f, 0.4f));

	while (
		!manager.mainWindow.IsClosed() &&
		!manager.mainWindow.GetInput(InputKey::kKey_Esc) &&
		!manager.HasToExit()) {

		manager.clock.Update();

		MEASURE("BeginFrame", manager.BeginFrame());
		MEASURE("Input", manager.mainWindow.UpdateInputs());
		MEASURE("SystemsUpdate", manager.UpdateSystems());
		MEASURE("Draw", manager.Draw());

		MEASURE("UI", manager.ui.Update());
		MEASURE("EndFrame", manager.EndFrame());

		for (int32 i = 0; i < nodeAmmount; ++i) {
			if (n[i]->GetComponent<Transform>()->GetPosition().Y < -100) {
				n[i]->GetComponent<Transform>()->SetPosition(FVector(
					-20.0f + (float32)(rand() % 40),
					-20.0f + (float32)(rand() % 40),
					-20.0f + (float32)(rand() % 40)));
				n[i]->GetComponent<RigidBody>()->SetStatic(true);
				n[i]->GetComponent<RigidBody>()->SetStatic(false);
			}
		}

	}

	manager.End();


	return 0;
}