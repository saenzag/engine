
#include "Manager.h"
#include "MyOpenGL.h"

#include <glew/include/GL/glew.h>
#include "LineRenderer.h"

using namespace ENGINENAME;

Manager::Manager() {
	lineRenderer = new LineRenderer();
}

Manager::~Manager() {
	delete lineRenderer;
}

void Manager::Init() {
	
	clock.Init();
	camera.Init();
	ui.Init();

	colliderSystem.Init();

	lineRenderer->Prepare();
}

void Manager::UpdateSystems() {
	const float32 dt = (float32)clock.GetDeltaSeconds();
	camera.Update(dt);
	

	transformSystem.Update();
	geometrySystem.Update();
	materialSystem.Update();

	clock.Start("physicSystem");
	rigidBodySystem.Update(dt);
	clock.Stop("physicSystem");

	clock.Start("colliderSystem");
	colliderSystem.Update(dt);
	clock.Stop("colliderSystem");	



}

void Manager::Draw() {

	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

#if COORDINATE_SYSTEM == RIGHT_HANDED_SYSTEM
	glCullFace(GL_BACK);
#else
	glCullFace(GL_FRONT);
#endif

	nodeSystem.Draw();
}

void Manager::End() {

	colliderSystem.End();

	ui.End();
	mainWindow.ShutDown();
	clock.ShutDown(false);

}

void Manager::BeginFrame() {
	mainWindow.ClearWindow();
}

void Manager::EndFrame() {
	mainWindow.Render();
}