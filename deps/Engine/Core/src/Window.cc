
#include "Window.h"
#include "Manager.h"

#if 0 //DYNAMIC LIBRARY
#define GLFW_DLL
#endif

//#include <glew/include/GL/glew.h>
#include "MyOpenGL.h"
#include <GLFW/glfw3.h> 

using namespace ENGINENAME;

struct Window::WindowData {

#ifdef ENGINE_WIN32
	GLFWwindow* mainWindow;
	const GLFWvidmode* vidMode;
	GLFWmonitor* primaryMonitor;
#elif 0

#endif

};

/////////// CALLBACKS \\\\\\\\\\\

#ifdef ENGINE_WIN32
void ResizeCallback(GLFWwindow* window, int width, int height) {
	if (window) {
		Manager& manager = Manager::Instance();
		FVector2 newWindowSize = FVector2((float32)width, (float32)height);
		manager.mainWindow.SetWindowSize(newWindowSize);
		manager.mainWindow.SetAspectRatio(newWindowSize.X / newWindowSize.Y);
		manager.camera.AddVersion();// Update camera
	}
}

void FramebufferSizeCallback(GLFWwindow* window, int width, int height) {
	if (window) {
		glViewport(0, 0, width, height);
	}
}

void GLFWErrorCallback(int Id, const char8* error_Message) {
	Id = 0;
	error_Message = "";
}
#endif

/////////// CALLBACKS \\\\\\\\\\\

Window::Window() {
	windowData_ = new WindowData();
}

Window::~Window() {
	if (windowData_) { delete windowData_; }
}

void* Window::GetWindow() {
	return (void*)windowData_->mainWindow;
}

void Window::Init(FVector2 windowSize) {
#ifdef ENGINE_WIN32

	if (!glfwInit()) {
		return;
	}
	windowData_->primaryMonitor = glfwGetPrimaryMonitor();
	windowData_->vidMode = glfwGetVideoMode(windowData_->primaryMonitor);

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	windowData_->mainWindow = glfwCreateWindow((int32)windowSize.X, (int32)windowSize.Y, ENGINENAMETEXT, NULL, NULL);
	
	if (!windowData_->mainWindow) {
		glfwTerminate();
		return;
	}

	glfwMakeContextCurrent(windowData_->mainWindow);
	glfwSwapInterval(0);//Disable VSync

	//Attatch Callbacks
	glfwSetWindowSizeCallback(windowData_->mainWindow, ResizeCallback);
	glfwSetFramebufferSizeCallback(windowData_->mainWindow, FramebufferSizeCallback);
	glfwSetErrorCallback(GLFWErrorCallback);
	//----------------------

	MyOpenGL::InitializeGlew();

	glViewport(0, 0, (int32)windowSize.X, (int32)windowSize.Y);
	SetWindowSize(windowSize);
	glfwMaximizeWindow(windowData_->mainWindow);

#endif
}

void Window::ShutDown() {
	glfwTerminate();
}

int32 Window::IsClosed() {
	return glfwWindowShouldClose(windowData_->mainWindow);
}

void Window::ClearWindow() {

	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);
}

void Window::Render() {
	glfwPollEvents();
	glfwSwapBuffers(windowData_->mainWindow);
}

void Window::ChangeWindowMode(WindowMode newWindowMode) {
	if (newWindowMode) {
		printf("");
	}
}

void Window::SetWindowSize(FVector2 newSize) {
	windowSize_ = newSize;
}

void Window::SetClearColor(FVector newClearColor) {
	clearColor_ = newClearColor;
	glClearColor(clearColor_.X, clearColor_.Y, clearColor_.Z, 1.0f);
}

void Window::SetMousePosition(FVector2 position) {
	if (windowData_->mainWindow) {
		glfwSetCursorPos(windowData_->mainWindow, (float64)position.X, (float64)position.Y);
	}
}

void Window::UpdateInputs() {
	for (auto &key : inputs) {
		UpdateInput(key.second, glfwGetKey(windowData_->mainWindow, key.first));
	}
	for (auto &key : mouseInputs) {
		UpdateInput(key.second, glfwGetMouseButton(windowData_->mainWindow, key.first));
	}
}

FVector2 Window::GetMousePosition() {
	float64 mouseX, mouseY;
	glfwGetCursorPos(windowData_->mainWindow, &mouseX, &mouseY);
	return FVector2((float32)mouseX, (float32)mouseY);
}

InputKeyState Window::GetInput(InputKey key) {
	if (inputs.count(key)) {
		//item exists
		return inputs.find(key)->second;
	}
	else {
		//item doesnt exist
		std::pair<InputKey, InputKeyState> newPair;
		newPair.first = key;
		newPair.second = InputKeyState::k_NotPressed;

		UpdateInput(newPair.second, glfwGetKey(windowData_->mainWindow, newPair.first));
		inputs.emplace(newPair);
		return newPair.second;
	}
}

InputKeyState Window::GetMouseInput(MouseInput input) {
	if (mouseInputs.count(input)) {
		return mouseInputs.find(input)->second;
	}
	else {
		std::pair<MouseInput, InputKeyState> newPair;
		newPair.first = input;
		newPair.second = InputKeyState::k_NotPressed;

		UpdateInput(newPair.second, glfwGetMouseButton(windowData_->mainWindow, newPair.first));
		mouseInputs.emplace(newPair);
		return newPair.second;
	}
}

void Window::UpdateInput(InputKeyState &keystate, int32 keyPressed) {
	if (keyPressed) {
		//pressed
		if (keystate == InputKeyState::k_NotPressed || keystate == InputKeyState::k_Up) {
			keystate = InputKeyState::k_Down; //2
		}
		else {
			keystate = InputKeyState::k_Pressed; //8
		}
	}
	else {
		// Not Pressed
		if (keystate == InputKeyState::k_Down || keystate == InputKeyState::k_Pressed) {
			keystate = InputKeyState::k_Up; // 4
		}
		else {
			keystate = InputKeyState::k_NotPressed; //1
		}
	}
}