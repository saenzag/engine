#include "MyOpenGL.h"

#include <glew/include/GL/glew.h>
#include "Geometry.h"

using namespace ENGINENAME;

MyOpenGL::MyOpenGL() {};
MyOpenGL::~MyOpenGL() {};

// HELLO TRIANGLE

void MyOpenGL::PrepareHelloTriangle(uint32 &program, uint32 &vao) {
	uint32 htvbo;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &htvbo);
	glBindVertexArray(vao);

	const char8 *vertexShaderSource = "#version 330 core\n"
		"layout (location = 0) in vec3 position;\n"
		"layout (location = 1) in vec3 color;\n"
		"out vec3 outcolor;\n"
		"void main()\n"
		"{\n"
		"	outcolor = color;"
		"   gl_Position = vec4(position, 1.0);\n"
		"}\0";

	const char8 *fragmentShaderSource = "#version 330 core\n"
		"out vec4 FragColor;\n"
		"in vec3 outcolor;\n"
		"void main()\n"
		"{\n"
		"   FragColor = vec4(outcolor, 1.0f);\n"
		"}\n\0";

	uint32 vs, fs;
	vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertexShaderSource, NULL);
	glCompileShader(vs);

	fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragmentShaderSource, NULL);
	glCompileShader(fs);

	program = glCreateProgram();

	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);

	glDeleteShader(vs);
	glDeleteShader(fs);

	float32 vertices[] = {
		-0.5f, -0.5f, 0.0f,	// left  
		0.5f, -0.5f, 0.0f,	// right 
		0.0f,  0.5f, 0.0f,  // top   

		1.0f,0.0f,0.0f, // R
		0.0f,1.0f,0.0f, // G
		0.0f,0.0f,1.0f, // B
	};

	glBindBuffer(GL_ARRAY_BUFFER, htvbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)(9 * sizeof(float)));
	glEnableVertexAttribArray(1);
}

void MyOpenGL::DrawHelloTriangle(uint32 &program, uint32 &vao) {
	glUseProgram(program);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
}

uint32 MyOpenGL::InitializeGlew() {
	return glewInit();
}

// GEOMETRY

void MyOpenGL::GenerateVAO(uint32 &vao) {
	glGenVertexArrays(1, &vao);
}

void MyOpenGL::GenerateVBO(uint32 &vbo) {
	glGenBuffers(1, &vbo);
}

void MyOpenGL::BindVertexArray(uint32 &vao) {
	glBindVertexArray(vao);
}

void MyOpenGL::BindVertexBuffer(uint32 &vbo) {
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
}

void MyOpenGL::SetVertexBufferData(std::vector<Vertex> &v) {
	glBufferData(GL_ARRAY_BUFFER, v.size() * sizeof(Vertex), &v[0], GL_STATIC_DRAW);
}

void MyOpenGL::SetVertexBufferData(std::vector<float32> &v) {
	glBufferData(GL_ARRAY_BUFFER, v.size() * sizeof(float32), &v[0], GL_STATIC_DRAW);
}

void MyOpenGL::SetIndicesData(std::vector<uint32> &i) {
	uint32 ElementBufferObject;
	glGenBuffers(1, &ElementBufferObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ElementBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, i.size() * sizeof(uint32), &i[0], GL_STATIC_DRAW);
}

void MyOpenGL::SetVertexAttribArrays(uint32 enabled, uint32 num) {
	if (enabled) {
		for (uint32 i = 0; i < num; ++i) {
			glEnableVertexAttribArray(i);
		}
	}
	else {
		for (uint32 i = 0; i < num; ++i) {
			glDisableVertexAttribArray(i);
		}
	}
}

void MyOpenGL::SetVertexAttribs(uint32 index, uint32 size, uint32 offset, uint32 sizeOf) {
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, size, GL_FLOAT, GL_FALSE, sizeOf, (void*)offset);
}

uint32 MyOpenGL::InitShader(uint32 &shader, const char8* source, ShaderType st, std::string& errorString) {
	uint32 shaderType = GL_FRAGMENT_SHADER;

	switch (st) {
	case ShaderType::kFragmentShader:
		shaderType = GL_FRAGMENT_SHADER;
		break;
	case ShaderType::kVertexShader:
		shaderType = GL_VERTEX_SHADER;
		break;
	}

	shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);

	//Test if succesfull
	int32 success;
	char8 infoLog[1024];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(shader, 1024, NULL, infoLog);
		//std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
		errorString = "ERROR: ";
		errorString += st == kFragmentShader ? "FRAGMENT_" : (st == kVertexShader ? "VERTEX_" : "UNKNOWN_");
		errorString += "SHADER_COMPILATION_ERROR --- ";
		errorString += infoLog;
		return ErrorType::kShaderCompilation;
	}

	return ErrorType::kNoError;
}

uint32 MyOpenGL::InitProgram(uint32 &program, uint32 &vs, uint32 &fs, std::string errorString)
{
	program = glCreateProgram();

	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);

	//Test if succesfull
	int32 success;
	char8 infoLog[1024];
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(program, 1024, NULL, infoLog);

		errorString = "ERROR: PROGRAM_LINKING_ERROR ---";
		errorString += infoLog;

		return ErrorType::kShaderLinking;
	}
	return ErrorType::kNoError;
}

void MyOpenGL::UseProgram(uint32 &program) {
	glUseProgram(program);
}

uint32 MyOpenGL::GetUniform(uint32 &program, const char8* name) {
	return glGetUniformLocation(program, name);
}

void MyOpenGL::SetUniform(UniformType ut, uint32 uniform, void * data) {

	switch (ut) {
	case kUniformType_Integer:
		glUniform1i(uniform, *(GLint*)data);
		break;
	case kUniformType_Float:
		glUniform1f(uniform, *(GLfloat*)data);
		break;
	case kUniformType_Vec3:
		glUniform3fv(uniform, 1, (GLfloat*)data);
		break;
	case kUniformType_Vec4:
		glUniform4fv(uniform, 1, (GLfloat*)data);
		break;
	case kUniformType_Mat3:
		glUniformMatrix3fv(uniform, 1, false, (GLfloat*)data);
		break;
	case kUniformType_Mat4:
		glUniformMatrix4fv(uniform, 1, false, (GLfloat*)data);
		break;
	}
}

void MyOpenGL::DrawTriangles(uint32 numIndices) {
	//glDrawArrays(GL_TRIANGLES, 0, numVertices);
	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, 0);
}

void MyOpenGL::UnbindVAO() {
	glBindVertexArray(0);
}

void MyOpenGL::DrawLine() {
	glDrawArrays(GL_LINES, 0, 2);
}
